<?php
App::uses('AppModel', 'Model');
/**
 * Ejerciciosfiscale Model
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 */
class Ejerciciosfiscale extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'ejerciciofiscal';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ejerciciofiscal' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'joinTable' => 'actuacionesfiscales_ejerciciosfiscales',
			'foreignKey' => 'ejerciciosfiscale_id',
			'associationForeignKey' => 'actuacionesfiscale_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
