<?php
App::uses('AppModel', 'Model');
/**
 * Operacione Model
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 * @property Evento $Evento
 * @property Fremite $Fremite
 * @property Frecibe $Frecibe
 * @property Eventualidadtipo $Eventualidadtipo
 * @property Anexo $Anexo
 */
class Operacione extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'actuacionesfiscale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'evento_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fremite_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'frecibe_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'eventualidadtipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'foreignKey' => 'actuacionesfiscale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Evento' => array(
			'className' => 'Evento',
			'foreignKey' => 'evento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fremite' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'fremite_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Frecibe' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'frecibe_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Eventualidadtipo' => array(
			'className' => 'Eventualidadtipo',
			'foreignKey' => 'eventualidadtipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AnexosOperacione' => array(
			'className' => 'AnexosOperacione',
			'foreignKey' => 'operacione_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
    );

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Anexo' => array(
			'className' => 'Anexo',
			'joinTable' => 'anexos_operaciones',
			'foreignKey' => 'operacione_id',
			'associationForeignKey' => 'anexo_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Operacione']['fecha_remite'])) {
			$this->data['Operacione']['fecha_remite'] = $this->dateFormatBeforeSaveWithHours($this->data['Operacione']['fecha_remite']);
		}
		return true;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Operacione']['fecha_remite'])) {
                $results[$key]['Operacione']['fecha_remite'] = $this->dateFormatAfterFindWithHours($val['Operacione']['fecha_remite']);
            }
            if (isset($val['Operacione']['fecha_recibe'])) {
                $results[$key]['Operacione']['fecha_recibe'] = $this->dateFormatAfterFindWithHours($val['Operacione']['fecha_recibe']);
            }
            if (isset($val['Frecibe']['fecha_remite'])) {
                $results[$key]['Frecibe']['fecha_remite'] = $this->dateFormatAfterFindWithHours($val['Frecibe']['fecha_remite']);
            }
            if (isset($val['Frecibe']['fecha_recibe'])) {
                $results[$key]['Frecibe']['fecha_recibe'] = $this->dateFormatAfterFindWithHours($val['Frecibe']['fecha_recibe']);
            }
            if (isset($val['Fremite']['fecha_remite'])) {
                $results[$key]['Fremite']['fecha_remite'] = $this->dateFormatAfterFindWithHours($val['Fremite']['fecha_remite']);
            }
            if (isset($val['Fremite']['fecha_recibe'])) {
                $results[$key]['Fremite']['fecha_recibe'] = $this->dateFormatAfterFindWithHours($val['Fremite']['fecha_recibe']);
            }
        }
        return $results;
    }

}
