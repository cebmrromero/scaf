<?php
App::uses('AppModel', 'Model');
/**
 * AnexosOperacione Model
 *
 * @property Anexo $Anexo
 * @property Operacione $Operacione
 */
class AnexosOperacione extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'anexo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'operacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Anexo' => array(
			'className' => 'Anexo',
			'foreignKey' => 'anexo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Operacione' => array(
			'className' => 'Operacione',
			'foreignKey' => 'operacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
