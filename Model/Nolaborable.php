<?php
App::uses('AppModel', 'Model');
/**
 * Nolaborable Model
 *
 */
class Nolaborable extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'dia';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'dia' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'descripcion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'repetitivo' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public function getDias() {
		$mdias = array();
		$dias = $this->find("all");
		foreach ($dias as $dia) {
			$mdias[] = $dia['Nolaborable']['dia'];
		}
		
		return $mdias;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Nolaborable']['dia']) && $val['Nolaborable']['repetitivo']) {
				list($anio, $mes, $dia) = explode('-', $results[$key]['Nolaborable']['dia']);
                $results[$key]['Nolaborable']['dia'] = "$mes-$dia";
            }
        }
        return $results;
    }
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Nolaborable']['dia'])) {
			if ($this->data['Nolaborable']['repetitivo']) {
				$this->data['Nolaborable']['dia'] .= '-' . date('Y');
			}
			$this->data['Nolaborable']['dia'] = $this->dateFormatBeforeSave($this->data['Nolaborable']['dia']);
		}
		return true;
	}
}
