<?php
App::uses('AppModel', 'Model');
/**
 * Credenciale Model
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 * @property Equipotrabajo $Equipotrabajo
 */
class Credenciale extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'numero';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'numero' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		//'fecha' => array(
		//	'date' => array(
		//		'rule' => array('date'),
		//		//'message' => 'Your custom message here',
		//		//'allowEmpty' => false,
		//		//'required' => false,
		//		//'last' => false, // Stop validation after this rule
		//		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		//	),
		//),
		'actuacionesfiscale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'foreignKey' => 'actuacionesfiscale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Equipotrabajo' => array(
			'className' => 'Equipotrabajo',
			'foreignKey' => 'credenciale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Credenciale']['fecha'])) {
			$this->data['Credenciale']['fecha'] = $this->dateFormatBeforeSave($this->data['Credenciale']['fecha']);
		}
		return true;
	}

}
