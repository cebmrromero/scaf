<?php
App::uses('AppModel', 'Model');
/**
 * Evento Model
 *
 * @property Anexo $Anexo
 * @property Evento $ParentEvento
 * @property Fase $Fase
 * @property Eventoinspeccione $Eventoinspeccione
 * @property Evento $ChildEvento
 * @property Eventualidade $Eventualidade
 * @property Eventualidadtipo $Eventualidadtipo
 * @property Operacione $Operacione
 * @property Actuacionesfiscale $Actuacionesfiscale
 * @property Anexo $Anexo
 */
class Evento extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->virtualFields['fulldenominacion'] = sprintf('CONCAT(%s.posicion, " ", %s.denominacion)', $this->alias, $this->alias);
    }
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'fulldenominacion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'denominacion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'posicion' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fase_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Parent' => array(
			'className' => 'Evento',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fase' => array(
			'className' => 'Fase',
			'foreignKey' => 'fase_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Eventoinspeccione' => array(
			'className' => 'Eventoinspeccione',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Children' => array(
			'className' => 'Evento',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Eventualidade' => array(
			'className' => 'Eventualidade',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Eventualidadtipo' => array(
			'className' => 'Eventualidadtipo',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Operacione' => array(
			'className' => 'Operacione',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Fase' => array(
			'className' => 'Fase',
			'foreignKey' => 'evento_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'joinTable' => 'actuacionesfiscales_eventos',
			'foreignKey' => 'evento_id',
			'associationForeignKey' => 'actuacionesfiscale_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Anexo' => array(
			'className' => 'Anexo',
			'joinTable' => 'anexos_eventos',
			'foreignKey' => 'evento_id',
			'associationForeignKey' => 'anexo_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}