<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public function dateFormatBeforeSave($dateString) {
		return date('Y-m-d', strtotime($dateString));
	}
    
	public function dateFormatBeforeSaveWithHours($dateString) {
		return date('Y-m-d H:i:s', strtotime($dateString));
	}
    
	public function dateFormatAfterFindWithHours($dateString) {
		return date('d-m-Y h:i A', strtotime($dateString));
	}
	
	public function dateFormatAfterFind($dateString) {
		return date('d-m-Y', strtotime($dateString));
	}
	
	public function isOwnedBy($compare, $funcionario_id) {
		if (is_array($compare)) {
			return in_array($funcionario_id, $compare);
		}
		return $compare == $funcionario_id;
	}
	
	// AuditLog
	public function currentUser() {
		App::import('component', 'CakeSession');        
		$user = CakeSession::read('Auth.User');
		return array('id' => $user['id'], 'description' => $user['username']);
	}
}
