<?php
App::uses('AppModel', 'Model');
/**
 * Eventualidade Model
 *
 * @property Evento $Evento
 * @property Detecta $Detecta
 * @property Corrige $Corrige
 * @property Eventualidadtipo $Eventualidadtipo
 * @property Mediocomunicacione $Mediocomunicacione
 */
class Eventualidade extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'asunto';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'fecha' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Fecha inválida',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'numero' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'evento_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'detecta_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'corrige_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'eventualidadtipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'mediocomunicacione_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'asunto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'evento' => array(
			'inlist' => array(
				'rule' => array('inlist', array('NR', 'R', 'A', 'N')),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Evento' => array(
			'className' => 'Evento',
			'foreignKey' => 'evento_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Detecta' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'detecta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Corrige' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'corrige_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Eventualidadtipo' => array(
			'className' => 'Eventualidadtipo',
			'foreignKey' => 'eventualidadtipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Mediocomunicacione' => array(
			'className' => 'Mediocomunicacione',
			'foreignKey' => 'mediocomunicacione_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'foreignKey' => 'actuacionesfiscale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Eventualidade']['fecha_deteccion'])) {
			$this->data['Eventualidade']['fecha_deteccion'] = $this->dateFormatBeforeSaveWithHours($this->data['Eventualidade']['fecha_deteccion']);
		}
		return true;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Eventualidade']['fecha_deteccion'])) {
                $results[$key]['Eventualidade']['fecha_deteccion'] = $this->dateFormatAfterFindWithHours($val['Eventualidade']['fecha_deteccion']);
            }
            if (isset($val['Eventualidade']['fecha_verificacion'])) {
                $results[$key]['Eventualidade']['fecha_verificacion'] = $this->dateFormatAfterFindWithHours($val['Eventualidade']['fecha_verificacion']);
            }
            if (isset($val['Corrige']['fecha_deteccion'])) {
                $results[$key]['Corrige']['fecha_deteccion'] = $this->dateFormatAfterFindWithHours($val['Corrige']['fecha_deteccion']);
            }
            if (isset($val['Corrige']['fecha_verificacion'])) {
                $results[$key]['Corrige']['fecha_verificacion'] = $this->dateFormatAfterFindWithHours($val['Corrige']['fecha_verificacion']);
            }
            if (isset($val['Detecta']['fecha_deteccion'])) {
                $results[$key]['Detecta']['fecha_deteccion'] = $this->dateFormatAfterFindWithHours($val['Detecta']['fecha_deteccion']);
            }
            if (isset($val['Detecta']['fecha_verificacion'])) {
                $results[$key]['Detecta']['fecha_verificacion'] = $this->dateFormatAfterFindWithHours($val['Detecta']['fecha_verificacion']);
            }
        }
        return $results;
    }
}
