<?php
App::uses('AppModel', 'Model');
/**
 * Actuacionesfiscale Model
 *
 * @property Fase $Fase
 * @property Actuacionesorigene $Actuacionesorigene
 * @property Actuaciontipo $Actuaciontipo
 * @property Objeto $Objeto
 * @property User $User
 * @property Equipotrabajo $Equipotrabajo
 * @property Operacione $Operacione
 * @property Prorroga $Prorroga
 */
class Actuacionesfiscale extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'numero';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'numero' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'obj_general' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dh_planificacion' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dh_ejecucion' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'dh_resultados' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fase_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'actuacionesorigene_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'actuaciontipo_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'objeto_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Fase' => array(
			'className' => 'Fase',
			'foreignKey' => 'fase_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actuacionesorigene' => array(
			'className' => 'Actuacionesorigene',
			'foreignKey' => 'actuacionesorigene_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actuaciontipo' => array(
			'className' => 'Actuaciontipo',
			'foreignKey' => 'actuaciontipo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Objeto' => array(
			'className' => 'Objeto',
			'foreignKey' => 'objeto_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Direccioncontrole' => array(
			'className' => 'Direccioncontrole',
			'foreignKey' => 'direccioncontrole_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Objetivoespecifico' => array(
			'className' => 'Objetivoespecifico',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Equipotrabajo' => array(
			'className' => 'Equipotrabajo',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Equipotrabajo.funcionariorole_id ASC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Operacione' => array(
			'className' => 'Operacione',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Prorroga' => array(
			'className' => 'Prorroga',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Eventualidade' => array(
			'className' => 'Eventualidade',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ActuacionesfiscalesEvento' => array(
			'className' => 'ActuacionesfiscalesEvento',
			'foreignKey' => 'actuacionesfiscale_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Evento' => array(
			'className' => 'Evento',
			'joinTable' => 'actuacionesfiscales_eventos',
			'foreignKey' => 'actuacionesfiscale_id',
			'associationForeignKey' => 'evento_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'Evento.posicion',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Ejerciciosfiscale' => array(
			'className' => 'Ejerciciosfiscale',
			'joinTable' => 'actuacionesfiscales_ejerciciosfiscales',
			'foreignKey' => 'actuacionesfiscale_id',
			'associationForeignKey' => 'ejerciciosfiscale_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'Ejerciciosfiscale.id DESC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Actuacionesfiscale']['fecha_inicio_estimada']) && !empty($this->data['Actuacionesfiscale']['fecha_fin_estimada'])) {
			$this->data['Actuacionesfiscale']['fecha_inicio_estimada'] = $this->dateFormatBeforeSave($this->data['Actuacionesfiscale']['fecha_inicio_estimada']);
			$this->data['Actuacionesfiscale']['fecha_fin_estimada'] = $this->dateFormatBeforeSave($this->data['Actuacionesfiscale']['fecha_fin_estimada']);
		}
		return true;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Actuacionesfiscale']['fecha_inicio_estimada'])) {
                $results[$key]['Actuacionesfiscale']['fecha_inicio_estimada'] = $this->dateFormatAfterFind($val['Actuacionesfiscale']['fecha_inicio_estimada']);
            }
            if (isset($val['Actuacionesfiscale']['fecha_fin_estimada'])) {
                $results[$key]['Actuacionesfiscale']['fecha_fin_estimada'] = $this->dateFormatAfterFind($val['Actuacionesfiscale']['fecha_fin_estimada']);
            }
            if (isset($val['Actuacionesfiscale']['fecha_inicio'])) {
                $results[$key]['Actuacionesfiscale']['fecha_inicio'] = $this->dateFormatAfterFindWithHours($val['Actuacionesfiscale']['fecha_inicio']);
            }
            if (isset($val['Actuacionesfiscale']['fecha_fin'])) {
                $results[$key]['Actuacionesfiscale']['fecha_fin'] = $this->dateFormatAfterFindWithHours($val['Actuacionesfiscale']['fecha_fin']);
            }
        }
        return $results;
    }

}
