<?php
App::uses('AppModel', 'Model');
/**
 * Anexo Model
 *
 * @property Fase $Fase
 * @property Evento $Evento
 * @property Operacione $Operacione
 */
class Anexo extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'denominacion';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'denominacion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fase_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Fase' => array(
			'className' => 'Fase',
			'foreignKey' => 'fase_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Evento' => array(
			'className' => 'Evento',
			'joinTable' => 'anexos_eventos',
			'foreignKey' => 'anexo_id',
			'associationForeignKey' => 'evento_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		),
		'Operacione' => array(
			'className' => 'Operacione',
			'joinTable' => 'anexos_operaciones',
			'foreignKey' => 'anexo_id',
			'associationForeignKey' => 'operacione_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
