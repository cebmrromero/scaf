<?php
App::uses('AppModel', 'Model');
/**
 * Prorroga Model
 *
 * @property Solicita $Solicita
 * @property Aprueba $Aprueba
 * @property Actuacionesfiscale $Actuacionesfiscale
 */
class Prorroga extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ndias' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo numérico',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'asunto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'es_aprobada' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'solicita_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'aprueba_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'actuacionesfiscale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Campo requerido',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Solicita' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'solicita_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Aprueba' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'aprueba_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'foreignKey' => 'actuacionesfiscale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fase' => array(
			'className' => 'Fase',
			'foreignKey' => 'fase_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Prorroga']['fecha_solicitud'])) {
			$this->data['Prorroga']['fecha_solicitud'] = $this->dateFormatBeforeSaveWithHours($this->data['Prorroga']['fecha_solicitud']);
		}
		return true;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Prorroga']['fecha_solicitud'])) {
                $results[$key]['Prorroga']['fecha_solicitud'] = $this->dateFormatAfterFindWithHours($val['Prorroga']['fecha_solicitud']);
            }
            if (isset($val['Prorroga']['fecha_verificacion'])) {
                $results[$key]['Prorroga']['fecha_verificacion'] = $this->dateFormatAfterFindWithHours($val['Prorroga']['fecha_verificacion']);
            }
        }
        return $results;
    }
}
