<?php
App::uses('AppModel', 'Model');
/**
 * Equipotrabajo Model
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 * @property Funcionario $Funcionario
 * @property Funcionariorole $Funcionariorole
 */
class Equipotrabajo extends AppModel {
    public $actsAs = array( 'AuditLog.Auditable' );

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'actuacionesfiscale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'funcionario_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe indicar el funcionario asociado',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'credenciale_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe indicar el oficio credencial del funcionario',
				'allowEmpty' => true,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'funcionariorole_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Debe indicar el rol del funcionario',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'nota_designacion' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Debe especificar su credencial',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'fecha' => array(
			'date' => array(
				'rule' => array('date', 'dmy'),
				'message' => 'Fecha inválida',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Actuacionesfiscale' => array(
			'className' => 'Actuacionesfiscale',
			'foreignKey' => 'actuacionesfiscale_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Funcionario' => array(
			'className' => 'Funcionario',
			'foreignKey' => 'funcionario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Funcionariorole' => array(
			'className' => 'Funcionariorole',
			'foreignKey' => 'funcionariorole_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public function beforeSave($options = array()) {
		if (!empty($this->data['Equipotrabajo']['fecha'])) {
			$this->data['Equipotrabajo']['fecha'] = $this->dateFormatBeforeSave($this->data['Equipotrabajo']['fecha']);
		}
		return true;
	}
    
    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            if (isset($val['Equipotrabajo']['fecha'])) {
                $results[$key]['Equipotrabajo']['fecha'] = $this->dateFormatAfterFind($val['Equipotrabajo']['fecha']);
            }
        }
        return $results;
    }
}
