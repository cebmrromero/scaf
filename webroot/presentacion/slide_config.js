var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    title: 'SCAF',
    subtitle: 'Sistema de Control de Actuaciones Fiscales',
    //eventInfo: {
    //  title: 'Google I/O',
    //  date: '6/x/2013'
    //},
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    usePrettify: true, // Default: true
    enableSlideAreas: true, // Default: true. False turns off the click areas on either slide of the slides.
    enableTouch: true, // Default: true. If touch support should enabled. Note: the device must support touch.
    //analytics: 'UA-XXXXXXXX-1', // TODO: Using this breaks GA for some reason (probably requirejs). Update your tracking code in template.html instead.
    favIcon: 'images/scaf_logo.png',
    fonts: [
      'Open Sans:regular,semibold,italic,italicsemibold',
      'Source Code Pro'
    ],
    //theme: ['mytheme'], // Add your own custom themes or styles in /theme/css. Leave off the .css extension.
  },

  // Author information
  presenters: [{
    name: 'Wuilliam A. Lacruz M.',
    company: 'Analista de Sistemas I<br>Contraloría del Estado Mérida',
    gplus: 'http://plus.google.com/118355243203147581138',
    twitter: '@TaHaNoM',
    www: 'http://www.wlacruz.com.ve',
    github: 'http://github.com/wuilliam321'
  }/*, {
    name: 'Second Name',
    company: 'Job Title, Google',
    gplus: 'http://plus.google.com/1234567890',
    twitter: '@yourhandle',
    www: 'http://www.you.com',
    github: 'http://github.com/you'
  }*/]
};

