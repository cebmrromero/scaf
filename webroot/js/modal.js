$(function () {
    /**
     * Plugin para mostrar el modal
     */
    jQuery.fn.checkAnexosToModal = function() {
        // Al enviar el fomulario se valida si tiene anexos
        $(this).bind('submit',function(e) {
            // Capturando el formulario
            var form = $(this);
            form.data('submitted', false);

            if (form.data('modal-done') == undefined || form.data('modal-done') === false) {
                $('#anexos-adjuntos').modal( "toggle" )
                e.preventDefault();
            }
            form.data('modal-done', true);
        });
        return this;
    };
    
    /**
     * Al presionar en continuar se cierra y se envia el form
     */
    $(".continue-anexos-modal").click(function () {
        $("form.check-anexos").submit()
    })
    
    /**
     * Al cerrar, solo se cierra el modal y se permite reenviar
     */
    $(".close-anexos-modal").click(function () {
        $("form.check-anexos").data('modal-done', false);
        $('#anexos-adjuntos').modal( "toggle" )
    })
    
    /**
     * Si el formulario tiene la clase indicada, entonces se valida si tiene anexos
     * para mostrar entonces el modal con la data
     */
    $("form.check-anexos").checkAnexosToModal();
    
    
    $(".recibir-anexos").click(function (e) {
        button = $(this);
        // Bandera para saber cual de ellos fue clickeado
        $(".recibir-anexos").removeAttr("alt");
        $(button).attr("alt", 'active');
        $('#anexos-adjuntos-recibir').modal( "toggle" )
        if ($(button).data('modal-done') == undefined || $(button).data('modal-done') === false) {
            e.preventDefault();
            $(button).removeClass("disabled");
            $('#anexos-adjuntos-recibir button').removeClass("disabled");
        } else {
            $(button).data('modal-done', true)
        }
    })
    
    
    $(".continue-recibe-anexos-modal").click(function (e) {
        form = $(this).parents("form")
        $.post($(form).attr("action"), $(form).serialize() ).done(function () {
            //$(".recibir-anexos[alt=active]").trigger("click")
            location.href = $(".recibir-anexos[alt=active]").attr("href")
        });
        $(this).removeClass("disabled");
        e.preventDefault()
    })
    
    
    $(".close-recibe-anexos-modal").click(function (e) {
        $(".recibir-anexos").data('modal-done', false);
        $('#anexos-adjuntos-recibir').modal( "toggle" )
        $(this).removeClass("disabled");
        e.preventDefault()
    })
})