var obj_especificos = new Array();
$(function () {
    /**
     * Objetivos especificos, control de add y edit
     */
    $("#add-objetivo").click(function (e) {
        e.preventDefault();
        if ($("#ActuacionesfiscaleObjetivoContent").val()) {
            obj_especificos.push($("#ActuacionesfiscaleObjetivoContent").val());
            write_obj_especificos(obj_especificos);
            // Limpiando el campo
            $("#ActuacionesfiscaleObjetivoContent").val("")
        } else {
            alert("¡Debe indicar el contenido del objetivo específico!");
            $("#ActuacionesfiscaleObjetivoContent").focus();
        }
        $("#add-objetivo").removeClass("disabled")
        $("#ActuacionesfiscaleObjetivoContent").focus()
    })
    
    function write_obj_especificos(obj_especificos, editar) {
        if (obj_especificos.length) {
            ul = $("<ul>").addClass("unstyled")
            m = 1;
            for ( i in obj_especificos) {
                input = $("<textarea>")
                $(input).attr({id: 'Objetivoespecifico' + i + 'Denominacion', name: 'data[Objetivoespecifico][' + i + '][denominacion]', 'class': 'span11'}).text(obj_especificos[i])
                li = $("<li>")
                $("<span>").addClass("content").text(m + ". ").appendTo(li)
                $(input).appendTo(li)
                $('<a href="#" class="obj del-obj btn btn-danger btn-mini" alt="' + i + '">X</a>').appendTo(li)
                
                if (editar === true) {
                    input = $("<input>")
                    $(input).attr({type: 'hidden', id: 'Objetivoespecifico' + i + 'Id', name: 'data[Objetivoespecifico][' + i + '][id]'}).val(i)
                    $(input).appendTo(li)
                }
                $(li).appendTo(ul)
                m++;
            }
            $("#obj-container").html(ul)
    
            $("a.del-obj").click(function (e) {
                e.preventDefault();
                if (confirm("¿Está seguro que desea eliminar el objetivo específico?")) {
                    obj_especificos.splice($(this).attr("alt"), 1);
                    write_obj_especificos(obj_especificos)
                }
            })
        } else {
            $("#obj-container").html('<label>&nbsp;</label><p class="text-success">Para agregar objetivos específicos, debe ingresar su contenido y presionar el botón (+).</p>');
        }
    }
})