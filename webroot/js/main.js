$(function () {
    /**
     * Retrazo de actualizacion del COMET en milisegundos
     */
    var COMET_DELAY = 10 * 1000
    
    /**
     * Arreglo que contendra los dias a bloquear por ser feriados
     */
    var disabledDays = new Array();
    
    /**
     * Obteniendo los dias no laborables
     */
    if ($( ".date" ).length || $( ".date-start" ).length || $( ".date-end" ).length) {
        $.ajax({
            type : 'get',
            url: BASE_URL + '/json/getNolaborables',
            dataType : 'json',
            success : function(response) {
                // Estableciendo los dias no laborables
                disabledDays = response;
                
                // Si esta en una ventana donde hay un rango de fecha se buscan los dias
                // habiles, calculados
                if ($( ".date-start" ).length || $( ".date-end" ).length) {
                    // Obteniendo los dias habiles
                    getDiasHabiles($( ".date-start" ).val(), $( ".date-end" ).val());
                }
            }
        });
    }
    
    /**
     * Metodo para teterminar si una fecha es feriada o no, lo busca en disabledDays
     */
    function esDiaFeriado(date) {
        // Obteniendo mes dia y anio
        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
        for (i = 0; i < disabledDays.length; i++) {
            // Si el dia o el mes es mejor a 9 le concatenamos en 0
            ma = (m < 10) ? '0' + (m+1) : m;
            da = (d < 10) ? '0' + d : d;
            day_all = ma + '-' + da;
            day_now =  y + '-' + ma + '-' + da;
            
            // Si la fecha dada esta en el arreglo, es feriado
            if(($.inArray(day_now,disabledDays) != -1) || ($.inArray(day_all,disabledDays) != -1)) {
                return [false];
            }
        }
        return [true];
    }
    
    /**
     * Desactivando los fines de semana y dias feriados
     */
    function noWeekendsOrHolidays(date) {
        var noWeekend = jQuery.datepicker.noWeekends(date);
        return noWeekend[0] ? esDiaFeriado(date) : noWeekend;
    }
    
    /**
     * Establece el numero de dias habiles en el campo y span correspondientes
     */
    function setDiasHabilesInput(ndias) {
        // Cambiando los valores
        $("#dh-span").text(ndias);
        $("#dh-input").val(ndias);
        
        // Habilitando la distribucion de dias habiles
        $(".dhabil").removeAttr("disabled");
    }
    
    /**
     * Metodo para obtener via ajax los dias habiles en formato json
     *
     * @returns int Numero de dias
     */ 
    function getDiasHabiles(fechainicio, fechafin) {
        // Inicialmente 0 dias
        ndias = 0;
        $.ajax({
            type : 'get',
            url: BASE_URL + '/json/getDiasHabilesJson',
            data: {fechainicio:fechainicio, fechafin:fechafin, diasferiados:disabledDays},
            dataType : 'json',
            async:false,
            success : function(diashabiles) {
                if (diashabiles.ndias > 0) {
                    // Llamada al metodo que establece y cambia los valores
                    setDiasHabilesInput(diashabiles.ndias);
                }
            }
        });
        return ndias;
    }
    
    /**
     * Estableciendo la localizacion del datepicker a espaniol
     */
    $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
    
    /**
     * Definiendo un selector simple de date picker
     */
    $(".date").datepicker({
        // Desactivando fines de semana y feriados
        beforeShowDay: noWeekendsOrHolidays,
        dateFormat: 'dd-mm-yy'
    });
    
    /**
     * Definiendo el elemento INICIO de un datepicker con rango
     */
    $( ".date-start" ).datepicker({
        // Desactivando fines de semana y feriados
        beforeShowDay: noWeekendsOrHolidays,
        dateFormat: "dd-mm-yy",
        // Restringiendo el dia minimo al dia actual
        //minDate: new Date(), // Comentado temporalmente
        // Por comodidad de muestran dos meses
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date-end" ).datepicker( "option", "minDate", selectedDate );
            // Recalculando dias habiles
            getDiasHabiles(selectedDate, $( ".date-end" ).val());
        }
    });
    
    /**
     * Definiendo el elemento FIN de un datepicker rango
     */
    $( ".date-end" ).datepicker({
        beforeShowDay: noWeekendsOrHolidays,
        dateFormat: "dd-mm-yy",
        //minDate: new Date(), // Comentado temporalmente
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date-start" ).datepicker( "option", "maxDate", selectedDate );
            getDiasHabiles($( ".date-start" ).val(), selectedDate);
        }
    });
    
    /**
     * Definiendo el elemento INICIO de un datepicker con rango
     */
    $( ".date-from" ).datepicker({
        // Desactivando fines de semana y feriados
        beforeShowDay: noWeekendsOrHolidays,
        dateFormat: "dd-mm-yy",
        // Por comodidad de muestran dos meses
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date-to" ).datepicker( "option", "minDate", selectedDate );
            // Recalculando dias habiles
            getDiasHabiles(selectedDate, $( ".date-to" ).val());
        }
    });
    
    /**
     * Definiendo el elemento FIN de un datepicker rango
     */
    $( ".date-to" ).datepicker({
        beforeShowDay: noWeekendsOrHolidays,
        dateFormat: "dd-mm-yy",
        numberOfMonths: 2,
        onClose: function( selectedDate ) {
            $( ".date-from" ).datepicker( "option", "maxDate", selectedDate );
            getDiasHabiles($( ".date-from" ).val(), selectedDate);
        }
    });
    
    /**
     * Al cambiar un distribuidor de dias habiles, se validan que sean positivos
     * y que ademas no excedan el numero de dias habiles disponibles
     */
    $(".dhabil").change(function (e) {
        if (!$(this).val()) {
            $(this).val(0);
        }
        var dhdisponibles;
        var dhtotal;
        var dh;
        
        // Validando que no pongan valores menores a 0
        if (parseInt($(this).val()) < 0) {
            $(this).val(1);
            e.preventDefault();
        }
        
        // Capturanto el total de dias disponibles
        dhdisponibles = parseInt($('#dh-input').val());
        
        // Contador total de dias habiles
        dhtotal = 0;
        
        // Auxiliar para dias habiles indicados
        dh = 0;
        
        // Contador DH
        i = 0;
        // Calculando el total de dias habiles utilizados
        $(".dhabil").each(function () {
            // Esto es para tener una bandera con la cual determinar
            // quien fue el que hizo el llamado para cambiar los dias habiles
            // es decir, el input que esta modificandose
            input = this;
            
            // Obteniendo la cantidad de dias habiles del input
            dh = ($(this).val()) ? parseInt($(this).val()) : 0;
            
            // Calculando el total
            dhtotal += dh;
            
            // Esto es para determinar cuantos input ha establecido y luego poner el 3ero automaticamente
            if ($(this).val() != 0) {
                i++;
            }
        })
        
        // Restando a los disponibles, el total utilizado
        dhdisponibles -= dhtotal;
        
        // Si hay mas utilizados que disponibles, se arroja un error
        if (dhdisponibles < 0) {
            alert("Atención! Sólo cuentas con (" + parseInt($('#dh-input').val()) + ") día(s) hábil(es)");
            
            // Si el valor cambiado ya esta en Cero (0) no se resta valor sino que se establece en 0
            // Esto para evitar mostrar -1, en el input
            $(this).val((parseInt($(this).val()) + dhdisponibles < 0) ? 0 : parseInt($(this).val()) + dhdisponibles);
            e.preventDefault();
        }
        
        // Si hay 3 input definidos, el 3ero se calcula
        if (i == 2) {
            $(".dhabil").each(function () {
                if (!$(this).val()) {
                    $(this).val(dhdisponibles);
                }
            })
        }
    })
    
    $( ".multiselect" ).multiselect({
        nonSelectedText: "Ninguno seleccionado",
        nSelectedText: "seleccionado(s)"
    })
    
    /**
     * Previene el reenvio multiple de formularios
     * 
     * jQuery plugin to prevent double submission of forms
     * Source: http://stackoverflow.com/questions/2830542/prevent-double-submission-of-forms-in-jquery
     */
    jQuery.fn.preventDoubleSubmission = function() {
        // Al enviar formulario se valida si fue realmente enviado
        $(this).bind('submit',function(e) {
            // Capturando el formulario
            var $form = $(this);
            
            // Si el formulario esta enviado, no se permite enviar nuevamente
            if ($form.data('submitted') === true) {
                // Previously submitted - don't submit again
                e.preventDefault();
            } else {
                // Si no habia sido enviado, lo marcamos ahora si como enviado
                $form.data('submitted', true);
            }
        });
        return this;
    };
    
    /**
     * Prevenir envio multiple de formularios
     */
    $("form").preventDoubleSubmission();
    
    /**
     * Desactivar el boton al presionarlo
     */
    $(".btn").one('click', function () {
        if (!$(this).hasClass('dropdown-toggle')) {
            $(this).addClass("disabled");
        }
    });
    
    /**
     * Reactivar el boton si algun campo es invaliuo
     */
    $('form input, form select, form textarea').bind("invalid", function () {
        $(".btn").removeClass("disabled");
    })
    
    /**
     * Tooltips - personalizados
     */
    //$( "#pending-alert" ).popover({html:true, trigger: 'click', placement: 'bottom'});
    $( "tr" ).popover({html:true, trigger: 'hover', placement: 'top'});
    
    /**
     * Editor personalizado
     */
    $('.editor').wysihtml5({locale: "es-ES"});
    
    
    /*****************
     * OTROS EVENTOS *
     *****************/
    
    /**
     * Mostrar u cultar el campo de municipio y lugar
     */
    function showHideCamposInspeccion() {
        if ($("#EventualidadeEventualidadtipoId").val() == 8) {
            $("#EventualidadeMunicipio, #EventualidadeLugar").parent().show("fade").removeClass('hidden');
        } else {
            $("#EventualidadeMunicipio, #EventualidadeLugar").parent().hide("fade");
            $("#EventualidadeMunicipio, #EventualidadeLugar").val('');
        }
    }
    /**
     * Si existe el elemento eventualidad tipo se establecen algunos metodos para
     * mostrar u ocultar elementos del formulario
     */
    if ($("#EventualidadeEventualidadtipoId").length) {
        $("#EventualidadeMunicipio, #EventualidadeLugar").parent().hide();
        showHideCamposInspeccion();
        
        // Esto es para garantizar que se ejecute el showHideCamposInspeccion si es un select
        $("#EventualidadeEventualidadtipoId").trigger("change");
    }
    
    /**
     * Al cambiar las eventualidades se verifica si es necesario mostrar lugar y municipio
     */
    $("#EventualidadeEventualidadtipoId").change(function () {
        showHideCamposInspeccion();
    })
    
    /**
     * Comet - Para mantener actualizado los pendientes (Boton rojo al lado del nombre)
     * 
     * Esto es un metodo que se repite constantemente para determinar si hay actividades,
     * Operaciones, eventualidades o prorrogas nuevas
     */
    var Comet = function (data_url) {
        this.url = data_url;
        
        this.connect = function() {
            var self = this;
            
            $.ajax({
                type : 'get',
                url : this.url,
                dataType : 'json',
                data : {'count': this.count},
                success : function(response) {
                    self.handleResponse(response);
                },
                complete : function(response) {
                    // Recursivamente actualizamos
                    setTimeout(function(){ comet.connect(); }, COMET_DELAY);
                }
            });
        }
        /**
         * Metodo para generar el popover con la informacion capturada
         */
        this.handleResponse = function(response) {
            var type = (parseInt(response.count) > 0) ? ' label-warning' : '';
            var content = '';
            if (response.Operacione.length > 0) {
                content += '<ul class=\'unstyled\'>';
                content += '    <li>Actividades';
                content += '        <ul>';
                for (i = 0; i < response.Operacione.length; i++) {
                    content += '        <li><a href=\'' + BASE_URL + '/actuacionesfiscales/details/' + response.Operacione[i].Operacione.actuacionesfiscale_id + '#operaciones' + response.Operacione[i].Operacione.evento_id + '\'>' + response.Operacione[i].Evento.denominacion + '</a>. <small>' + response.Operacione[i].Operacione.observaciones + '</small></li>';
                }
                content += '        </ul>';
                content += '    </li>';
                content += '</ul>';
            }
            if (response.Eventualidade.length > 0) {
                content += '<ul class=\'unstyled\'>';
                content += '    <li>Eventualidades';
                content += '        <ul>';
                for (i = 0; i < response.Eventualidade.length; i++) {
                    content += '        <li><a href=\'' + BASE_URL + '/actuacionesfiscales/details/' + response.Eventualidade[i].Eventualidade.actuacionesfiscale_id + '#eventualidades' + response.Eventualidade[i].Eventualidade.evento_id + '\'>' + response.Eventualidade[i].Eventualidade.asunto + '</a></li>';
                }
                content += '        </ul>';
                content += '    </li>';
                content += '</ul>';
            }
            if (response.Prorroga.length > 0) {
                content += '<ul class=\'unstyled\'>';
                content += '    <li>Prórrogas';
                content += '        <ul>';
                for (i = 0; i < response.Prorroga.length; i++) {
                    content += '        <li><a href=\'' + BASE_URL + '/prorrogas\'>' + response.Prorroga[i].Prorroga.asunto+ '</li>';
                }
                content += '        </ul>';
                content += '    </li>';
                content += '</ul>';
            }
            if (response.Operacione.length == 0 && response.Eventualidade.length == 0 && response.Prorroga.length == 0) {
                content += '<strong>Excelente!</strong> Ha cumplido todos sus pendientes';
            }
            $('#pending-container').html('<span class="label'+type+'" id="pending-alert" title="Pendientes" data-content="'+content+'">'+response.count+'</span>');
            $( "#pending-alert" ).popover({html:true, trigger: 'click', placement: 'bottom'});
            if ($(".tab-pane.active .evento #operaciones-evento").length) {
                $("div[class='tab-pane active'] #operaciones-evento").load($("div[class='tab-pane active'] .update-operaciones").attr("href"));
            }
            if ($(".tab-pane.active .evento #eventualidades-evento").length) {
                $("div[class='tab-pane active'] #eventualidades-evento").load($("div[class='tab-pane active'] .update-eventualidades").attr("href"));
            }
        }
    }
    
    $(".bar").popover({html:true, trigger: 'hover', placement: 'bottom'});
    
    /**
     * Si existe el contenedor de pendientes iniciamos el comet
     */
    if ($('#pending-container').length) {
        var comet = new Comet($('#pending-container').attr('alt'));
        comet.connect();
    }
    
    /**
     * Cancelar, funcionamiento
     */
    $("button.cancel").click(function () {
        history.go(-1);
    })
    
    /**
     * Al cambiar los valores del reporte consolidado (Filtros)
     */
    $("#ReporteConsolidadoForm input, #ReporteConsolidadoForm select").change(function () { $("#ReporteConsolidadoForm").submit() });
    
    
    /**
     * Al agregar un nuevo formulario se clona el elemento ultimo de los funcionarios
     * y luego se cambian algyunos valores para no hacer repetitivo todo
     */
    $("#add-funcionario").click(function (e) {
        // Evitando que se ejecute el linl
        e.preventDefault();
        
        if ($(this).attr("disabled") == "disabled") {
            return;
        }
        
        $(this).attr("disabled", "disabled");
        
        // Capturando la ultima linea y clonandola
        line = $("#equipo-autidor div.row-fluid")[$("#equipo-autidor div.row-fluid").length - 1];
        line = $(line).clone(false);
        
        // Agregando una clase nueva para identificar el nuevo funcionario
        $(line).addClass("new");
        
        // Recorriendo todos los elementos de la linea para cambiar su name y su id
        i = 0;
        $.each($(line).find(".control-group select, .control-group input"), function () {
            // Si es el 1er elemento es el rol y lo ponemos apoyo por defecto
            if (i == 0) {
                $(this).val(6);
            } else {
                $(this).val('');
            }
            
            // Obteniendo el ultimo id
            n = this.name.match(/\d+/);
            
            // Incrementado el mismo
            n++;
            
            // Estableciendo los nuevos name e id
            this.name = this.name.replace(/\d+/, n);
            this.id = this.id.replace(/\d+/, n);
            if ($(this).attr("data-changer")) {
                $(this).attr("data-changer", $(this).attr("data-changer").replace(/\d+/, n))
            }
            if ($(this).hasClass("date")) {
                id = this.id;
                $(this).removeAttr("id")
                $(this).removeClass("hasDatepicker")
                this.id = id;
                $(this).datepicker({
                    // Desactivando fines de semana y feriados
                    beforeShowDay: noWeekendsOrHolidays,
                    dateFormat: 'dd-mm-yy',
                    onSelect: function () {
                        $(this).focus()
                    }
                });
            }
            i++
        })
        
        // Agregando la nueva linea
        $("#equipo-autidor").append(line);
    
        $("a.del-funcionario").on("click", function (e) {
            e.preventDefault();
            if (confirm('Esta seguro que desea eliminar este registro?')) {
                var elemento = $(this).parent().parent()
                if ($(elemento).hasClass('new')) {
                    $("#add-funcionario").removeAttr("disabled");
                }
                $(elemento).remove()
            }
            $(this).removeClass("disabled");
        })
        
        // Para que no aparezca bloqueado siempre
        $(this).removeClass("disabled");
        $('.mask-nd').mask(MASK_ND);
        validate_values_equipo();
        changer_roles(6);
        //funcionarios_restrict();
        //$("#equipo-autidor .funcionario").each(function () { $(this).trigger("change"); })
    })
    
    function validate_values_equipo() {
        $("#equipo-autidor div.new select.funcionario").change(function () {
            $('#nueva-credencial #CredencialeFuncionarioId').val(this.value)
        });
        
        $("#equipo-autidor div.new select, #equipo-autidor div.new input").focusout(function () {
            var completado = true;
            $("#equipo-autidor div.new select, #equipo-autidor div.new input").each(function () {
                if (!$(this).val() && !$(this).hasClass("empty")) {
                    completado = false;
                }
            })
            if (completado) {
                if ($('#nueva-credencial').length) {
                    // Si la ventana existe es porque esta editando
                    $('#nueva-credencial').modal( "toggle" )
                } else {
                    // De lo contrario se permite seguir agregando gnete
                    $("#add-funcionario").removeAttr("disabled");
                }
            }
        })
    }
    
    $(".close-credencial-modal").click(function (e) {
        e.preventDefault();
        $('#nueva-credencial').modal( "toggle" )
        $(this).removeClass("disabled")
    })
    
    $(".continue-credencial-modal").click(function (e) {
        e.preventDefault();
        $.post($('#CredencialeAddForm').attr("action"), $('#CredencialeAddForm').serialize(), function (data) {
            if (data) {
                $('#nueva-credencial').modal( "toggle" )
                $(".empty")[$(".empty").length - 1].value = data
                $("#add-funcionario").removeAttr("disabled");
                $('#CredencialeAddForm').reset()
            }
        })
    })
    
    $("#ActuacionesfiscaleEditForm").submit(function (e) {
        var completed = 0;
        $(".credencial").each(function () {
            if (this.value) {
                completed++
            }
        })
        if ($('#nueva-credencial').length && $(".credencial").length != completed) {
            e.preventDefault();
            alert("Debe indicar el oficio credencial del(los) nuevo(s) funcionario(s)");
            $("#equipo-autidor div.new .mask-nd").focus();
            $(".form-actions button").removeAttr("disabled");
        }
    })
    
    
    validate_values_equipo();
    
    $("a.del-funcionario").on("click", function (e) {
        e.preventDefault();
        if (confirm('Esta seguro que desea eliminar este registro?')) {
            if ($("#ActuacionesfiscaleEditForm").length) {
                var success = false;
                $.post(this.href, function (data) { success = data });
            }
            if (success == '1') {
                $(this).parent().parent().remove()
            }
        }
        $(this).removeClass("disabled");
    })
    
    $(".operaciones #CredencialeAddForm").submit(function (e) {
        e.preventDefault();
        $.post(this.action, $(this).serialize(), function (data) {
            if (data) {
                $(".operaciones #CredencialeAddForm").addClass("hidden")
                $("#OperacioneAddForm").removeClass("hidden")
            }
        })
    })
    
    if ($("#objeto_id").length) {
        source = new Array();
        $("#ActuacionesfiscaleObjetoId option").each(function () {
            source.push(this.text)
        });
        $("#objeto_id").typeahead({source: source})
        
        $("#objeto_id").change(function () {
            if ($("#ActuacionesfiscaleObjetoId option:contains('" + $(this).val() + "')").length) {
                $("#ActuacionesfiscaleObjetoId option:contains('" + $(this).val() + "')")[0].selected = true
            }
        })
    }
    
    if ($("#ActuacionesfiscaleEditForm").length) {
        $("#objeto_id").val($("#ActuacionesfiscaleObjetoId option:selected").text()).trigger("change")
    }
    
    /**
     * Cambiador de funcionario roles, y busca los funcionarios asociados a su rol
     */
    function changer_roles(val) {
        if (val) {
            $(".funcionariorole-changer").change(function () {
                findFuncionarioRoles($(this).val());
            })
            findFuncionarioRoles(val);
        } else {
            if ($(".funcionariorole-changer").length) {
                findFuncionarioRoles();
            }
            
            $(".funcionariorole-changer").change(function () {
                findFuncionarioRoles($(this).val());
            })
        }
    }
    changer_roles();
    
    function findFuncionarioRoles(val) {
        var changers = $(".funcionariorole-changer");
        if (val) {
            changers = $(".funcionariorole-changer option[value=" + val + "]:selected").parent()
        }
        $(changers).each(function () {
            var target = $(this).attr("data-changer")
            var aux_val = $(target).val();
            $(target).empty();
            $(target).append($("<option>").val("").text("-Seleccione-"))
            url = $(this).attr("alt") + '/' + this.value + '/' + $("#ActuacionesfiscaleDireccioncontroleId").val();
            $.ajax({
                url: url,
                dataType: 'json',
                async: false,
                success: function (data) {
                    for (k in data) {
                        option = $("<option>").val(k).text(data[k])
                        if (k == aux_val) {
                            $(option).attr("selected", "selected");
                        }
                        $(target).append(option)
                    }
                }
            })
        })
        funcionarios_restrict();
    }
    
    $("#ActuacionesfiscaleDireccioncontroleId").change(function () {
        if ($(this).val()) {
            $("#equipo-autidor select.funcionario").removeAttr("disabled");
            changer_roles();
        } else {
            $("#equipo-autidor select.funcionario").attr("disabled", "disabled");
        }
    })
    
    function funcionarios_restrict() {
        // Aqui establece el valor actual en su alt
        $("#equipo-autidor .funcionario").each(function () {
            $(this).attr("alt", $(this).val());
        })
        $("#equipo-autidor .funcionario").change(function () {
            $("#equipo-autidor .funcionario option").removeClass("hidden");
            $(this).attr("alt", $(this).val())
            var selected = new Array();
            $("#equipo-autidor .funcionario option:selected").each(function () {
                selected.push(this.value);
            })
            
            for (k in selected) {
                $("#equipo-autidor .funcionario[alt!=" + selected[k] + "] option[value=" + selected[k] + "]").addClass("hidden")
            }
        })
        $("#equipo-autidor .funcionario").each(function () { $(this).trigger("change"); })
    }
    funcionarios_restrict();
    
    /**
     * Mascaras
     */
    $('.mask-af').mask(MASK_AF);
    $('.mask-nd').mask(MASK_ND);
    $('.mask-oc').mask(MASK_OC);
})