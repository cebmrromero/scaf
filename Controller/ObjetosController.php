<?php
App::uses('AppController', 'Controller');
/**
 * Objetos Controller
 *
 * @property Objeto $Objeto
 */
class ObjetosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Objeto->recursive = 0;
		$this->set('objetos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Objeto->exists($id)) {
			throw new NotFoundException(__('Invalid objeto'));
		}
		$this->Objeto->recursive = 2;
		$options = array('conditions' => array('Objeto.' . $this->Objeto->primaryKey => $id));
		$this->set('objeto', $this->Objeto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Objeto->create();
			if ($this->Objeto->save($this->request->data)) {
				$this->Session->setFlash(__('Objeto guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Objeto no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$objetotipos = $this->Objeto->Objetotipo->find('list');
		$this->set(compact('objetotipos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Objeto->exists($id)) {
			throw new NotFoundException(__('Invalid objeto'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Objeto->save($this->request->data)) {
				$this->Session->setFlash(__('El objeto ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El objeto no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Objeto.' . $this->Objeto->primaryKey => $id));
			$this->request->data = $this->Objeto->find('first', $options);
		}
		$objetotipos = $this->Objeto->Objetotipo->find('list');
		$this->set(compact('objetotipos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Objeto->id = $id;
		if (!$this->Objeto->exists()) {
			throw new NotFoundException(__('Invalid objeto'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Objeto->delete()) {
			$this->Session->setFlash(__('Objeto eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Objeto no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
