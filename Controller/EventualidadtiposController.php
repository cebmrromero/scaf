<?php
App::uses('AppController', 'Controller');
/**
 * Eventualidadtipos Controller
 *
 * @property Eventualidadtipo $Eventualidadtipo
 */
class EventualidadtiposController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Eventualidadtipo->recursive = 0;
		$this->set('eventualidadtipos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Eventualidadtipo->exists($id)) {
			throw new NotFoundException(__('Invalid eventualidadtipo'));
		}
		$eventualidadestados = array('NR' => 'No revisada', 'A' => 'Aprobada', 'N' => 'Negada', 'R' => 'Revisada');
		$this->Eventualidadtipo->recursive = 2;
		$options = array('conditions' => array('Eventualidadtipo.' . $this->Eventualidadtipo->primaryKey => $id));
		$eventualidadtipo = $this->Eventualidadtipo->find('first', $options);
		$this->set(compact('eventualidadtipo', 'eventualidadestados'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Eventualidadtipo->create();
			if ($this->Eventualidadtipo->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de eventualidad ha sido guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de eventualidad no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$eventos = $this->Eventualidadtipo->Evento->find('list');
		$this->set(compact('eventos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Eventualidadtipo->exists($id)) {
			throw new NotFoundException(__('Invalid eventualidadtipo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Eventualidadtipo->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de eventualidad ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de eventualidad no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Eventualidadtipo.' . $this->Eventualidadtipo->primaryKey => $id));
			$this->request->data = $this->Eventualidadtipo->find('first', $options);
		}
		$eventos = $this->Eventualidadtipo->Evento->find('list');
		$this->set(compact('eventos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Eventualidadtipo->id = $id;
		if (!$this->Eventualidadtipo->exists()) {
			throw new NotFoundException(__('Invalid eventualidadtipo'));
		}
		$eventualidadtipo = $this->Eventualidadtipo->read(null, $id);
		$this->request->onlyAllow('post', 'delete');
		if ($eventualidadtipo['Eventualidadtipo']['bloqueado']) {
			$this->Session->setFlash(__('Registro bloqueado, no puede eliminarse'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Eventualidadtipo->delete()) {
			$this->Session->setFlash(__('Tipo de eventualidad eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tipo de eventualidad no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
