<?php
App::uses('AppController', 'Controller');
/**
 * Direccioncontroles Controller
 *
 * @property Direccioncontrole $Direccioncontrole
 */
class DireccioncontrolesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->paginate = array(
			'limit' => 5
		);
		$this->Direccioncontrole->recursive = 0;
		$this->set('direccioncontroles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Direccioncontrole->exists($id)) {
			throw new NotFoundException(__('Invalid direccioncontrole'));
		}
		$this->Direccioncontrole->recursive = 2;
		$options = array('conditions' => array('Direccioncontrole.' . $this->Direccioncontrole->primaryKey => $id));
		$this->set('direccioncontrole', $this->Direccioncontrole->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Direccioncontrole->create();
			if ($this->Direccioncontrole->save($this->request->data)) {
				$this->Session->setFlash(__('La Dirección de Control ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La Dirección de Control no pudo ser guardada. Intente de neuvo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Direccioncontrole->exists($id)) {
			throw new NotFoundException(__('Invalid direccioncontrole'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Direccioncontrole->save($this->request->data)) {
				$this->Session->setFlash(__('La Dirección de Control ha sido actualizada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La Dirección de Control no pudo ser actualizada. Intente de neuvo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Direccioncontrole.' . $this->Direccioncontrole->primaryKey => $id));
			$this->request->data = $this->Direccioncontrole->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Direccioncontrole->id = $id;
		if (!$this->Direccioncontrole->exists()) {
			throw new NotFoundException(__('Invalid direccioncontrole'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Direccioncontrole->delete()) {
			$this->Session->setFlash(__('La Dirección de Control ha sido eliminada existosamente'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('La Dirección de Control no pudo ser eliminada. Intente de neuvo.'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
