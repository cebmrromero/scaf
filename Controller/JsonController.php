<?php
App::uses('AppController', 'Controller');
/**
 * Actuacionesfiscales Controller
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 */
class JsonController extends AppController {
    var $uses = array();

/**
 * getPending method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function getPending() {
        $this->loadModel('Actuacionesfiscale');
		$auth_user = $this->Session->read("Auth.User");
		$query = array(
			'conditions' => array(
				'Operacione.frecibe_id' => $auth_user['funcionario_id'],
				'Operacione.es_recibido' => 0,
			)
		);
		$this->Actuacionesfiscale->Operacione->recursive = 0;
		$operaciones = $this->Actuacionesfiscale->Operacione->find("all", $query);
		$noperaciones = sizeof($operaciones);
		
		$query = array(
			'conditions' => array(
				'Eventualidade.corrige_id' => $auth_user['funcionario_id'],
				'Eventualidade.estado' => 'NR',
			)
		);
		$this->Actuacionesfiscale->Eventualidade->recursive = -1;
		$eventualidades = $this->Actuacionesfiscale->Eventualidade->find("all", $query);
		$neventualidades = sizeof($eventualidades);
		
		$query = array(
			'conditions' => array(
				'Prorroga.aprueba_id' => $auth_user['funcionario_id'],
				'Prorroga.es_aprobada' => '2',
			)
		);
		$this->Actuacionesfiscale->Prorroga->recursive = -1;
		$prorrogas = $this->Actuacionesfiscale->Prorroga->find("all", $query);
		$nprorrogas = sizeof($prorrogas);
		
		$count = $noperaciones + $neventualidades + $nprorrogas;
		
		$data = array(
			'count' => $count,
			'Operacione' => $operaciones,
			'Eventualidade' => $eventualidades,
			'Prorroga' => $prorrogas,
		);

		$this->layout = false;
		echo json_encode($data);
		exit(1);
	}
	
	
	public function getOperacionesEvento($actuacionesfiscale_id, $evento_id) {
		$this->loadModel("Operacione");
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$auth_user = $this->getAuthUser();
		$query = array();
		$query['conditions'] = array(
			'Operacione.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'Operacione.evento_id' => $evento_id
		);
		$operaciones = $this->Operacione->find("all", $query);
		
		$m=0;
		foreach ($operaciones as $operacione) {
			// Calculo de las fechas
			$dh = parent::getDiasHabiles($operacione['Operacione']['fecha_remite'], $operacione['Operacione']['fecha_recibe'], array_values($diasferiados));
			$operaciones[$m]['Operacione']['diff_recepcion'] = $dh['ndias'];
				
			$operaciones[$m]['Operacione']['diff_remision'] = null;
			if (isset($operaciones[$m - 1]['Operacione']['fecha_remite'])) {
				$dh = parent::getDiasHabiles($operaciones[$m - 1]['Operacione']['fecha_recibe'], $operacione['Operacione']['fecha_remite'], array_values($diasferiados));
				$operaciones[$m]['Operacione']['diff_remision'] = $dh['ndias'];
			}
			$m++;
		}
		
		$this->layout = false;
		$this->set(compact("auth_user", "operaciones", "actuacionesfiscale_id", "evento_id"));
	}
	public function getEventualidadesEvento($actuacionesfiscale_id, $evento_id) {
		$this->loadModel("Eventualidade");
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$auth_user = $this->getAuthUser();
		$query = array();
		$query['conditions'] = array(
			'Eventualidade.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'Eventualidade.evento_id' => $evento_id
		);
		$eventualidades = $this->Eventualidade->find("all", $query);
		$i = 0;
		foreach ($eventualidades as $eventualidade) {
			// Calculo de las fechas
			$dh = parent::getDiasHabiles($eventualidade['Eventualidade']['fecha_deteccion'], $eventualidade['Eventualidade']['fecha_verificacion'], array_values($diasferiados));
			$eventualidades[$i]['Eventualidade']['diff_verificacion'] = $dh['ndias'];
			$i++;
		}
		$this->layout = false;
		$this->set(compact("auth_user", "eventualidades", "actuacionesfiscale_id", "evento_id"));
	}
    
    public function getNolaborables() {
        $this->loadModel('Nolaborable');
        $this->loadModel('Eventualidade');
        
        $nolaborables = $this->Nolaborable->getDias();
        echo json_encode(array_values($nolaborables));
        exit(1);
    }
	
	public function getDiasHabilesJson() {
		$fechainicio = (isset($this->request->query['fechainicio'])) ? $this->request->query['fechainicio'] : null;
		$fechafin = (isset($this->request->query['fechafin'])) ? $this->request->query['fechafin'] : null;
		$diasferiados = (isset($this->request->query['diasferiados'])) ? $this->request->query['diasferiados'] : null;
		$this->layout = false;
		echo json_encode(parent::getDiasHabiles($fechainicio,$fechafin, $diasferiados));
        exit(1);
	}
	
	public function getFuncionariosByRol($funcionariorole_id = null, $direccioncontrole_id = null) {
		$this->loadModel('Funcionario');
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'LEFT',
				'conditions' => array(
					'User.funcionario_id = Funcionario.id'
				)
			),
			array('table' => 'groups',
				'alias' => 'Group',
				'type' => 'LEFT',
				'conditions' => array(
					'User.group_id = Group.id'
				)
			),
			array('table' => 'funcionarioroles_groups',
				'alias' => 'FuncionariorolesGroup',
				'type' => 'LEFT',
				'conditions' => array(
					'FuncionariorolesGroup.group_id = Group.id'
				)
			),
			array('table' => 'funcionarioroles',
				'alias' => 'Funcionariorole',
				'type' => 'LEFT',
				'conditions' => array(
					'FuncionariorolesGroup.funcionariorole_id = Funcionariorole.id'
				)
			),
		);
		if ($funcionariorole_id == 6) {
			$query['conditions'] = array(
				'Funcionariorole.id' => $funcionariorole_id
			);
		} else {
			$query['conditions'] = array(
				'Funcionariorole.id' => $funcionariorole_id,
				'Funcionario.direccioncontrole_id' => $direccioncontrole_id,
			);
		}
		$query['order'] = "Funcionario.nombre";
		$funcionarios = $this->Funcionario->find("list", $query);
		echo json_encode($funcionarios);
		exit(0);
	}
}