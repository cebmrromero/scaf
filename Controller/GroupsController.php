<?php
App::uses('AppController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 */
class GroupsController extends AppController {
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Group->recursive = 0;
		$this->set('groups', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}
		$this->Group->recursive = 2;
		$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
		$this->set('group', $this->Group->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('Grupo guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Grupo no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('Grupo actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Grupo no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$this->request->data = $this->Group->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	//public function delete($id = null) {
	//	$this->Group->id = $id;
	//	if (!$this->Group->exists()) {
	//		throw new NotFoundException(__('Invalid group'));
	//	}
	//	$this->request->onlyAllow('post', 'delete');
	//	if ($this->Group->delete()) {
	//		$this->Session->setFlash(__('Grupo eliminado'), 'flash_custom', array('class' => 'alert-success'));
	//		$this->redirect(array('action' => 'index'));
	//	}
	//	$this->Session->setFlash(__('Grupo no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
	//	$this->redirect(array('action' => 'index'));
	//}
}
