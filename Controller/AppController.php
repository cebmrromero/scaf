<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            'authError' => '<div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Error!</strong> No tiene suficientes permisos para acceder
                            </div>'
        ),
        'Session',
        //'DebugKit.Toolbar',
		'FilterResults.Filter' => array(
        'auto' => array(
            'paginate' => true,
            'explode'  => true,  // recommended
        ),
        'explode' => array(
            'character'   => ' ',
            'concatenate' => 'AND',
        )
    )
    );
    
    public $helpers = array(
		'Html', 'Form', 'Session', 'Time',
		'FilterResults.Search' => array(
			'operators' => array(
				'LIKE'       => 'containing',
				'NOT LIKE'   => 'not containing',
				'LIKE BEGIN' => 'starting with',
				'LIKE END'   => 'ending with',
				'='  => 'equal to',
				'!=' => 'different',
				'>'  => 'greater than',
				'>=' => 'greater or equal to',
				'<'  => 'less than',
				'<=' => 'less or equal to'
			)
		)
	);
    
    public function beforeFilter() {
        //Configure AuthComponent
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('controller' => 'actuacionesfiscales', 'action' => 'index');
        $this->Auth->allow('display');
    }
    
	
	/**
	 * Metodo getDiasHabiles
	 * 
	 * Permite devolver un arreglo con los dias habiles
	 * entre el rango de fechas dado excluyendo los
	 * dias feriados dados (Si existen)
	 * 
	 * @param string $fechainicio Fecha de inicio en formato Y-m-d
	 * @param string $fechafin Fecha de fin en formato Y-m-d
	 * @param array $diasferiados Arreglo de dias feriados en formato Y-m-d
	 * @return array $diashabiles Arreglo definitivo de dias habiles
	 */
	public function getDiasHabiles($fechainicio, $fechafin, $diasferiados = array()) {
        // Validacion por seguridad
        if (is_null($diasferiados)) {
            $diasferiados = array();
        }
        // Numero de dias a devolver
        $ndias = 0;
        
        // Incremento en 1 dia
        $diainc = 24*60*60;
        
        // Arreglo de dias habiles, inicianlizacion
        $diashabiles = array();
        
        if ($fechainicio && $fechafin) {
            // Convirtiendo en timestamp las fechas, es dia habil, a partir del siguiente, por lo tanto
            // se le suma un dia, a la fecha de inicio
            $fechainicio = strtotime($fechainicio) + $diainc;
            $fechafin = strtotime($fechafin);
            
            if ($fechainicio <= $fechafin) {
                // Se recorre desde la fecha de inicio a la fecha fin, incrementando en 1 dia
                for ($midia = $fechainicio; $midia <= $fechafin; $midia += $diainc) {
                    if ($this->esDiaHabil($midia, $diasferiados)) {
                        array_push($diashabiles, date('Y-m-d', $midia));
                    }
                }
                $ndias = sizeof($diashabiles);
            }
        }
        return array('Diashabiles' => $diashabiles, 'ndias' => $ndias);
	}
    
    /**
     * Determina si el dia es habil
     * @param timestamp $midia Timestap del dia
     * @param array $diasferiados Arreglo de dias feriados en formato Y-m-d
     */
    public function esDiaHabil($midia, $diasferiados) {
        // Si el dia indicado, no es sabado o domingo es habil
        if (!in_array(date('N', $midia), array(6,7))) {
            // Si no es un dia feriado entonces es habil
            if (!in_array(date('Y-m-d', $midia), $diasferiados) && !in_array(date('m-d', $midia), $diasferiados)) {
                return true;
            }
        }
        return false;
    }
    
    public function getFechaFinFromDH($fecha_inicio, $max_dias_habiles, $diasferiados) {
        // Fecha inicio igual al auxiliar
        $aux = $fi = strtotime($fecha_inicio);
        
        // Incremento de 1 dia
		$diainc = 24*60*60;
        
        // Acumuladores
		$i = 0;
		$dh = 0;
        $atrazo = 0;
        
        // Mientras el numero de dias habiles detectados sea menor al maximo dias habiles
		while ($dh < $max_dias_habiles) {
            // Incremento en 1 dia
			$aux += $diainc;
            
            // Si el dia es habil, se cuenta y se define como ultimo dia
			//echo date('Y-m-d', $aux);
			//echo (in_array($aux, array_values($diasferiados))) ? 'SI' : 'NO';echo "<br />";
			if ($this->esDiaHabil($aux, array_values($diasferiados))) {
				//echo date('Y-m-d', $aux) . " SI<br />";
				$ff = $aux;
				$dh++;
			}
			$i++;
		}
        $hoy = strtotime(date('Y-m-d'));
        if ($hoy > $ff) {
            $atrazo = $this->getDiasHabiles(date("Y-m-d", $ff),date("Y-m-d", $hoy), array_values($diasferiados));
        }
        
        return array("fecha_inicio" => date("Y-m-d", $fi), "fecha_fin" => date("Y-m-d", $ff), "ndias" => $dh, 'atrazo' => $atrazo['ndias']);
    }
    
    public function getFechasFromDH($fecha_inicio, $max_dias_habiles, $diasferiados) {
        // Fecha inicio igual al auxiliar
        $aux = $fi = strtotime($fecha_inicio);
        
        // Incremento de 1 dia
		$diainc = 24*60*60;
        
        // Acumuladores
		$dh = 0;
        $atrazo = 0;
		
		$dias = array();
        
        // Mientras el numero de dias habiles detectados sea menor al maximo dias habiles
		while ($dh < $max_dias_habiles) {
            // Incremento en 1 dia
			$aux += $diainc;
			
            // Si el dia es habil, se cuenta y se define como ultimo dia
			if ($this->esDiaHabil($aux, array_values($diasferiados))) {
				$dias[] = date('Y-m-d', $aux);
				$dh++;
			}
		}
        
        return $dias;
    }
    
    
    /**
     * Obteniendo el usuario y datos asociados que se necesitan
     */
    public function getAuthUser() {
        $auth_user = $this->Session->read("Auth.User");
        $this->loadModel("Funcionario");
        
        $query = array(
            'conditions' => array(
                'Funcionario.id' => $auth_user['funcionario_id']
            )
        );
        $this->Funcionario->recursive = 0;
        $funcionario = $this->Funcionario->find("first", $query);
        $auth_user += $funcionario;
        
        return $auth_user;
    }
	
	public function runUnauthorized() {
		$this->Session->setFlash(__('No tiene permisos para realizar la acción'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect('/');
	}
	
	public function getOtrosNolaborablesByAF($actuacionesfiscale_id) {
		$this->loadModel("Eventualidade");
		// Obteniendo los dias no laborables como eventualidades
		$query['conditions'] = array(
			'Eventualidade.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'Eventualidade.estado' => 'A',
			'Eventualidade.eventualidadtipo_id IN' => array(Configure::read('App.EVENTUALIDAD_NOLABORABLE'), Configure::read('App.EVENTUALIDAD_DIFERIMIENTO'))
		);
		$query['fields'] = array("Eventualidade.fecha_deteccion", "Eventualidadtipo.numero_dh", "Evento.fase_id");
		return $this->Eventualidade->find("all", $query);
	}
	
	public function getActuacionesfiscaleDetails($actuacionesfiscale, $fases, $nolaborables_todos) {
		$data = array();
		
		// Inicializando el arreglo de la data
		foreach(array_keys($fases) as $fase) {
			$data[$fase] = array(
				'dias_ejecutados' => 0,
				'eventualidades_ndias' => 0,
			);
		}
		
		//$data = $this->getDiasEjecutadosByEventosAF($actuacionesfiscale['Evento'], $data, $fases, $nolaborables_todos);
		$data = $this->getFechasByFases($actuacionesfiscale['Actuacionesfiscale'], $data, $fases);
		$data = $this->getProrrogasByAF($actuacionesfiscale['Actuacionesfiscale'], $data, $fases, $nolaborables_todos);
		$data = $this->getFechasDiff($actuacionesfiscale['Actuacionesfiscale'], $data, $fases, $nolaborables_todos);
		//print_r($data);
		return $data;
	}
	
	public function getFechasDiff($actuacionesfiscale, $data, $fases, $nolaborables_todos) {
		foreach(array_keys($fases) as $fase) {
			$data[$fase]['fechas_limites'] = array();
		}
		foreach(array_keys($fases) as $fase) {
			if ($fase == 1) {
				$fi = ($actuacionesfiscale['fecha_inicio']) ? $actuacionesfiscale['fecha_inicio'] : $actuacionesfiscale['fecha_inicio_estimada'];
			}
			$data[$fase]['fechas_limites'] = $this->getFechaFinFromDH($fi, $actuacionesfiscale['dh_planificacion'] + $data[$fase]['eventualidades_ndias'] + $data[$fase]['prorrogas_ndias'], $nolaborables_todos);
			$fi = $data[$fase]['fechas_limites']['fecha_fin'];
			$data[$fase]['fechas_limites']['diff'] = $this->getDiasHabiles(date('Y-m-d'), $data[$fase]['fechas_limites']['fecha_fin'], $nolaborables_todos);
			$data[$fase]['fechas_limites']['diff'] = ($data[$fase]['fechas_limites']['diff']['ndias']) ? $data[$fase]['fechas_limites']['diff']['ndias'] - $data[$fase]['eventualidades_ndias'] : '';
		}
		
		return $data;
	}
	
	public function getProrrogasByAF($actuacionesfiscale, $data, $fases, $nolaborables_todos) {
		$this->loadModel("Prorroga");
		foreach(array_keys($fases) as $fase) {
			$data[$fase]['prorrogas'] = array();
		}
		$query = array();
		$query['conditions'] = array(
			'Prorroga.actuacionesfiscale_id' => $actuacionesfiscale['id'],
			'Prorroga.es_aprobada' => 1,
		);
		$query['fields'] = array("Prorroga.ndias", "Prorroga.fecha_solicitud", "Prorroga.fase_id");
		$dprorrogas = $this->Prorroga->find("all", $query);
		foreach ($dprorrogas as $dprorroga) {
			$data[$dprorroga['Prorroga']['fase_id']]['prorrogas'][] = $this->getFechaFinFromDH($dprorroga['Prorroga']['fecha_solicitud'], $dprorroga['Prorroga']['ndias'], $nolaborables_todos);
		}
		foreach(array_keys($fases) as $fase) {
			$data[$fase]['prorrogas_ndias'] = 0;
			foreach ($data[$fase]['prorrogas'] as $prorroga) {
				$data[$fase]['prorrogas_ndias'] += $prorroga['ndias'];
			}
		}
		return $data;
	}
	
	public function getFechasByFases($actuacionesfiscale, $data, $fases) {
		$otrosnolaborable = $this->getOtrosNolaborablesByAF($actuacionesfiscale['id']);
		foreach ($otrosnolaborable as $e) {
			foreach(array_keys($fases) as $fase) {
				if ($e['Evento']['fase_id'] == $fase) {
					$data[$fase]['eventualidades_ndias'] += $e['Eventualidadtipo']['numero_dh'];
				}
			}
		}
		return $data;
	}
	
	/**
	 * Devuelve un arreglo donde se ve los dias ejecutados por cada fase
	 */
	public function getDiasEjecutadosByEventosAF($eventos, $data, $fases, $nolaborables_todos) {
		foreach ($eventos as $evento) {
			$ff = null;
			$fi = null;
			if (isset($evento['Operacione'])) {
				$j = 0;
				foreach ($evento['Operacione'] as $operacione) {
					if ($j == 0) {
						$fi = $operacione['fecha_remite'];
						$j++;
					}
					if ($operacione['fecha_recibe']) {
						$ff = $operacione['fecha_recibe'];
					}
				}
				$dh = $this->getDiasHabiles($fi, $ff, $nolaborables_todos);
				foreach(array_keys($fases) as $fase) {
					if ($evento['fase_id'] == $fase) {
						$data[$fase]['dias_ejecutados'] += $dh['ndias'];
					}
				}
			}
		}
		return $data;
	}
}
