<?php
App::uses('AppController', 'Controller');
/**
 * Actuacionesorigenes Controller
 *
 * @property Actuacionesorigene $Actuacionesorigene
 */
class ActuacionesorigenesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->paginate = array(
			'limit' => 5
		);
		$this->Actuacionesorigene->recursive = 0;
		$this->set('actuacionesorigenes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Actuacionesorigene->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesorigene'));
		}
		$this->Actuacionesorigene->recursive = 2;
		$options = array('conditions' => array('Actuacionesorigene.' . $this->Actuacionesorigene->primaryKey => $id));
		$this->set('actuacionesorigene', $this->Actuacionesorigene->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Actuacionesorigene->create();
			if ($this->Actuacionesorigene->save($this->request->data)) {
				$this->Session->setFlash(__('El Orígen de la Actuación ha sido guardado existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Orígen de la Actuación no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Actuacionesorigene->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesorigene'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Actuacionesorigene->save($this->request->data)) {
				$this->Session->setFlash(__('El Orígen de la Actuación ha sido actualizado existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Orígen de la Actuación no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Actuacionesorigene.' . $this->Actuacionesorigene->primaryKey => $id));
			$this->request->data = $this->Actuacionesorigene->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Actuacionesorigene->id = $id;
		if (!$this->Actuacionesorigene->exists()) {
			throw new NotFoundException(__('Invalid actuacionesorigene'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Actuacionesorigene->delete()) {
			$this->Session->setFlash(__('El Orígen de la Actuación ha sido eliminado existosamente'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('El Orígen de la Actuación no pudo ser eliminado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
