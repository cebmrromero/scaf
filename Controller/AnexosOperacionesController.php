<?php
App::uses('AppController', 'Controller');
/**
 * AnexosOperaciones Controller
 *
 * @property AnexosOperacione $AnexosOperacione
 */
class AnexosOperacionesController extends AppController {
/**
 * recibidos method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function recibidos() {
		foreach($this->request->data['AnexosOperacione'] as $anexos_operacione) {
			if ($this->request->is('post') || $this->request->is('put')) {
				if ($this->AnexosOperacione->save($anexos_operacione)) {
					$success = 1;
				} else {
					$success = 0;
				}
			}
		}
		exit(0);
	}
}
