<?php
App::uses('AppController', 'Controller');
/**
 * Credenciales Controller
 *
 * @property Credenciale $Credenciale
 */
class CredencialesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Credenciale->recursive = 0;
		$this->set('credenciales', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Credenciale->exists($id)) {
			throw new NotFoundException(__('Invalid credenciale'));
		}
		$options = array('conditions' => array('Credenciale.' . $this->Credenciale->primaryKey => $id));
		$this->set('credenciale', $this->Credenciale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Credenciale->create();
			//$this->Credenciale->deleteAll(array('Credenciale.actuacionesfiscale_id' => $this->request->data['Credenciale']['actuacionesfiscale_id']));
			if ($this->Credenciale->save($this->request->data)) {
				$credenciale_id = $this->Credenciale->getLastInsertID();
				$this->loadModel("Equipotrabajo");
				$this->Equipotrabajo->recursive = -1;
				$equipotrabajos = $this->Equipotrabajo->findAllByActuacionesfiscaleId($this->request->data['Credenciale']['actuacionesfiscale_id']);
				for ($i = 0; $i < sizeof($equipotrabajos); $i++) {
					$equipotrabajos[$i]['Equipotrabajo']['credenciale_id'] = $credenciale_id;
					$this->Equipotrabajo->save($equipotrabajos[$i]);
				}
				echo $credenciale_id;
			}
		}
		exit(1);
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Credenciale->exists($id)) {
			throw new NotFoundException(__('Invalid credenciale'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Credenciale->save($this->request->data)) {
				$this->Session->setFlash(__('The credenciale has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The credenciale could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Credenciale.' . $this->Credenciale->primaryKey => $id));
			$this->request->data = $this->Credenciale->find('first', $options);
		}
		$actuacionesfiscales = $this->Credenciale->Actuacionesfiscale->find('list');
		$this->set(compact('actuacionesfiscales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Credenciale->id = $id;
		if (!$this->Credenciale->exists()) {
			throw new NotFoundException(__('Invalid credenciale'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Credenciale->delete()) {
			$this->Session->setFlash(__('Credenciale deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Credenciale was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
