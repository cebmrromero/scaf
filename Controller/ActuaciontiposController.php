<?php
App::uses('AppController', 'Controller');
/**
 * Actuaciontipos Controller
 *
 * @property Actuaciontipo $Actuaciontipo
 */
class ActuaciontiposController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Actuaciontipo->recursive = 0;
		$this->set('actuaciontipos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Actuaciontipo->exists($id)) {
			throw new NotFoundException(__('Invalid actuaciontipo'));
		}
		$this->Actuaciontipo->recursive = 2;
		$options = array('conditions' => array('Actuaciontipo.' . $this->Actuaciontipo->primaryKey => $id));
		$this->set('actuaciontipo', $this->Actuaciontipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Actuaciontipo->create();
			if ($this->Actuaciontipo->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de actuación ha sido guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de actuacion no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Actuaciontipo->exists($id)) {
			throw new NotFoundException(__('Invalid actuaciontipo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Actuaciontipo->save($this->request->data)) {
				$this->Session->setFlash(__('El tipo de actuación ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El tipo de actuación no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Actuaciontipo.' . $this->Actuaciontipo->primaryKey => $id));
			$this->request->data = $this->Actuaciontipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Actuaciontipo->id = $id;
		if (!$this->Actuaciontipo->exists()) {
			throw new NotFoundException(__('Invalid actuaciontipo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Actuaciontipo->delete()) {
			$this->Session->setFlash(__('Tipo de actuación eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tipo de actuación no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
