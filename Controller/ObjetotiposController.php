<?php
App::uses('AppController', 'Controller');
/**
 * Objetotipos Controller
 *
 * @property Objetotipo $Objetotipo
 */
class ObjetotiposController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Objetotipo->recursive = 0;
		$this->set('objetotipos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Objetotipo->exists($id)) {
			throw new NotFoundException(__('Invalid objetotipo'));
		}
		$options = array('conditions' => array('Objetotipo.' . $this->Objetotipo->primaryKey => $id));
		$this->set('objetotipo', $this->Objetotipo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Objetotipo->create();
			if ($this->Objetotipo->save($this->request->data)) {
				$this->Session->setFlash(__('Tipo de objeto guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Tipo de objeto no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Objetotipo->exists($id)) {
			throw new NotFoundException(__('Invalid objetotipo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Objetotipo->save($this->request->data)) {
				$this->Session->setFlash(__('Tipo de objeto actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Tipo de objeto no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Objetotipo.' . $this->Objetotipo->primaryKey => $id));
			$this->request->data = $this->Objetotipo->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Objetotipo->id = $id;
		if (!$this->Objetotipo->exists()) {
			throw new NotFoundException(__('Invalid objetotipo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Objetotipo->delete()) {
			$this->Session->setFlash(__('Tipo de objeto eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tipo de objeto no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
