<?php
App::uses('AppController', 'Controller');
/**
 * ActuacionesfiscalesEventos Controller
 *
 * @property ActuacionesfiscalesEvento $ActuacionesfiscalesEvento
 */
class ActuacionesfiscalesEventosController extends AppController {

/**
 * finalizar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function finalizar($actuacionesfiscale_id, $evento_id) {
		$evento = $this->ActuacionesfiscalesEvento->Evento->findById($evento_id);
		$this->ActuacionesfiscalesEvento->recursive = -1;
		$query['conditions'] = array(
			'ActuacionesfiscalesEvento.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'ActuacionesfiscalesEvento.evento_id' => $evento_id
		);
		$actuacionesfiscalesevento = $this->ActuacionesfiscalesEvento->find('first', $query);
		$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['es_completado'] = 1;
		$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['es_activo'] = 0;
		$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['fecha_fin'] = date('Y-m-d H:i:s');
		if ($this->ActuacionesfiscalesEvento->save($actuacionesfiscalesevento)) {
			// Buscando el siguiente evento para activarlo, es decir, el hijo 0 inmediato
			$query['conditions'] = array(
				'ActuacionesfiscalesEvento.evento_id' => $evento['Children'][0]['id'],
				'ActuacionesfiscalesEvento.actuacionesfiscale_id' => $actuacionesfiscale_id
			);
			$actuacionesfiscalesevento = $this->ActuacionesfiscalesEvento->find('first', $query);
			if (!$actuacionesfiscalesevento) {
				$this->ActuacionesfiscalesEvento->create();
				$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['actuacionesfiscale_id'] = $actuacionesfiscale_id;
				$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['evento_id'] = $evento['Children'][0]['id'];
				$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['es_activo'] = 1;
			} else {
				$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['es_activo'] = 1;
			}
			
			// Activando el siguiente evento
			$this->ActuacionesfiscalesEvento->save($actuacionesfiscalesevento);
			$this->Session->setFlash(__('La acción ha sido marcada como completada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La acción no pudo ser marcada como completada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}
}
