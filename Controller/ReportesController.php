<?php
App::uses('AppController', 'Controller');
/**
 * Reportes Controller
 *
 * @property Reporte $Reporte
 */
class ReportesController extends AppController {
    /**
     * Es un reporte sin modelo
     */ 
    var $uses = array();
    
    public function index() {

	}
    
	public function byStatus() {
        $this->loadModel("Actuacionesfiscale");
		$auth_user = $this->Session->read("Auth.User");
		$query['conditions'] = array(
			'Actuacionesfiscale.fecha_fin IS NULL'
		);
		$query['order'] = array(
			'Actuacionesfiscale.fecha_fin_estimada' => 'ASC'
		);
		$this->Actuacionesfiscale->recursive = 2;
		$actuacionesfiscales = $this->Actuacionesfiscale->find('all', $query);
		$this->set(compact('auth_user', 'actuacionesfiscales'));
	}
	
	public function byTime() {
        $this->loadModel("Actuacionesfiscale");
		$auth_user = $this->Session->read("Auth.User");
		$this->Actuacionesfiscale->recursive = 0;
		$actuacionesfiscales = $this->Actuacionesfiscale->find("all");
		
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$j = 0;
		foreach($actuacionesfiscales as $actuacionesfiscale) {
			// Obteniendo los dias no laborables como eventualidades
			$query = array();
			$query['conditions'] = array(
				'Eventualidade.actuacionesfiscale_id' => $actuacionesfiscale['Actuacionesfiscale']['id'],
				'Eventualidade.estado' => 'A',
				'Eventualidade.eventualidadtipo_id IN' => array(Configure::read('App.EVENTUALIDAD_NOLABORABLE'), Configure::read('App.EVENTUALIDAD_DIFERIMIENTO'))
			);
			$query['fields'] = array("Eventualidade.fecha_deteccion", "Eventualidadtipo.numero_dh", "Evento.fase_id");
			$otrosnolaborable = $this->Actuacionesfiscale->Eventualidade->find("all", $query);
			$nolaborables_todos = array_merge(array_values($diasferiados), array_values($otrosnolaborable));
			
			$this->Actuacionesfiscale->Operacione->virtualFields['diff'] = '-';
			$this->Actuacionesfiscale->Operacione->virtualFields['fecha_inicio'] = '-';
			$this->Actuacionesfiscale->Operacione->virtualFields['fecha_fin'] = '-';
			$dh = $this->getFechaFinFromDH(($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio']) ? $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio'] : $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_estimada'], $actuacionesfiscale['Actuacionesfiscale']['dh_planificacion'] + $actuacionesfiscale['Actuacionesfiscale']['dh_ejecucion'] + $actuacionesfiscale['Actuacionesfiscale']['dh_resultados'], $nolaborables_todos);
			$dh['diff'] = $this->getDiasHabiles(date('Y-m-d'), $dh['fecha_fin'], $nolaborables_todos);
			$dh['diff'] = ($dh['diff']['ndias']) ? $dh['diff']['ndias'] : '';
		
			//$dh = $this->getDiasHabiles(($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio']) ? $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio'] : $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_estimada'], ($actuacionesfiscale['Actuacionesfiscale']['fecha_fin']) ? $actuacionesfiscale['Actuacionesfiscale']['fecha_fin'] : $actuacionesfiscale['Actuacionesfiscale']['fecha_fin_estimada'], array_values($diasferiados));
			
			//print_r($otrosnolaborable);
			
			//$otrosnolaborable = Hash::combine($otrosnolaborable, '{n}.Eventualidadtipo.numero_dh', '{n}.Eventualidade.fecha_deteccion');
			//$i = 0;
			//$aux = array();
			$eventualidades_f1 = array();
			$eventualidades_f2 = array();
			$eventualidades_f3 = array();
			foreach ($otrosnolaborable as $e) {
				if ($e['Evento']['fase_id'] == 1) {
					$eventualidades_f1[] = array('fecha' => $e['Eventualidade']['fecha_deteccion'], 'ndias' => $e['Eventualidadtipo']['numero_dh']);
				}
				if ($e['Evento']['fase_id'] == 2) {
					$eventualidades_f2[] = array('fecha' => $e['Eventualidade']['fecha_deteccion'], 'ndias' => $e['Eventualidadtipo']['numero_dh']);
				}
				if ($e['Evento']['fase_id'] == 3) {
					$eventualidades_f3[] = array('fecha' => $e['Eventualidade']['fecha_deteccion'], 'ndias' => $e['Eventualidadtipo']['numero_dh']);
				}
				//$aux += $this->getFechasFromDH(date("Y-m-d", strtotime($v)), $k, array_values($diasferiados));
				//$i++;
			}
			$ediff = 0;
			foreach($eventualidades_f1 as $e) {
				$ediff += $e['ndias'];
			}
			foreach($eventualidades_f2 as $e) {
				$ediff += $e['ndias'];
			}
			foreach($eventualidades_f3 as $e) {
				$ediff += $e['ndias'];
			}
			$actuacionesfiscales[$j]['Actuacionesfiscale']['total_estimado'] = $dh['diff'] + $ediff;
			$query = "
				SELECT
					Actuacionesfiscale.id,
					Actuacionesfiscale.numero,
					Evento.id,
					Evento.denominacion,
					Operacione.id,
					(select x.fecha_remite from operaciones as x where x.actuacionesfiscale_id = Operacione.actuacionesfiscale_id and x.evento_id = Operacione.evento_id order by x.id ASC limit 1) AS Operacione__fecha_inicio,
					(select x.fecha_recibe from operaciones as x where x.actuacionesfiscale_id = Operacione.actuacionesfiscale_id and x.evento_id = Operacione.evento_id order by x.id DESC limit 1) AS Operacione__fecha_fin
				FROM
					actuacionesfiscales AS Actuacionesfiscale
				RIGHT JOIN actuacionesfiscales_eventos AS ActuacionesfiscalesEvento ON ActuacionesfiscalesEvento.actuacionesfiscale_id = Actuacionesfiscale.id
				RIGHT JOIN eventos AS Evento on ActuacionesfiscalesEvento.evento_id = Evento.id
				LEFT JOIN operaciones AS Operacione on Operacione.actuacionesfiscale_id = Actuacionesfiscale.id AND Operacione.evento_id = Evento.id
				WHERE Actuacionesfiscale.id = %s
				GROUP BY Actuacionesfiscale.id, Evento.id
				ORDER BY Actuacionesfiscale.id, Evento.id
			";
			$query = sprintf($query, $actuacionesfiscale['Actuacionesfiscale']['id']);
			$this->Actuacionesfiscale->Operacione->recursive = -1;
			$operaciones = $this->Actuacionesfiscale->Operacione->query($query);
			$dh = null;
			$i = 0;
			foreach ($operaciones as $operacione) {
				// Calculando los dias habiles utilizados
				$dh = $this->getDiasHabiles($operacione['Operacione']['fecha_inicio'], $operacione['Operacione']['fecha_fin'], array_values($diasferiados));
				$operaciones[$i]['Operacione']['diff'] = $dh['ndias'];
				
				// Calculando la diferencia de ejecucion entre un evento y el siguiente
				$operaciones[$i]['Operacione']['diff_ejecucion'] = null;
				if (isset($operaciones[$i - 1]['Operacione']['fecha_fin'])) {
					$dh = $this->getDiasHabiles($operaciones[$i - 1]['Operacione']['fecha_fin'], $operacione['Operacione']['fecha_inicio'], array_values($diasferiados));
					$operaciones[$i]['Operacione']['diff_ejecucion'] = $dh['ndias'];
				}
				
				// Definiendo los demas valores
				$actuacionesfiscales[$j]['Operacione'][$i] = $operaciones[$i]['Operacione'];
				$actuacionesfiscales[$j]['Operacione'][$i]['Evento'] = $operaciones[$i]['Evento'];
				
				// Si es el 1er ciclo, se captura la FI
				if ($i == 0) {
					$fi = $operacione['Operacione']['fecha_inicio'];
				}
				// Si hay fecha de fin, se establece la variable que la contiene
				if ($operacione['Operacione']['fecha_fin']) {
					$ff = $operacione['Operacione']['fecha_fin'];
				}
				$i++;
			}
			$dh = $this->getDiasHabiles($fi, $ff, $nolaborables_todos);
			// Obteniendo el tiempo de ejecucion total
			$actuacionesfiscales[$j]['Actuacionesfiscale']['diff_total'] = $dh['ndias'];
			
			// Estableciendo los valores para mejor visualizacion
			$actuacionesfiscales[$j]['Actuacionesfiscale']['fi'] = $fi;
			$actuacionesfiscales[$j]['Actuacionesfiscale']['ff'] = $ff;
			$j++;
		}
		$this->set(compact('auth_user', 'actuacionesfiscales'));
	}
	
	public function byTimeDetail($actuacionesfiscale_id = null, $evento_id = null) {
        $this->loadModel("Actuacionesfiscale");
		$auth_user = $this->Session->read("Auth.User");
		$query = array(
			'conditions' => array(
				'Operacione.actuacionesfiscale_id' => $actuacionesfiscale_id,
				'Operacione.evento_id' => $evento_id
			)
		);
		$actuacionesfiscale = $this->Actuacionesfiscale->find("first", array('conditions' => array('Actuacionesfiscale.id' => $actuacionesfiscale_id)));
		$evento = $this->Actuacionesfiscale->Operacione->Evento->find("first", array('conditions' => array('Evento.id' => $evento_id)));
		$operaciones = $this->Actuacionesfiscale->Operacione->find("all", $query);
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$i = 0;
		foreach($operaciones as $operacione) {
			$dh = $this->getDiasHabiles($operacione['Operacione']['fecha_remite'], $operacione['Operacione']['fecha_recibe'], array_values($diasferiados));
			$operaciones[$i]['Operacione']['diff_recepcion'] = $dh['ndias'];
				
			$operaciones[$i]['Operacione']['diff_remision'] = null;
			if (isset($operaciones[$i - 1]['Operacione']['fecha_remite'])) {
				$dh = $this->getDiasHabiles($operaciones[$i - 1]['Operacione']['fecha_recibe'], $operacione['Operacione']['fecha_remite'], array_values($diasferiados));
				$operaciones[$i]['Operacione']['diff_remision'] = $dh['ndias'];
			}
			$i++;
		}
		$this->set(compact('auth_user', 'operaciones', 'actuacionesfiscale', 'evento'));
	}
	
	public function consolidado() {
		$this->loadModel("Actuacionesfiscale");
		$this->Actuacionesfiscale->virtualFields['fecha_inicio_fase1'] = '';
		$this->Actuacionesfiscale->virtualFields['fecha_inicio_fase2'] = '';
		$this->Actuacionesfiscale->virtualFields['fecha_inicio_fase3'] = '';
		$this->Actuacionesfiscale->virtualFields['fecha_fin_fase1'] = '';
		$this->Actuacionesfiscale->virtualFields['fecha_fin_fase2'] = '';
		$this->Actuacionesfiscale->virtualFields['fecha_fin_fase3'] = '';
		$this->Actuacionesfiscale->virtualFields['programa_entregado'] = '';
		$this->Actuacionesfiscale->virtualFields['informe_original'] = '';
		$this->Actuacionesfiscale->virtualFields['informe_preliminar'] = '';
		$this->Actuacionesfiscale->virtualFields['informe_definitivo'] = '';
		$where = '';
		$and = ' WHERE ';
		if (isset($this->request->query['direccioncontrole_id']) && !empty($this->request->query['direccioncontrole_id'])) {
			$where .= $and . " Direccioncontrole.id = '{$this->request->query['direccioncontrole_id']}'";
			$and = ' AND ';
		}
		if (isset($this->request->query['actuacionesfiscale_id']) && !empty($this->request->query['actuacionesfiscale_id'])) {
			$where .= $and . " Actuacionesfiscale.id = '{$this->request->query['actuacionesfiscale_id']}'";
			$and = ' AND ';
		}
		if (isset($this->request->query['objeto_id']) && !empty($this->request->query['objeto_id'])) {
			$where .= $and . " Objeto.id = '{$this->request->query['objeto_id']}'";
			$and = ' AND ';
		}
		if (isset($this->request->query['actuacionesfiscale_id']) && !empty($this->request->query['actuacionesfiscale_id'])) {
			$where .= $and . " Actuacionesfiscale.id = '{$this->request->query['actuacionesfiscale_id']}'";
			$and = ' AND ';
		}
		if (isset($this->request->query['fecha_desde']) && !empty($this->request->query['fecha_desde'])) {
			App::uses('CakeTime', 'Utility');
			$fecha_desde = CakeTime::format('Y-m-d', $this->request->query['fecha_desde']);
			$where .= $and . " Actuacionesfiscale.fecha_inicio >= '$fecha_desde'";
			$and = ' AND ';
		}
		if (isset($this->request->query['fecha_hasta']) && !empty($this->request->query['fecha_hasta'])) {
			App::uses('CakeTime', 'Utility');
			$fecha_hasta = CakeTime::format('Y-m-d', $this->request->query['fecha_hasta']);
			$where .= $and . " Actuacionesfiscale.fecha_fin <= '$fecha_hasta'";
			$and = ' AND ';
		}
		if (isset($this->request->query['actuaciontipo_id']) && !empty($this->request->query['actuaciontipo_id'])) {
			$where .= $and . " Actuaciontipo.id = '{$this->request->query['actuaciontipo_id']}'";
			$and = ' AND ';
		}
		$funcionario_left = '';
		if (isset($this->request->query['funcionario_id']) && !empty($this->request->query['funcionario_id'])) {
			$funcionario_id = $this->request->query['funcionario_id'];
			$funcionario_left = "JOIN equipotrabajos as Equipotrabajo ON Equipotrabajo.actuacionesfiscale_id = Actuacionesfiscale.id AND Equipotrabajo.funcionario_id = '$funcionario_id'";
		}
		if (isset($this->request->query['programa_entregado']) && ($this->request->query['programa_entregado'] === '0')) {
			$where .= $and . " (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 2 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['programa_entregado']) && ($this->request->query['programa_entregado'] === '1')) {
			$where .= $and . " NOT (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 2 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['original']) && ($this->request->query['original'] === '0')) {
			$where .= $and . " (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 6 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['original']) && ($this->request->query['original'] === '1')) {
			$where .= $and . " NOT (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 6 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['preliminar']) && ($this->request->query['preliminar'] === '0')) {
			$where .= $and . " (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 7 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['preliminar']) && ($this->request->query['preliminar'] === '1')) {
			$where .= $and . " NOT (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 7 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['definitivo']) && ($this->request->query['definitivo'] === '0')) {
			$where .= $and . " (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 8 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		if (isset($this->request->query['definitivo']) && ($this->request->query['definitivo'] === '1')) {
			$where .= $and . " NOT (SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 8 ORDER BY x.id DESC LIMIT 1) = 0";
			$and = ' AND ';
		}
		$query = "
			SELECT
				Actuacionesfiscale.*, Actuaciontipo.denominacion, Direccioncontrole.denominacion, @original, Objeto.denominacion,
				(SELECT IFNULL(x.fecha_remite, '') AS fecha_remite FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 1 ORDER BY x.id ASC LIMIT 1) AS Actuacionesfiscale__fecha_inicio_fase1,
				(SELECT IFNULL(x.fecha_recibe, '') AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 1 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__fecha_fin_fase1,
				(SELECT IFNULL(x.fecha_remite, '') AS fecha_remite FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 2 ORDER BY x.id ASC LIMIT 1) AS Actuacionesfiscale__fecha_inicio_fase2,
				(SELECT IFNULL(x.fecha_recibe, '') AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 2 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__fecha_fin_fase2,
				(SELECT IFNULL(x.fecha_remite, '') AS fecha_remite FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 3 ORDER BY x.id ASC LIMIT 1) AS Actuacionesfiscale__fecha_inicio_fase3,
				(SELECT IFNULL(x.fecha_recibe, '') AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xf.id = 3 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__fecha_fin_fase3,
				(SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 2 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__programa_entregado,
				(SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 6 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__informe_original,
				(SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 7 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__informe_preliminar,
				(SELECT IF(x.fecha_recibe, x.fecha_recibe, COUNT(x.fecha_recibe)) AS fecha_recibe FROM operaciones AS x LEFT JOIN eventos AS xe ON xe.id = x.evento_id LEFT JOIN fases AS xf ON xf.id = xe.fase_id  WHERE actuacionesfiscale_id = Operacione.actuacionesfiscale_id and xe.id = 8 ORDER BY x.id DESC LIMIT 1) AS Actuacionesfiscale__informe_definitivo
			FROM
				actuacionesfiscales AS Actuacionesfiscale
			LEFT JOIN operaciones AS Operacione ON Operacione.actuacionesfiscale_id = Actuacionesfiscale.id
			LEFT JOIN eventos AS Evento ON Operacione.evento_id = Evento.id
			LEFT JOIN fases AS Fase ON Evento.fase_id = Fase.id
			LEFT JOIN actuaciontipos AS Actuaciontipo ON Actuacionesfiscale.actuaciontipo_id = Actuaciontipo.id
			LEFT JOIN objetos AS Objeto ON Actuacionesfiscale.objeto_id = Objeto.id
			LEFT JOIN direccioncontroles AS Direccioncontrole ON Actuacionesfiscale.direccioncontrole_id = Direccioncontrole.id
			$funcionario_left
			$where
			GROUP BY Actuacionesfiscale.id
			ORDER BY Actuacionesfiscale.id
		";
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$this->Actuacionesfiscale->query("SET @original = ''; ");
		$actuacionesfiscales = $this->Actuacionesfiscale->query($query);
		
		$j = 0;
		foreach ($actuacionesfiscales as $actuacionesfiscale) {
			// Obteniendo los dias no laborables como eventualidades
			$query = array();
			$query['conditions'] = array(
				'Eventualidade.actuacionesfiscale_id' => $actuacionesfiscale['Actuacionesfiscale']['id'],
				'Eventualidade.estado' => 'A',
				'Eventualidade.eventualidadtipo_id IN' => array(Configure::read('App.EVENTUALIDAD_NOLABORABLE'), Configure::read('App.EVENTUALIDAD_DIFERIMIENTO'))
			);
			$query['fields'] = array("Eventualidade.fecha_deteccion");
			// TODO: Aqui va lo del diferimiento OJO
			$otrosnolaborable = $this->Actuacionesfiscale->Eventualidade->find("list", $query);
			$i = 0;
			foreach ($otrosnolaborable as $k => $v) {
				$otrosnolaborable[$k] = date("Y-m-d", strtotime($v));
				$i++;
			}
			$dh = $this->getDiasHabiles($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_fase1'], $actuacionesfiscale['Actuacionesfiscale']['fecha_fin_fase1'], array_merge(array_values($diasferiados), array_values($otrosnolaborable)));
			$actuacionesfiscales[$j]['Actuacionesfiscale']['dh_fase1'] = $dh['ndias'];
			
			$dh = $this->getDiasHabiles($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_fase2'], $actuacionesfiscale['Actuacionesfiscale']['fecha_fin_fase2'], array_merge(array_values($diasferiados), array_values($otrosnolaborable)));;
			$actuacionesfiscales[$j]['Actuacionesfiscale']['dh_fase2'] = $dh['ndias'];
			
			$dh = $this->getDiasHabiles($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_fase3'], $actuacionesfiscale['Actuacionesfiscale']['fecha_fin_fase3'], array_merge(array_values($diasferiados), array_values($otrosnolaborable)));
			$actuacionesfiscales[$j]['Actuacionesfiscale']['dh_fase3'] = $dh['ndias'];
			
			$this->Actuacionesfiscale->Equipotrabajo->recursive = -1;
			$equipotrabajos = $this->Actuacionesfiscale->Equipotrabajo->findAllByActuacionesfiscaleId($actuacionesfiscale['Actuacionesfiscale']['id']);
			$actuacionesfiscales[$j]['Funcionarios'] = array();
			foreach ($equipotrabajos as $equipotrabajo) {
				$this->Actuacionesfiscale->Equipotrabajo->Funcionario->recursive = -1;
				$funcionario = $this->Actuacionesfiscale->Equipotrabajo->Funcionario->findById($equipotrabajo['Equipotrabajo']['funcionario_id']);
				$actuacionesfiscales[$j]['Funcionarios'][] = $funcionario['Funcionario']['nombre'];
			}
			
			$j++;
		}
		
		$this->loadModel("Direccioncontrole");
		$direccioncontroles = $this->Direccioncontrole->find("list");
		$actuacionesfiscales_list = $this->Actuacionesfiscale->find("list");
		$this->loadModel("Actuaciontipo");
		$actuaciontipos = $this->Actuaciontipo->find("list");
		$this->loadModel("Funcionario");
		$funcionarios = $this->Funcionario->find("list", array('order' => 'Funcionario.nombre ASC'));
		$this->loadModel("Objeto");
		$objetos_list = $this->Objeto->find("list");
		$this->set(compact("actuacionesfiscales", "direccioncontroles", "objetos_list", "actuacionesfiscales_list", "actuaciontipos", "funcionarios"));
	}
}
