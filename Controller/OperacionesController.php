<?php
App::uses('AppController', 'Controller');
/**
 * Operaciones Controller
 *
 * @property Operacione $Operacione
 */
class OperacionesController extends AppController {
	public function afterFilter() {
		parent::afterFilter();
		
		$auth_user = $this->Session->read("Auth.User");
		if (in_array($this->action, array('edit', 'delete')) && $auth_user['group_id'] != 1) {
			if (!$this->Operacione->isOwnedBy($this->request->data['Operacione']['fremite_id'], $auth_user['funcionario_id'])) {
				$this->runUnauthorized();
			}
		}
	}
/**
 * add method
 *
 * @return void
 */
	public function add($actuacionesfiscale_id = null, $evento_id = null) {
		$auth_user = $this->Session->read("Auth.User");
		if ($this->request->is('post')) {
			$this->Operacione->create();
			if ($this->Operacione->saveAll($this->request->data)) {
				$this->loadModel('ActuacionesfiscalesEvento');
				// Buscamos el evento asociado y lo cambiamos a Activo = 1
				$query['conditions'] = array(
					'ActuacionesfiscalesEvento.actuacionesfiscale_id' => $this->request->data['Operacione']['actuacionesfiscale_id'],
					'ActuacionesfiscalesEvento.evento_id' => $this->request->data['Operacione']['evento_id']
				);
				$actuacionesfiscalesevento = $this->ActuacionesfiscalesEvento->find("first", $query);
				
				// Aqui se cambia el valor
				$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['es_activo'] = 1;
				
				// Si el evento solo tiene 1 operacion, es que acaba de iniciar, por lo tanto
				// Establecemos su fecha de inicio al dia actual
				$query['conditions'] = array(
					'Operacione.actuacionesfiscale_id' => $this->request->data['Operacione']['actuacionesfiscale_id'],
					'Operacione.evento_id' => $this->request->data['Operacione']['evento_id']
				);
				$noperacione = $this->Operacione->find("count", $query);
				
				// Aqui se establece la fecha
				if ($noperacione == 1) {
					$actuacionesfiscalesevento['ActuacionesfiscalesEvento']['fecha_inicio'] = date('Y-m-d H:i:s');
				}
				
				// Guardamos el evento
				$this->ActuacionesfiscalesEvento->save($actuacionesfiscalesevento);
				
				// Se indica la fecha de inicio, si no existe de la AF
				$actuacionesfiscale = $this->Operacione->Actuacionesfiscale->find("first", array("conditions" => array("Actuacionesfiscale.id" => $this->request->data['Operacione']['actuacionesfiscale_id'])));
				if (empty($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio'])) {
					$actuacionesfiscale['Actuacionesfiscale']['fecha_inicio'] = date("Y-m-d");
					$this->Operacione->Actuacionesfiscale->save($actuacionesfiscale);
				}
				$this->Session->setFlash(__('La Actividad ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				if ($this->request->data['Operacione']['eventualidadtipo_id'] != Configure::read('App.SOLICITUD_ID') && $this->request->data['Operacione']['eventualidadtipo_id'] != Configure::read('App.CONSIGNAR_ID')) {
					$this->redirect(array('controller' => 'eventualidades', 'action' => 'add', $this->request->data['Operacione']['actuacionesfiscale_id'], $this->request->data['Operacione']['evento_id'], $this->request->data['Operacione']['frecibe_id'], $this->request->data['Operacione']['eventualidadtipo_id']));
				} else {
					$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Operacione']['actuacionesfiscale_id']));
				}
			} else {
				$this->Session->setFlash(__('La Actividad no pudo ser guardada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		// Si hay ambas cosas, se busca el elemento que corresponda
		$actuacionesfiscale = null;
		$evento = null;
		if ($actuacionesfiscale_id) {
			//  Buscando la AF en parametros
			$query = array();
			$query['conditions'] = array(
				'Actuacionesfiscale.id' => $actuacionesfiscale_id
			);
			$query['fields'] = array(
				'Actuacionesfiscale.numero'
			);
			$actuacionesfiscale = $this->Operacione->Actuacionesfiscale->find('first', $query);
		}
		if ($evento_id) {
			// Buscnado el evento en parametros
			$query = array();
			$query['conditions'] = array(
				'Evento.id' => $evento_id
			);
			$query['fields'] = array(
				'Evento.denominacion'
			);
			$evento = $this->Operacione->Evento->find('first', $query);
		}
		$actuacionesfiscales = $this->Operacione->Actuacionesfiscale->find('list');
		$eventos = $this->Operacione->Evento->find('list');
		
		$this->loadModel('Equipotrabajo');
		$query = array();
		$query['conditions'] = array(
			'Equipotrabajo.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'not' => array('Equipotrabajo.funcionariorole_id' => array('3', '4')) // No se muestran los auditores normales ni los asistentes
		);
		$query['fields'] = array('Equipotrabajo.funcionario_id');
		$equipo_trabajo = $this->Equipotrabajo->find('list', $query);
		$equipo_trabajo[] = Configure::read('App.CONTRALOR_ID'); // Contralor
		$equipo_trabajo[] = Configure::read('App.DIRECTORG_ID'); // Director General
		
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Frecibe.id'
				)
			),
		);
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
				'Frecibe.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id']
			);
		} else {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
			);
		}
		$director_area = $this->Operacione->Frecibe->find('list', $query);
		$equipo_trabajo = array_merge($equipo_trabajo, array_keys($director_area));
		if(!in_array($auth_user['funcionario_id'], $equipo_trabajo) && $auth_user['group_id'] != 1) {
			$this->runUnauthorized();
		}
		// Filtrando los que reciben para que no salga el que esta conectado
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Frecibe.id'
				)
			),
		);
		$query['conditions'] = array(
			array('not' => array('Frecibe.id =' => $auth_user['funcionario_id'])),
			'Frecibe.id' => array_values($equipo_trabajo)
		);
		$frecibes = $this->Operacione->Frecibe->find('list', $query);
		
		$query = array();
		// Si no es una correccion se muestran los tipos de eventos excepto la correccion
		// tambien se muestran los tipos adicionales que estan asociados a un evento especifico
		$query['conditions'] = array(
			'Eventualidadtipo.evento_id IS NULL',
			'Eventualidadtipo.es_operacion' => 1
		);
		$query['order'] = 'Eventualidadtipo.posicion';
		$eventualidadtipos = $this->Operacione->Eventualidadtipo->find('list', $query);
		$tiene_anexos = false;
		if (!empty($evento['Anexo'])) {
			$tiene_anexos = true;
		}
		$this->set(compact('auth_user', 'tiene_anexos', 'eventualidadtipos', 'actuacionesfiscale_id', 'evento_id', 'actuacionesfiscale', 'evento', 'actuacionesfiscales', 'eventos', 'frecibes'));
	}

/**
 * edit method
 *
 * @return void
 */
	public function edit($id = null) {
		$auth_user = $this->getAuthUser();
		if (!$this->Operacione->exists($id)) {
			throw new NotFoundException(__('Invalid operacione'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Operacione->create();
			if ($this->Operacione->save($this->request->data)) {
				$this->loadModel("Eventualidade");
				// Si el evento tiene eventualidades solo se edita, si no tiene eventualidades
				// y el tipo de evento esta definido, se redirecciona a donde debe
				$query['conditions'] = array(
					'Eventualidade.actuacionesfiscale_id' => $this->request->data['Operacione']['actuacionesfiscale_id'],
					'Eventualidade.evento_id' => $this->request->data['Operacione']['evento_id']
				);
				$noeventualidade = $this->Eventualidade->find("count", $query);
				
				$this->Session->setFlash(__('La Actividad ha sido actualizada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				
				// Si no hay eventualidades  y esta definido el tipo de eventualidad se crea dicha eventualidad
				if (!$noeventualidade && $this->request->data['Operacione']['eventualidadtipo_id'] != Configure::read('App.SOLICITUD_ID') && $this->request->data['Operacione']['eventualidadtipo_id'] != Configure::read('App.CONSIGNAR_ID')) {
					$this->redirect(array('controller' => 'eventualidades', 'action' => 'add', $this->request->data['Operacione']['actuacionesfiscale_id'], $this->request->data['Operacione']['evento_id'], $this->request->data['Operacione']['frecibe_id'], $this->request->data['Operacione']['eventualidadtipo_id']));
				} else {
					$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Operacione']['actuacionesfiscale_id']));
				}
				$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Operacione']['actuacionesfiscale_id']));
			} else {
				$this->Session->setFlash(__('La Actividad no pudo ser actualizada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$query = array();
			$query['conditions'] = array(
				'Operacione.id' => $id
			);
			$this->request->data = $this->Operacione->find("first", $query);
			
			// SI ya la recibio no puede editarla
			if ($this->request->data['Operacione']['es_recibido']) {
				$this->Session->setFlash(__('La Actividad ya fue recibida. No puede editarla'), 'flash_custom', array('class' => 'alert-error'));
				$this->redirect($this->referer());
			}
		}
		
		//  Buscando la AF en parametros
		$query = array();
		$query['conditions'] = array(
			'Actuacionesfiscale.id' => $this->request->data['Operacione']['actuacionesfiscale_id']
		);
		$query['fields'] = array(
			'Actuacionesfiscale.numero'
		);
		$actuacionesfiscale = $this->Operacione->Actuacionesfiscale->find('first', $query);
		
		// Buscnado el evento en parametros
		$query = array();
		$query['conditions'] = array(
			'Evento.id' => $this->request->data['Operacione']['evento_id']
		);
		$query['fields'] = array(
			'Evento.denominacion'
		);
		$evento = $this->Operacione->Evento->find('first', $query);
			
		$actuacionesfiscales = $this->Operacione->Actuacionesfiscale->find('list');
		$eventos = $this->Operacione->Evento->find('list');
		
		$this->loadModel('Equipotrabajo');
		$query = array();
		$query['conditions'] = array(
			'Equipotrabajo.actuacionesfiscale_id' => $this->request->data['Operacione']['actuacionesfiscale_id'],
			'not' => array('Equipotrabajo.funcionariorole_id' => array('3', '4')) // No se muestran los auditores normales ni los asistentes
		);
		$query['fields'] = array('Equipotrabajo.funcionario_id');
		$equipo_trabajo = $this->Equipotrabajo->find('list', $query);
		$equipo_trabajo[] = Configure::read('App.CONTRALOR_ID'); // Contralor
		$equipo_trabajo[] = Configure::read('App.DIRECTORG_ID'); // Director General
		
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Frecibe.id'
				)
			),
		);
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
				'Frecibe.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id']
			);
		} else {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
			);
		}
		$director_area = $this->Operacione->Frecibe->find('list', $query);
		$equipo_trabajo = array_merge($equipo_trabajo, array_keys($director_area));
		if(!in_array($auth_user['funcionario_id'], $equipo_trabajo) && $auth_user['group_id'] != 1) {
			$this->runUnauthorized();
		}
		
		// Filtrando los que reciben para que no salga el que esta conectado
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Frecibe.id'
				)
			),
		);
		$query['conditions'] = array(
			array('not' => array('Frecibe.id =' => $auth_user['funcionario_id'])),
			'Frecibe.id' => array_values($equipo_trabajo)
		);
		$frecibes = $this->Operacione->Frecibe->find('list', $query);
		
		$query = array();
		// Si no es una correccion se muestran los tipos de eventos excepto la correccion
		// tambien se muestran los tipos adicionales que estan asociados a un evento especifico
		$query['conditions'] = array(
			'Eventualidadtipo.evento_id IS NULL',
			'Eventualidadtipo.es_operacion' => 1
		);
		$actuacionesfiscale_id = $this->request->data['Operacione']['actuacionesfiscale_id'];
		$evento_id = $this->request->data['Operacione']['evento_id'];
		$eventualidadtipos = $this->Operacione->Eventualidadtipo->find('list', $query);
		$tiene_anexos = false;
		if (!empty($evento['Anexo'])) {
			$tiene_anexos = true;
		}
		$this->set(compact('auth_user', 'tiene_anexos', 'eventualidadtipos', 'actuacionesfiscale_id', 'evento_id', 'actuacionesfiscale', 'evento', 'actuacionesfiscales', 'eventos', 'frecibes'));
	}

/**
 * marcar_recibida method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_recibida($id = null) {
		if (!$this->Operacione->exists($id)) {
			throw new NotFoundException(__('Invalid operacione'));
		}
		$query = array('conditions' => array('Operacione.' . $this->Operacione->primaryKey => $id));
		$operacione = $this->Operacione->find('first', $query);
		$operacione['Operacione']['es_recibido'] = 1;
		$operacione['Operacione']['fecha_recibe'] = date('Y-m-d H:i:s');
		if ($this->Operacione->save($operacione)) {
			$this->Session->setFlash(__('La Actividad ha sido marcada como recibida'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Actividad no pudo ser marcada como recibida. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Operacione->id = $id;
		if (!$this->Operacione->exists()) {
			throw new NotFoundException(__('Invalid operacione'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Operacione->delete()) {
			$this->Session->setFlash(__('Actividad eliminada'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Actividad no pudo ser eliminada'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect($this->referer());
	}

}
