<?php
App::uses('AppController', 'Controller');
/**
 * Anexos Controller
 *
 * @property Anexo $Anexo
 */
class AnexosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Anexo->recursive = 0;
		$this->set('anexos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Anexo->exists($id)) {
			throw new NotFoundException(__('Invalid anexo'));
		}
		$this->Anexo->recursive = 2;
		$options = array('conditions' => array('Anexo.' . $this->Anexo->primaryKey => $id));
		$this->set('anexo', $this->Anexo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Anexo->create();
			if ($this->Anexo->save($this->request->data)) {
				$this->Session->setFlash(__('El Anexo ha sido guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El anexo no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$fases = $this->Anexo->Fase->find('list');
		$eventos = $this->Anexo->Evento->find('list', array("order" => "Evento.posicion"));
		$this->set(compact('fases', 'eventos'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Anexo->exists($id)) {
			throw new NotFoundException(__('Invalid anexo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Anexo->save($this->request->data)) {
				$this->Session->setFlash(__('The anexo has been saved'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The anexo could not be saved. Please, try again.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Anexo.' . $this->Anexo->primaryKey => $id));
			$this->request->data = $this->Anexo->find('first', $options);
		}
		$fases = $this->Anexo->Fase->find('list');
		$eventos = $this->Anexo->Evento->find('list', array("order" => "Evento.posicion"));
		$this->set(compact('fases', 'eventos'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Anexo->id = $id;
		if (!$this->Anexo->exists()) {
			throw new NotFoundException(__('Invalid anexo'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Anexo->delete()) {
			$this->Session->setFlash(__('Anexo deleted'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Anexo was not deleted'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
