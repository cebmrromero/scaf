<?php
App::uses('AppController', 'Controller');
/**
 * Eventualidades Controller
 *
 * @property Eventualidade $Eventualidade
 */
class EventualidadesController extends AppController {
	public function afterFilter() {
		parent::afterFilter();
		
		$auth_user = $this->Session->read("Auth.User");
		if (in_array($this->action, array('edit', 'delete')) && $auth_user['group_id'] != 1) {
			if (!$this->Eventualidade->isOwnedBy($this->request->data['Eventualidade']['detecta_id'], $auth_user['funcionario_id'])) {
				$this->runUnauthorized();
			}
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add($actuacionesfiscale_id = null, $evento_id = null, $corrige_id = null, $eventualidadtipo_id = null) {
		$es_correccion = false;
		if ($eventualidadtipo_id == Configure::read('App.CORECCION_ID')) {
			$es_correccion = true;
		}
		$auth_user = $this->Session->read("Auth.User");
		if ($this->request->is('post')) {
			$this->Eventualidade->create();
			if ($this->Eventualidade->save($this->request->data)) {
				$this->Session->setFlash(__('La Eventualidad ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Eventualidade']['actuacionesfiscale_id']));
			} else {
				$this->Session->setFlash(__('La Eventualidad no pudo ser guardada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		// Si estan todos los parametros
		$detecta = null;
		$corrige = null;
		$actuacionesfiscale = null;
		$evento = null;
		$eventualidadtipo = null;
		$numero_corecciones = 0;
		if ($actuacionesfiscale_id && $evento_id) {
			//  Buscando funcionario que corrige
			$query = array();
			$query['joins'] = array(
				array('table' => 'users',
					'alias' => 'User',
					'type' => 'RIGHT',
					'conditions' => array(
						'User.funcionario_id = Corrige.id'
					)
				),
			);
			$query['conditions'] = array(
				'Corrige.id' => $corrige_id,
				array('not' => array('Corrige.id =' => $auth_user['funcionario_id']))
			);
			$query['fields'] = array(
				'Corrige.nombre'
			);
			$corrige = $this->Eventualidade->Corrige->find('first', $query);
			
			//  Buscando la AF en parametros
			$query = array();
			$query['conditions'] = array(
				'Actuacionesfiscale.id' => $actuacionesfiscale_id
			);
			$query['fields'] = array(
				'Actuacionesfiscale.numero'
			);
			$actuacionesfiscale = $this->Eventualidade->Evento->Actuacionesfiscale->find('first', $query);
			
			// Buscnado el evento en parametros
			$query = array();
			$query['conditions'] = array(
				'Evento.id' => $evento_id
			);
			$query['fields'] = array(
				'Evento.denominacion'
			);
			$evento = $this->Eventualidade->Evento->find('first', $query);
			if ($es_correccion || $eventualidadtipo_id) {
				// Buscnado el tipo de evento
				$query = array();
				$query['conditions'] = array(
					'Eventualidadtipo.id' => $eventualidadtipo_id
				);
				$query['order'] = 'Eventualidadtipo.posicion';
				$eventualidadtipo = $this->Eventualidade->Eventualidadtipo->find('first', $query);
			}
			$query = array();
			$query['conditions'] = array(
				'Eventualidade.evento_id' => $evento_id,
				'Eventualidade.eventualidadtipo_id' => $eventualidadtipo_id,
				'Eventualidade.actuacionesfiscale_id' => $actuacionesfiscale_id,
			);
			$numero_corecciones = $this->Eventualidade->find("count", $query);
		}
		$numero_corecciones++;
		$eventos = $this->Eventualidade->Evento->find('list');
		$detectas = $this->Eventualidade->Detecta->find('list');
		
		$this->loadModel('Equipotrabajo');
		$query = array();
		$query['conditions'] = array(
			'Equipotrabajo.actuacionesfiscale_id' => $actuacionesfiscale_id,
			'not' => array('Equipotrabajo.funcionariorole_id' => array('3', '4')) // No se muestran los auditores normales ni los asistentes
		);
		$query['fields'] = array('Equipotrabajo.funcionario_id');
		$equipo_trabajo = $this->Equipotrabajo->find('list', $query);
		$equipo_trabajo[] = Configure::read('App.CONTRALOR_ID'); // Contralor
		$equipo_trabajo[] = Configure::read('App.DIRECTORG_ID'); // Director General
		
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Corrige.id'
				)
			),
		);
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
				'Corrige.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id']
			);
		} else {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
			);
		}
		$director_area = $this->Eventualidade->Corrige->find('list', $query);
		$equipo_trabajo = array_merge($equipo_trabajo, array_keys($director_area));
		
		if(!in_array($auth_user['funcionario_id'], $equipo_trabajo) && $auth_user['group_id'] != 1) {
			$this->runUnauthorized();
		}
		// Filtrando los que corrigen para que no salga el que esta conectado
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Corrige.id'
				)
			),
		);
		$query['conditions'] = array(
			array('not' => array('Corrige.id =' => $auth_user['funcionario_id'])),
			'Corrige.id' => array_values($equipo_trabajo)
		);
		$corriges = $this->Eventualidade->Corrige->find('list', $query);
		$query = array();
		if (!$es_correccion) {
			// Si no es una correccion se muestran los tipos de eventos excepto la correccion
			// tambien se muestran los tipos adicionales que estan asociados a un evento especifico
			$query['conditions'] = array(
				//array('not' => array('Eventualidadtipo.id =' => 1)), // Quitado temporalmente TODO: Probar que sea funcional
				array('or' => array(
					'Eventualidadtipo.evento_id IS NULL',
					'Eventualidadtipo.evento_id' => $evento_id,
				)),
				'Eventualidadtipo.es_eventualidad' => 1,
			);
			$query['order'] = 'Eventualidadtipo.posicion';
		}
		$eventualidadtipos = $this->Eventualidade->Eventualidadtipo->find('list', $query);
		$mediocomunicaciones = $this->Eventualidade->Mediocomunicacione->find('list');
		$actuacionesfiscales = $this->Eventualidade->Actuacionesfiscale->find('list');
		$this->set(compact('auth_user', 'eventualidadtipo_id', 'numero_corecciones', 'es_correccion', 'eventualidadtipo', 'corrige', 'actuacionesfiscale', 'evento', 'actuacionesfiscale_id', 'evento_id', 'corrige_id', 'eventos', 'detectas', 'corriges', 'eventualidadtipos', 'mediocomunicaciones', 'actuacionesfiscales'));
	}

/**
 * edit method
 *
 * @return void
 */
	public function edit($id) {
		$auth_user = $this->Session->read("Auth.User");
		if (!$this->Eventualidade->exists($id)) {
			throw new NotFoundException(__('Invalid eventualidade'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Eventualidade->create();
			if ($this->Eventualidade->save($this->request->data)) {
				$this->Session->setFlash(__('La Eventualidad ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Eventualidade']['actuacionesfiscale_id']));
			} else {
				$this->Session->setFlash(__('La Eventualidad no pudo ser guardada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$query = array();
			$query['conditions'] = array(
				'Eventualidade.id' => $id
			);
			$query['order'] = 'Eventualidadtipo.posicion';
			$this->request->data = $this->Eventualidade->find("first", $query);
			
			// SI ya la recibio no puede editarla
			if ($this->request->data['Eventualidade']['fecha_verificacion']) {
				$this->Session->setFlash(__('La Eventualidad ya fue verificada. No puede editarla'), 'flash_custom', array('class' => 'alert-error'));
				$this->redirect($this->referer());
			}
		}
		$es_correccion = false;
		if ($this->request->data['Eventualidade']['eventualidadtipo_id'] == Configure::read('App.CORECCION_ID')) {
			$es_correccion = true;
		}
		//  Buscando funcionario que corrige
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Corrige.id'
				)
			),
		);
		$query['conditions'] = array(
			'Corrige.id' => $this->request->data['Eventualidade']['corrige_id'],
			array('not' => array('Corrige.id =' => $auth_user['funcionario_id']))
		);
		$query['fields'] = array(
			'Corrige.nombre'
		);
		$corrige = $this->Eventualidade->Corrige->find('first', $query);
		
		//  Buscando la AF en parametros
		$query = array();
		$query['conditions'] = array(
			'Actuacionesfiscale.id' => $this->request->data['Eventualidade']['actuacionesfiscale_id']
		);
		$query['fields'] = array(
			'Actuacionesfiscale.numero'
		);
		$actuacionesfiscale = $this->Eventualidade->Evento->Actuacionesfiscale->find('first', $query);
		
		// Buscnado el evento en parametros
		$query = array();
		$query['conditions'] = array(
			'Evento.id' => $this->request->data['Eventualidade']['evento_id']
		);
		$query['fields'] = array(
			'Evento.denominacion'
		);
		$evento = $this->Eventualidade->Evento->find('first', $query);
		if ($es_correccion || $this->request->data['Eventualidade']['eventualidadtipo_id']) {
			// Buscnado el tipo de evento
			$query = array();
			$query['conditions'] = array(
				'Eventualidadtipo.id' => $this->request->data['Eventualidade']['eventualidadtipo_id']
			);
			$eventualidadtipo = $this->Eventualidade->Eventualidadtipo->find('first', $query);
		}
		$query = array();
		$query['conditions'] = array(
			'Eventualidade.evento_id' => $this->request->data['Eventualidade']['evento_id'],
			'Eventualidade.eventualidadtipo_id' => $this->request->data['Eventualidade']['eventualidadtipo_id'],
			'Eventualidade.actuacionesfiscale_id' => $this->request->data['Eventualidade']['actuacionesfiscale_id'],
		);
		$numero_corecciones = $this->Eventualidade->find("count", $query);
		$numero_corecciones++;
		$eventos = $this->Eventualidade->Evento->find('list');
		$detectas = $this->Eventualidade->Detecta->find('list');
		
		$this->loadModel('Equipotrabajo');
		$query = array();
		$query['conditions'] = array(
			'Equipotrabajo.actuacionesfiscale_id' => $this->request->data['Eventualidade']['actuacionesfiscale_id'],
			'not' => array('Equipotrabajo.funcionariorole_id' => array('3', '4')) // No se muestran los auditores normales ni los asistentes
		);
		$query['fields'] = array('Equipotrabajo.funcionario_id');
		$equipo_trabajo = $this->Equipotrabajo->find('list', $query);
		$equipo_trabajo[] = Configure::read('App.CONTRALOR_ID'); // Contralor
		$equipo_trabajo[] = Configure::read('App.DIRECTORG_ID'); // Director General
		
		$query = array();
		$query['joins'] = array(
			array('table' => 'users',
				'alias' => 'User',
				'type' => 'RIGHT',
				'conditions' => array(
					'User.funcionario_id = Corrige.id'
				)
			),
		);
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
				'Corrige.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id']
			);
		} else {
			$query['conditions'] = array(
				'User.group_id' => 7, // Directores
			);
		}
		$director_area = $this->Eventualidade->Corrige->find('list', $query);
		$equipo_trabajo = array_merge($equipo_trabajo, array_keys($director_area));
		
		if(!in_array($auth_user['funcionario_id'], $equipo_trabajo) && $auth_user['group_id'] != 1) {
			$this->runUnauthorized();
		}
		
		// Filtrando los que corrigen para que no salga el que esta conectado
		$query = array();
		$query['conditions'] = array(
			array('not' => array('Corrige.id =' => $auth_user['funcionario_id'])),
			'Corrige.id' => array_values($equipo_trabajo)
		);
		$corriges = $this->Eventualidade->Corrige->find('list', $query);
		
		$query = array();
		if (!$es_correccion) {
			// Si no es una correccion se muestran los tipos de eventos excepto la correccion
			// tambien se muestran los tipos adicionales que estan asociados a un evento especifico
			$query['conditions'] = array(
				//array('not' => array('Eventualidadtipo.id =' => 1)), // TODO: Igual que en add
				array('or' => array(
					'Eventualidadtipo.evento_id IS NULL',
					'Eventualidadtipo.evento_id' => $this->request->data['Eventualidade']['evento_id']
				)),
				'Eventualidadtipo.es_eventualidad' => 1,
			);
			$query['order'] = 'Eventualidadtipo.posicion';
		}
		$eventualidadtipo_id = $this->request->data['Eventualidade']['eventualidadtipo_id'];
		$actuacionesfiscale_id = $this->request->data['Eventualidade']['actuacionesfiscale_id'];
		$evento_id = $this->request->data['Eventualidade']['evento_id'];
		$eventualidadtipos = $this->Eventualidade->Eventualidadtipo->find('list', $query);
		$mediocomunicaciones = $this->Eventualidade->Mediocomunicacione->find('list');
		$actuacionesfiscales = $this->Eventualidade->Actuacionesfiscale->find('list');
		$this->set(compact('auth_user', 'eventualidadtipo_id', 'numero_corecciones', 'es_correccion', 'eventualidadtipo', 'corrige', 'actuacionesfiscale', 'evento', 'actuacionesfiscale_id', 'evento_id', 'eventos', 'detectas', 'corriges', 'eventualidadtipos', 'mediocomunicaciones', 'actuacionesfiscales'));
	}

/**
 * marcar_revisada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_revisada($id = null) {
		if ($this->marcar($id, 'R')) {
			$this->Session->setFlash(__('La Eventualidad ha sido marcada como Revisada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Eventualidad no pudo ser marcada como Revisada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * marcar_aprobada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_aprobada($id = null) {
		if ($this->marcar($id, 'A')) {
			$this->Session->setFlash(__('La Eventualidad ha sido marcada como Aprobada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Eventualidad no pudo ser marcada como Aprobada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * marcar_aprobada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_negada($id = null) {
		if ($this->marcar($id, 'N')) {
			$this->Session->setFlash(__('La Eventualidad ha sido marcada como Negada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Eventualidad no pudo ser marcada como Negada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * marcar_revisada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar($id = null, $marca = null) {
		if (!$this->Eventualidade->exists($id)) {
			throw new NotFoundException(__('Invalid eventualidade'));
		}
		$query = array('conditions' => array('Eventualidade.' . $this->Eventualidade->primaryKey => $id));
		$eventualidad = $this->Eventualidade->find('first', $query);
		$eventualidad['Eventualidade']['estado'] = $marca;
		$eventualidad['Eventualidade']['fecha_verificacion'] = date('Y-m-d H:i:s');
		if ($this->Eventualidade->save($eventualidad)) {
			return true;
		}
		return false;
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Eventualidade->id = $id;
		if (!$this->Eventualidade->exists()) {
			throw new NotFoundException(__('Invalid eventualidade'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Eventualidade->delete()) {
			$this->Session->setFlash(__('Eventualidad eliminada'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect($this->referer());
		}
		$this->Session->setFlash(__('Eventualidad no pudo ser eliminada'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect($this->referer());
	}
}
