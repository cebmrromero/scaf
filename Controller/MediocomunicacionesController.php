<?php
App::uses('AppController', 'Controller');
/**
 * Mediocomunicaciones Controller
 *
 * @property Mediocomunicacione $Mediocomunicacione
 */
class MediocomunicacionesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Mediocomunicacione->recursive = 0;
		$this->set('mediocomunicaciones', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Mediocomunicacione->exists($id)) {
			throw new NotFoundException(__('Invalid mediocomunicacione'));
		}
		$eventualidadestados = array('NR' => 'No revisada', 'A' => 'Aprobada', 'N' => 'Negada', 'R' => 'Revisada');
		$this->Mediocomunicacione->recursive = 2;
		$options = array('conditions' => array('Mediocomunicacione.' . $this->Mediocomunicacione->primaryKey => $id));
		$mediocomunicacione = $this->Mediocomunicacione->find('first', $options);
		$this->set(compact('mediocomunicacione', 'eventualidadestados'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Mediocomunicacione->create();
			if ($this->Mediocomunicacione->save($this->request->data)) {
				$this->Session->setFlash(__('Medio de comunicación guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Medio de comunicación no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Mediocomunicacione->exists($id)) {
			throw new NotFoundException(__('Invalid mediocomunicacione'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Mediocomunicacione->save($this->request->data)) {
				$this->Session->setFlash(__('Medio de comunicación actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Medio de comunicación no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Mediocomunicacione.' . $this->Mediocomunicacione->primaryKey => $id));
			$this->request->data = $this->Mediocomunicacione->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Mediocomunicacione->id = $id;
		if (!$this->Mediocomunicacione->exists()) {
			throw new NotFoundException(__('Invalid mediocomunicacione'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Mediocomunicacione->delete()) {
			$this->Session->setFlash(__('Medio de comunicación eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Medio de comunicación no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
