<?php
App::uses('AppController', 'Controller');
/**
 * Funcionarioroles Controller
 *
 * @property Funcionariorole $Funcionariorole
 */
class FuncionariorolesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Funcionariorole->recursive = 0;
		$this->set('funcionarioroles', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Funcionariorole->exists($id)) {
			throw new NotFoundException(__('Invalid funcionariorole'));
		}
		$this->Funcionariorole->recursive = 2;
		$options = array('conditions' => array('Funcionariorole.' . $this->Funcionariorole->primaryKey => $id));
		$this->set('funcionariorole', $this->Funcionariorole->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Funcionariorole->create();
			if ($this->Funcionariorole->save($this->request->data)) {
				$this->Session->setFlash(__('El rol ha sido guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El rol no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Funcionariorole->exists($id)) {
			throw new NotFoundException(__('Invalid funcionariorole'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Funcionariorole->save($this->request->data)) {
				$this->Session->setFlash(__('El rol ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El rol no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Funcionariorole.' . $this->Funcionariorole->primaryKey => $id));
			$this->request->data = $this->Funcionariorole->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Funcionariorole->id = $id;
		if (!$this->Funcionariorole->exists()) {
			throw new NotFoundException(__('Invalid funcionariorole'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Funcionariorole->delete()) {
			$this->Session->setFlash(__('Rol eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('El rol no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
