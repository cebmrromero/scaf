<?php
App::uses('AppController', 'Controller');
/**
 * Eventos Controller
 *
 * @property Evento $Evento
 */
class EventosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->paginate = array(
			'order' => array('Evento.fase' => 'ASC', 'Evento.posicion' => 'ASC')
		);
		$this->Evento->recursive = 0;
		$this->set('eventos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid accion'));
		}
		$eventualidadestados = array('NR' => 'No revisada', 'A' => 'Aprobada', 'N' => 'Negada', 'R' => 'Revisada');
		$this->Evento->recursive = 2;
		$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
		$evento = $this->Evento->find('first', $options);
		$this->set(compact('evento', 'eventualidadestados'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Evento->create();
			if ($this->Evento->save($this->request->data)) {
				$this->Session->setFlash(__('La Acción ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La Acción no pudo ser guardada. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$anexos = $this->Evento->Anexo->find('list');
		$fases = $this->Evento->Fase->find('list');
		$parents = $this->Evento->Parent->find('list', array('order' => array('Parent.posicion' => 'ASC')));
		$actuacionesfiscales = $this->Evento->Actuacionesfiscale->find('list');
		$this->set(compact('anexos', 'fases', 'parents', 'actuacionesfiscales'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Evento->exists($id)) {
			throw new NotFoundException(__('Invalid evento'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Evento->save($this->request->data)) {
				$this->Session->setFlash(__('La Acción ha sido actualizada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La Acción no pudo ser actualizada. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
			$this->request->data = $this->Evento->find('first', $options);
		}
		$anexos = $this->Evento->Anexo->find('list');
		$fases = $this->Evento->Fase->find('list');
		$parents = $this->Evento->Parent->find('list', array('order' => array('Parent.posicion' => 'ASC')));
		$actuacionesfiscales = $this->Evento->Actuacionesfiscale->find('list');
		$this->set(compact('anexos', 'fases', 'parents', 'actuacionesfiscales'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Evento->id = $id;
		if (!$this->Evento->exists()) {
			throw new NotFoundException(__('Invalid evento'));
		}
		$evento = $this->Evento->read(null, $id);
		$this->request->onlyAllow('post', 'delete');
		if ($evento['Evento']['bloqueado']) {
			$this->Session->setFlash(__('Registro bloqueado, no puede eliminarse'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Evento->delete()) {
			$this->Session->setFlash(__('La Acción ha sido eliminada existosamente'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('La Acción no pudo ser eliminada. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
