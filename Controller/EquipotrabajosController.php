<?php
App::uses('AppController', 'Controller');
/**
 * Equipotrabajos Controller
 *
 * @property Equipotrabajo $Equipotrabajo
 */
class EquipotrabajosController extends AppController {

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Equipotrabajo->id = $id;
		if (!$this->Equipotrabajo->exists()) {
			throw new NotFoundException(__('Invalid equipotrabajo'));
		}
		$this->request->onlyAllow('get', 'post', 'delete');
		if ($this->Equipotrabajo->delete()) {
			if (!$this->request->is('ajax')) {
				$this->Session->setFlash(__('Equipotrabajo deleted'));
				$this->redirect(array('action' => 'index'));
			} else {
				echo 1;
				exit(0);
			}
		}
		if (!$this->request->is('ajax')) {
			$this->Session->setFlash(__('Equipotrabajo was not deleted'));
			$this->redirect(array('action' => 'index'));
		} else {
			echo 0;
			exit(0);
		}
	}
}
