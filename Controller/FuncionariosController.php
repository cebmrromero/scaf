<?php
App::uses('AppController', 'Controller');
/**
 * Funcionarios Controller
 *
 * @property Funcionario $Funcionario
 */
class FuncionariosController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {// Add filter
		$this->Filter->addFilters(
			array(
				'nombre' => array(
					'Funcionario.nombre' => array(
						'operator' => 'LIKE',
						'value' => array(
							'before' => '%', // optional
							'after'  => '%'  // optional
						)
					),
				),
				'direccioncontrole_id' => array(
					'Funcionario.direccioncontrole_id' => array(
						'select' => $this->Filter->select('Direccioncontrole', $this->Funcionario->Direccioncontrole->find('list'))
					),
				),
			)
		);
	
		$this->Filter->setPaginate('order', "Funcionario.direccioncontrole_id ASC, Funcionario.nombre ASC"); // optional
	
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());
		$this->Funcionario->recursive = 0;
		$this->set('funcionarios', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Funcionario->exists($id)) {
			throw new NotFoundException(__('Invalid funcionario'));
		}
		$auth_user = $this->getAuthUser();
		$eventualidadestados = array('NR' => 'No revisada', 'A' => 'Aprobada', 'N' => 'Negada', 'R' => 'Revisada');
		$this->Funcionario->recursive = 2;
		$options = array('conditions' => array('Funcionario.' . $this->Funcionario->primaryKey => $id));
		$funcionario = $this->Funcionario->find('first', $options);
		$this->set(compact('funcionario', 'auth_user', 'eventualidadestados'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$auth_user = $this->getAuthUser();
		if ($this->request->is('post')) {
			$this->Funcionario->create();
			if ($this->Funcionario->saveAll($this->request->data)) {
				$this->Session->setFlash(__('El funcionario se ha guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El funcionario no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$direccioncontroles = $this->Funcionario->Direccioncontrole->find("list");
		$groups = $this->Funcionario->User->Group->find("list");
		$this->set(compact("direccioncontroles", 'auth_user', 'groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$auth_user = $this->getAuthUser();
		if (!$this->Funcionario->exists($id)) {
			throw new NotFoundException(__('Invalid funcionario'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$fieldlist = array(
				'fieldList' => array(
					'Funcionario' => array('id', 'nombre', 'direccioncontrole_id'),
					'User' => array('username', 'group_id')
				)
			);
			if ($this->Funcionario->saveAll($this->request->data, $fieldlist)) {
				$this->Session->setFlash(__('El funcionario ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El funcionario no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Funcionario.' . $this->Funcionario->primaryKey => $id));
			$this->Funcionario->recursive = 1;
			$this->request->data = $this->Funcionario->find('first', $options);
		}
		$direccioncontroles = $this->Funcionario->Direccioncontrole->find("list");
		$groups = $this->Funcionario->User->Group->find("list");
		$this->set(compact("direccioncontroles", 'auth_user', 'groups'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Funcionario->id = $id;
		if (!$this->Funcionario->exists()) {
			throw new NotFoundException(__('Invalid funcionario'));
		}
		$funcionario = $this->Funcionario->read(null, $id);
		$this->request->onlyAllow('post', 'delete');
		if ($funcionario['Funcionario']['bloqueado']) {
			$this->Session->setFlash(__('Registro bloqueado, no puede eliminarse'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Funcionario->delete()) {
			$this->Session->setFlash(__('Funcionario eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Funcionario no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
