<?php
App::uses('AppController', 'Controller');
/**
 * Actuacionesfiscales Controller
 *
 * @property Actuacionesfiscale $Actuacionesfiscale
 */
class ActuacionesfiscalesController extends AppController {
	public function afterFilter() {
		parent::afterFilter();
		
		$auth_user = $this->Session->read("Auth.User");
		if ($this->action == 'details' && $auth_user['group_id'] != 1 && $auth_user['Funcionario']['direccioncontrole_id'] != $this->request->data['Actuacionesfiscale']['direccioncontrole_id'] && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$funcionario_ids = Hash::extract($this->request->data['Equipotrabajo'], '{n}.funcionario_id');
			if (!$this->Actuacionesfiscale->isOwnedBy($funcionario_ids, $auth_user['funcionario_id'])) {
				$this->runUnauthorized();
			}
		}
	}
    
    public function beforeFilter() {
        $this->Auth->allow('resume');
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$auth_user = $this->Session->read("Auth.User");
		$query = array();
		if ($auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID') && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['group_id'] != '1') {
			// Buscando las AF en las que un funcionario se encuentra
			$query['joins'] = array(
				array('table' => 'equipotrabajos',
					'alias' => 'Equipotrabajo',
					'type' => 'LEFT',
					'conditions' => array(
						'Equipotrabajo.actuacionesfiscale_id = Actuacionesfiscale.id'
					)
				)
			);
			if ($auth_user['group_id'] == 7) { // Directores
				$query['conditions'] = array(
					'and' => array(
						'or' => array(
							'Equipotrabajo.funcionario_id' => $auth_user['funcionario_id'],
							'Actuacionesfiscale.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id']
						),
						'Actuacionesfiscale.fecha_fin IS NULL',
					)
				);
			} else {
				$query['conditions'] = array(
					'and' => array(
						//'or' => array(
							'Equipotrabajo.funcionario_id' => $auth_user['funcionario_id'],
							'Actuacionesfiscale.direccioncontrole_id' => $auth_user['Funcionario']['direccioncontrole_id'],
						//),
						'Actuacionesfiscale.fecha_fin IS NULL',
					)
				);
			}
			$query['group'] = array('Actuacionesfiscale.id');
		} else {
			$query['conditions'] = array(
				'Actuacionesfiscale.fecha_fin IS NULL'
			);
		}
		$actuacionesfiscales = $this->Actuacionesfiscale->find('all', $query);
		unset($query);
		
		// Buscando las operaciones en las que esta implicado el funcionario
		$query['conditions'] = array(
			'Operacione.frecibe_id' => $auth_user['funcionario_id'],
			'Operacione.es_recibido' => 0,
		);
		$operaciones = $this->Actuacionesfiscale->Operacione->find('all', $query);
		
		// Buscando las operaciones en las que esta implicado el funcionario
		$query['conditions'] = array(
			'Eventualidade.corrige_id' => $auth_user['funcionario_id'],
			'Eventualidade.estado' => 'NR'
		);
		$eventualidades = $this->Actuacionesfiscale->Eventualidade->find('all', $query);
		
		$this->set(compact('auth_user', 'actuacionesfiscales', 'operaciones', 'eventualidades'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Parametro');
		//$this->loadModel('Funcionario');
		$this->loadModel('Funcionariorole');
		$this->loadModel('Evento');
		if ($this->request->is('post')) {
			$this->Actuacionesfiscale->create();
			//$this->request->data['Actuacionesfiscale']['ejercicio_economico'] = $this->request->data['Actuacionesfiscale']['ejercicio_economico']['year'];
			if ($this->Actuacionesfiscale->saveAll($this->request->data)) {
				$this->Session->setFlash(__('La Actuación Fiscal ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$numero_af = $this->Parametro->findByName("numero_af");
				$numero_af['Parametro']['value'] = $this->request->data['Actuacionesfiscale']['numero'];
				$this->Parametro->save($numero_af);
				$this->redirect(array('action' => 'details', $this->Actuacionesfiscale->getLastInsertID()));
			} else {
				$this->Session->setFlash(__('La Actuación Fiscal no pudo ser guardada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$numero_af = $this->Parametro->findByName("numero_af");
		$numero_af = $this->getNextNumeroAf($numero_af['Parametro']['value']);
		$auth_user = $this->Session->read("Auth.User");
		$fases = $this->Actuacionesfiscale->Fase->find('list');
		$actuacionesorigenes = $this->Actuacionesfiscale->Actuacionesorigene->find('list');
		$actuaciontipos = $this->Actuacionesfiscale->Actuaciontipo->find('list');
		$ejerciciosfiscales = $this->Actuacionesfiscale->Ejerciciosfiscale->find('list', array('order' => 'Ejerciciosfiscale.id DESC'));
		$objetos = $this->Actuacionesfiscale->Objeto->find('list');
		$users = $this->Actuacionesfiscale->User->find('list');
		// Buscando la lista de dependencias
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$direccioncontroles = $this->Actuacionesfiscale->Direccioncontrole->find('list', array('conditions' => array('Direccioncontrole.id' => $auth_user['Funcionario']['direccioncontrole_id'])));
		} else {
			$direccioncontroles = $this->Actuacionesfiscale->Direccioncontrole->find('list');
		}
		$eventos = $this->Evento->find("list");
		//$funcionarios = $this->Funcionario->find('list', array('conditions' => array('Funcionario.es_mostrado' => '1'), 'order' => array('Funcionario.nombre')));
		$this->Funcionariorole->recursive = 0;
		$funcionarioroles = $this->Funcionariorole->find('all', array('fields' => array('id', 'denominacion', 'requerido')));
		$funcionarioroles_list = $this->Funcionariorole->find('list');
		$objetivoespecificos = $this->Actuacionesfiscale->Objetivoespecifico->find('list');
		$this->set(compact('auth_user', 'objetivoespecificos', 'numero_af', 'fases', 'eventos', 'actuacionesorigenes', 'actuaciontipos', 'objetos', 'users', 'direccioncontroles', 'funcionarios', 'funcionarioroles', 'funcionarioroles_list', 'ejerciciosfiscales'));
	}
	
	public function edit($id) {
		$this->loadModel('Equipotrabajo');
		if (!$this->Actuacionesfiscale->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesfiscale'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//$this->request->data['Actuacionesfiscale']['ejercicio_economico'] = $this->request->data['Actuacionesfiscale']['ejercicio_economico']['year'];
			if ($this->Actuacionesfiscale->saveAll($this->request->data)) {
				$this->Session->setFlash(__('La Actuación Fiscal ha sido actualizada existosamente'), 'flash_custom', array('class' => 'alert-success'));
			} else {
				$this->Session->setFlash(__('La Actuación Fiscal no pudo ser actualizada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$this->redirect($this->referer());
	}

/**
 * details method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function details($id = null) {
		if (!$this->Actuacionesfiscale->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesfiscale'));
		}
		// Cargando el modelos
		$this->loadModel('Evento');
		$this->loadModel('Funcionario');
		$this->loadModel('Funcionariorole');
		
		$this->loadModel("Nolaborable");
		$diasferiados = $this->Nolaborable->getDias();
		$otrosnolaborable = $this->getOtrosNolaborablesByAF($id);
		$nolaborables_todos = array_merge(array_values($diasferiados), array_values($otrosnolaborable));
		
		$query = array('conditions' => array('Actuacionesfiscale.' . $this->Actuacionesfiscale->primaryKey => $id));
		$this->request->data = $this->Actuacionesfiscale->find('first', $query);
		
		$i = 0;
		foreach ($this->request->data['Evento'] as $evento) {
			// Buscando todas las operaciones
			$query = array();
			$query['conditions'] = array(
				'Operacione.actuacionesfiscale_id' => $id,
				'Operacione.evento_id' => $evento['id']
			);
			$operaciones = $this->Evento->Operacione->find("all", $query);

			$aux = array();
			$m=0;
			foreach ($operaciones as $operacione) {
				// Calculo de las fechas
				$dh = $this->getDiasHabiles($operacione['Operacione']['fecha_remite'], $operacione['Operacione']['fecha_recibe'], array_values($diasferiados));
				$operaciones[$m]['Operacione']['diff_recepcion'] = $dh['ndias'];
					
				$operaciones[$m]['Operacione']['diff_remision'] = null;
				if (isset($operaciones[$m - 1]['Operacione']['fecha_remite'])) {
					$dh = $this->getDiasHabiles($operaciones[$m - 1]['Operacione']['fecha_recibe'], $operacione['Operacione']['fecha_remite'], array_values($diasferiados));
					$operaciones[$m]['Operacione']['diff_remision'] = $dh['ndias'];
				}
				$m++;
			}
			foreach ($operaciones as $operacione) {
				$aux = $operacione['Operacione'];
				$aux['Actuacionesfiscale'] = $operacione['Actuacionesfiscale'];
				$aux['Evento'] = $operacione['Evento'];
				$aux['Fremite'] = $operacione['Fremite'];
				$aux['Frecibe'] = $operacione['Frecibe'];
				$aux['Anexo'] = $operacione['Anexo'];
				$this->request->data['Evento'][$i]['Operacione'][] = $aux;
			}
			
			// Buscando las eventualidades
			$query = array();
			$query['conditions'] = array(
				'Eventualidade.actuacionesfiscale_id' => $id,
				'Eventualidade.evento_id' => $evento['id']
			);
			$eventualidades = $this->Evento->Eventualidade->find("all", $query);
			$aux = array();
			foreach ($eventualidades as $eventualidade) {
				$aux = $eventualidade['Eventualidade'];
				$dh = $this->getDiasHabiles($aux['fecha_deteccion'], $aux['fecha_verificacion'], array_values($diasferiados));
				$aux['Detecta'] = $eventualidade['Detecta'];
				$aux['Corrige'] = $eventualidade['Corrige'];
				$aux['Eventualidadtipo'] = $eventualidade['Eventualidadtipo'];
				$aux['diff_verificacion'] = $dh['ndias'];
				$this->request->data['Evento'][$i]['Eventualidade'][] = $aux;
			}
			$i++;
		}
		
		$fases = $this->Actuacionesfiscale->Fase->find('list');
		$details = $this->getActuacionesfiscaleDetails($this->request->data, $fases, $nolaborables_todos);
		
		$query = array();
		$query['conditions'] = array(
			'Prorroga.actuacionesfiscale_id' => $id,
			'Prorroga.fase_id' => $this->request->data['Actuacionesfiscale']['fase_id'],
			'Prorroga.es_aprobada' => '2'
		);
		$this->request->data['Actuacionesfiscale']['total_ejecutados'] = $this->getDiasHabiles($this->request->data['Actuacionesfiscale']['fecha_inicio'], date('Y-m-d'), $nolaborables_todos);
		$this->request->data['Actuacionesfiscale']['total_ejecutados'] = $this->request->data['Actuacionesfiscale']['total_ejecutados']['ndias'];
		$query['limit'] = 1;
		$prorroga = $this->Actuacionesfiscale->Prorroga->find("first", $query);
		$auth_user = $this->Session->read("Auth.User");
		$this->Actuacionesfiscale->Fase->recursive = -1;
		$fases_all = $this->Actuacionesfiscale->Fase->find('all');
		$actuacionesorigenes = $this->Actuacionesfiscale->Actuacionesorigene->find('list');
		$actuaciontipos = $this->Actuacionesfiscale->Actuaciontipo->find('list');
		$ejerciciosfiscales = $this->Actuacionesfiscale->Ejerciciosfiscale->find('list', array('order' => 'Ejerciciosfiscale.id DESC'));
		$objetos = $this->Actuacionesfiscale->Objeto->find('list');
		$users = $this->Actuacionesfiscale->User->find('list');
		
		// Buscando la lista de dependencias
		if ($auth_user['group_id'] != 1 && $auth_user['funcionario_id'] != Configure::read('App.DIRECTORG_ID') && $auth_user['funcionario_id'] != Configure::read('App.CONTRALOR_ID')) {
			$direccioncontroles = $this->Actuacionesfiscale->Direccioncontrole->find('list', array('conditions' => array('Direccioncontrole.id' => $auth_user['Funcionario']['direccioncontrole_id'])));
		} else {
			$direccioncontroles = $this->Actuacionesfiscale->Direccioncontrole->find('list');
		}
		$eventos = $this->Evento->find("list");
		$funcionarios = $this->Funcionario->find('list', array('order' => array('Funcionario.nombre')));
		$this->Funcionariorole->recursive = 0;
		$funcionarioroles = $this->Funcionariorole->find('all', array('fields' => array('id', 'denominacion', 'requerido')));
		$funcionarioroles_list = $this->Funcionariorole->find('list');
		$objetivoespecificos = $this->Actuacionesfiscale->Objetivoespecifico->find('list');
		$this->set(compact('auth_user', 'objetivoespecificos', 'prorroga', 'fases', 'fases_all', 'actuacionesorigenes', 'actuaciontipos', 'objetos', 'users', 'direccioncontroles', 'eventos', 'funcionarios', 'funcionarioroles', 'funcionarioroles_list', 'details', 'ejerciciosfiscales'));
	}

/**
 * cambiar_fase method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cambiar_fase($id = null, $fase_id = null) {
		if (!$this->Actuacionesfiscale->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesfiscale'));
		}
		$this->Actuacionesfiscale->recursive = -1;
		$query = array('conditions' => array('Actuacionesfiscale.id' => $id));
		$actuacionesfiscale = $this->Actuacionesfiscale->find('first', $query);
		$fase_id += 1;
		$actuacionesfiscale['Actuacionesfiscale']['fase_id'] = $fase_id;
		if ($this->Actuacionesfiscale->save($actuacionesfiscale, false, array('fase_id'))) {
			$this->Session->setFlash(__('La Actuación Fiscal ha cambiado de fase existosamente'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Actuación Fiscal no pudo ser cambiada de fase. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * terminar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function terminar($id = null) {
		if (!$this->Actuacionesfiscale->exists($id)) {
			throw new NotFoundException(__('Invalid actuacionesfiscale'));
		}
		$this->Actuacionesfiscale->recursive = -1;
		$query = array('conditions' => array('Actuacionesfiscale.id' => $id));
		$actuacionesfiscale = $this->Actuacionesfiscale->find('first', $query);
		$actuacionesfiscale['Actuacionesfiscale']['fecha_fin'] = date('Y-m-d H:i:s');
		if ($this->Actuacionesfiscale->save($actuacionesfiscale)) {
			$this->Session->setFlash(__('La Actuación Fiscal ha sido finalizada existosamente'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Actuación Fiscal no pudo ser finalizada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect(array('action' => 'index'));
	}
	
	public function getNextNumeroAf($numero) {
		$numero = explode('-', $numero);
		$numero[2]++;
		if ($numero[2] < 10) {
			$numero[2] = "0" . $numero[2];
		}
		return join("-", $numero);
	}
}
