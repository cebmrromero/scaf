<?php
App::uses('AppController', 'Controller');
/**
 * Nolaborables Controller
 *
 * @property Nolaborable $Nolaborable
 */
class NolaborablesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Nolaborable->recursive = 0;
		$this->set('nolaborables', $this->paginate());
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Nolaborable->create();
			if ($this->Nolaborable->save($this->request->data)) {
				$this->Session->setFlash(__('Día no laborable guardado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Día no laborable no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Nolaborable->exists($id)) {
			throw new NotFoundException(__('Invalid nolaborable'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Nolaborable->save($this->request->data)) {
				$this->Session->setFlash(__('Día no laborable actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Dia no laborable no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Nolaborable.' . $this->Nolaborable->primaryKey => $id));
			$this->request->data = $this->Nolaborable->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Nolaborable->id = $id;
		if (!$this->Nolaborable->exists()) {
			throw new NotFoundException(__('Invalid nolaborable'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Nolaborable->delete()) {
			$this->Session->setFlash(__('Día no laborable eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Día no laborable no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
}
