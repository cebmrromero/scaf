<?php
App::uses('AppController', 'Controller');
/**
 * Fases Controller
 *
 * @property Fase $Fase
 */
class FasesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Fase->recursive = 0;
		$fases = $this->paginate();
		$inicios = array('0' => 'Manual', '1' => 'Automático');
		$this->set(compact("inicios", "fases"));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Fase->exists($id)) {
			throw new NotFoundException(__('Invalid fase'));
		}
		$this->Fase->recursive = 2;
		$options = array('conditions' => array('Fase.' . $this->Fase->primaryKey => $id));
		$this->set('fase', $this->Fase->find('first', $options));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Fase->exists($id)) {
			throw new NotFoundException(__('Invalid fase'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Fase->save($this->request->data)) {
				$this->Session->setFlash(__('La Fase ha sido actualizada'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La Fase no pudo ser actualizada. Por favor, intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Fase.' . $this->Fase->primaryKey => $id));
			$this->request->data = $this->Fase->find('first', $options);
		}
		$inicios = array('0' => 'Manual', '1' => 'Automático');
		$eventos = $this->Fase->Evento->find("list");
		$this->set(compact("inicios", 'eventos'));
	}
}
