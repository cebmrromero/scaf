<?php
App::uses('AppController', 'Controller');
/**
 * Prorrogas Controller
 *
 * @property Prorroga $Prorroga
 */
class ProrrogasController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Prorroga->recursive = 0;
		$query = array();
		$query['order'] = array(
			'Prorroga.es_aprobada' => 'DESC',
			'Prorroga.fecha_solicitud' => 'ASC'
		);
		$this->paginate = $query;
		$this->set('prorrogas', $this->paginate());
	}

/**
 * add method
 *
 * @return void
 */
	public function add($actuacionesfiscale_id = null, $fase_id = null) {
		$auth_user = $this->Session->read("Auth.User");
		if ($this->request->is('post')) {
			$this->Prorroga->create();
			if ($this->Prorroga->save($this->request->data)) {
				$this->Session->setFlash(__('La Prorroga ha sido guardada existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('controller' => 'actuacionesfiscales', 'action' => 'details', $this->request->data['Prorroga']['actuacionesfiscale_id']));
			} else {
				$this->Session->setFlash(__('La Prorroga no pudo ser guardada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		//  Buscando la AF en parametros
		$query = array();
		$query['conditions'] = array(
			'Actuacionesfiscale.id' => $actuacionesfiscale_id
		);
		$query['fields'] = array(
			'Actuacionesfiscale.numero'
		);
		$actuacionesfiscale = $this->Prorroga->Actuacionesfiscale->find('first', $query);
		
		//  Buscando la Fase
		$query = array();
		$query['conditions'] = array(
			'Fase.id' => $fase_id
		);
		$fase = $this->Prorroga->Fase->find('first', $query);
		$solicitas = $this->Prorroga->Solicita->find('list');
		$apruebas = $this->Prorroga->Aprueba->find('list');
		$actuacionesfiscales = $this->Prorroga->Actuacionesfiscale->find('list');
		$fases = $this->Prorroga->Fase->find('list');
		$this->set(compact('auth_user', 'actuacionesfiscale', 'fase', 'actuacionesfiscale_id', 'fase_id', 'solicitas', 'apruebas', 'actuacionesfiscales', 'fases'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$auth_user = $this->Session->read("Auth.User");
		if (!$this->Prorroga->exists($id)) {
			throw new NotFoundException(__('Invalid prorroga'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Prorroga->save($this->request->data)) {
				$this->Session->setFlash(__('La prorroga ha sido actualizada'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La prorroga no pudo ser actualizada. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('Prorroga.' . $this->Prorroga->primaryKey => $id));
			$this->request->data = $this->Prorroga->find('first', $options);
		}
		$solicitas = $this->Prorroga->Solicita->find('list');
		$apruebas = $this->Prorroga->Aprueba->find('list');
		$actuacionesfiscales = $this->Prorroga->Actuacionesfiscale->find('list');
		$fases = $this->Prorroga->Fase->find('list');
		//  Buscando la AF en parametros
		$query = array();
		$query['conditions'] = array(
			'Actuacionesfiscale.id' => $this->request->data['Prorroga']['actuacionesfiscale_id']
		);
		$query['fields'] = array(
			'Actuacionesfiscale.numero'
		);
		$actuacionesfiscale = $this->Prorroga->Actuacionesfiscale->find('first', $query);
		
		//  Buscando la Fase
		$query = array();
		$query['conditions'] = array(
			'Fase.id' => $this->request->data['Prorroga']['fase_id']
		);
		$query['fields'] = array(
			'Fase.denominacion'
		);
		$fase = $this->Prorroga->Fase->find('first', $query);
		$this->set(compact('auth_user', 'solicitas', 'apruebas', 'actuacionesfiscales', 'fases', 'actuacionesfiscale', 'fase'));
	}

/**
 * marcar_aprobada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_aprobada($id = null) {
		if ($this->marcar($id, '1')) {
			$this->Session->setFlash(__('La Prorroga ha sido marcada como Aprobada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Prorroga no pudo ser marcada como Aprobada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * marcar_aprobada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar_negada($id = null) {
		if ($this->marcar($id, '0')) {
			$this->Session->setFlash(__('La Prorroga ha sido marcada como Negada'), 'flash_custom', array('class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('La Prorroga no pudo ser marcada como Negada. Intente de nuevo'), 'flash_custom', array('class' => 'alert-error'));
		}
		$this->redirect($this->referer());
	}

/**
 * marcar_revisada method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function marcar($id = null, $marca = null) {
		$auth_user = $this->Session->read('Auth.User');
		if (!$this->Prorroga->exists($id)) {
			throw new NotFoundException(__('Invalid prorroga'));
		}
		$query = array('conditions' => array('Prorroga.' . $this->Prorroga->primaryKey => $id));
		$prorroga = $this->Prorroga->find('first', $query);
		$prorroga['Prorroga']['es_aprobada'] = $marca;
		$prorroga['Prorroga']['fecha_verificacion'] = date('Y-m-d H:i:s');
		$prorroga['Prorroga']['aprueba_id'] = $auth_user['funcionario_id'];
		if ($this->Prorroga->save($prorroga )) {
			return true;
		}
		return false;
	}
}
