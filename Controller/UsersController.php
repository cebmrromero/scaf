<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('initDB', 'login', 'logout');
	}
	
	public function initDB() {
		$group = $this->User->Group;
		
		// Administradores
		$group->id = 1;
		$this->Acl->allow($group, 'controllers');
		
		// Contralor
		$group->id = 2;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/add');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/edit');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->allow($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/Prorrogas');
		$this->Acl->deny($group, 'controllers/Prorrogas/add');
		$this->Acl->allow($group, 'controllers/Reportes');
		$this->Acl->allow($group, 'controllers/Json');
		
		$this->Acl->allow($group, 'controllers/Actuacionesorigenes/index');
		$this->Acl->allow($group, 'controllers/Actuacionesorigenes/view');
		$this->Acl->allow($group, 'controllers/Actuaciontipos/index');
		$this->Acl->allow($group, 'controllers/Actuaciontipos/view');
		$this->Acl->allow($group, 'controllers/Anexos/index');
		$this->Acl->allow($group, 'controllers/Anexos/view');
		$this->Acl->allow($group, 'controllers/Direccioncontroles/index');
		$this->Acl->allow($group, 'controllers/Direccioncontroles/view');
		$this->Acl->allow($group, 'controllers/Eventos/index');
		$this->Acl->allow($group, 'controllers/Eventos/view');
		$this->Acl->allow($group, 'controllers/Fases/index');
		$this->Acl->allow($group, 'controllers/Fases/view');
		$this->Acl->allow($group, 'controllers/Funcionarioroles/index');
		$this->Acl->allow($group, 'controllers/Funcionarioroles/view');
		$this->Acl->allow($group, 'controllers/Funcionarios/index');
		$this->Acl->allow($group, 'controllers/Funcionarios/view');
		$this->Acl->allow($group, 'controllers/Mediocomunicaciones/index');
		$this->Acl->allow($group, 'controllers/Mediocomunicaciones/view');
		$this->Acl->allow($group, 'controllers/Nolaborables');
		$this->Acl->allow($group, 'controllers/Objetos/index');
		$this->Acl->allow($group, 'controllers/Objetos/view');
		$this->Acl->allow($group, 'controllers/Objetotipos/index');
		$this->Acl->allow($group, 'controllers/Objetotipos/view');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Director General
		$group->id = 8;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/add');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/edit');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->allow($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/Prorrogas');
		$this->Acl->deny($group, 'controllers/Prorrogas/add');
		$this->Acl->allow($group, 'controllers/Reportes');
		$this->Acl->allow($group, 'controllers/Json');
		
		$this->Acl->allow($group, 'controllers/Actuacionesorigenes/index');
		$this->Acl->allow($group, 'controllers/Actuacionesorigenes/view');
		$this->Acl->allow($group, 'controllers/Actuaciontipos/index');
		$this->Acl->allow($group, 'controllers/Actuaciontipos/view');
		$this->Acl->allow($group, 'controllers/Anexos/index');
		$this->Acl->allow($group, 'controllers/Anexos/view');
		$this->Acl->allow($group, 'controllers/Direccioncontroles/index');
		$this->Acl->allow($group, 'controllers/Direccioncontroles/view');
		$this->Acl->allow($group, 'controllers/Eventos/index');
		$this->Acl->allow($group, 'controllers/Eventos/view');
		$this->Acl->allow($group, 'controllers/Fases/index');
		$this->Acl->allow($group, 'controllers/Fases/view');
		$this->Acl->allow($group, 'controllers/Funcionarioroles/index');
		$this->Acl->allow($group, 'controllers/Funcionarioroles/view');
		$this->Acl->allow($group, 'controllers/Funcionarios/index');
		$this->Acl->allow($group, 'controllers/Funcionarios/view');
		$this->Acl->allow($group, 'controllers/Mediocomunicaciones/index');
		$this->Acl->allow($group, 'controllers/Mediocomunicaciones/view');
		$this->Acl->allow($group, 'controllers/Nolaborables');
		$this->Acl->allow($group, 'controllers/Objetos/index');
		$this->Acl->allow($group, 'controllers/Objetos/view');
		$this->Acl->allow($group, 'controllers/Objetotipos/index');
		$this->Acl->allow($group, 'controllers/Objetotipos/view');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Directores
		$group->id = 7;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/add');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/edit');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Equipotrabajos/delete');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->allow($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/ActuacionesfiscalesEventos/finalizar');
		$this->Acl->allow($group, 'controllers/Prorrogas');
		$this->Acl->allow($group, 'controllers/Reportes');
		$this->Acl->allow($group, 'controllers/Json');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Supervisor
		$group->id = 3;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/add');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/edit');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Equipotrabajos/delete');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->allow($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/ActuacionesfiscalesEventos/finalizar');
		$this->Acl->allow($group, 'controllers/Prorrogas/add');
		$this->Acl->allow($group, 'controllers/Reportes');
		$this->Acl->allow($group, 'controllers/Json');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Coordinador
		$group->id = 4;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->deny($group, 'controllers/Actuacionesfiscales/add');
		$this->Acl->deny($group, 'controllers/Actuacionesfiscales/edit');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->deny($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/Eventualidades/add');
		$this->Acl->allow($group, 'controllers/Eventualidades/edit');
		$this->Acl->allow($group, 'controllers/Eventualidades/delete');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/Json');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Abogados
		$group->id = 5;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Operaciones');
		$this->Acl->allow($group, 'controllers/Credenciales');
		$this->Acl->allow($group, 'controllers/Eventualidades');
		$this->Acl->allow($group, 'controllers/AnexosOperaciones');
		$this->Acl->allow($group, 'controllers/Json');
		$this->Acl->allow($group, 'controllers/Users/password');
		$this->Acl->allow($group, 'controllers/Users/edit');
		
		// Asitente Administrativo
		$group->id = 6;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/index');
		$this->Acl->allow($group, 'controllers/Actuacionesfiscales/details');
		$this->Acl->allow($group, 'controllers/Json');
		
		echo "all done";
		exit;
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Filter->addFilters(
			array(
				'username' => array(
					'User.username' => array(
						'operator' => 'LIKE',
						'value' => array(
							'before' => '%', // optional
							'after'  => '%'  // optional
						)
					),
				),
				'funcionario' => array(
					//'Funcionario.nombre' => array(
					//	'select' => $this->Filter->select('Funcionario', $this->User->Funcionario->find('list'))
					//),
					'Funcionario.nombre' => array(
						'operator' => 'LIKE',
						'value' => array(
							'before' => '%', // optional
							'after'  => '%'  // optional
						)
					),
				),
				'group_id' => array(
					'Group.name' => array(
						'select' => $this->Filter->select('Group', $this->User->Group->find('list'))
					),
				),
			)
		);
		$this->Filter->setPaginate('order', "User.group_id"); // optional
		
		// Define conditions
		$this->Filter->setPaginate('conditions', $this->Filter->getConditions());
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Usuario guardado existosamente'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Usuario no pudo ser guardado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$groups = $this->User->Group->find('list');
		$funcionarios = $this->User->Funcionario->find('list');
		$this->set(compact('groups', 'funcionarios'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$auth_user = $this->getAuthUser();
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			unset($this->request->data['User']['password']);
			if ($this->User->save($this->request->data, true, array('username', 'group_id', 'funcionario_id'))) {
				$this->Session->setFlash(__('Usuario ha sido actualizado'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect($this->referer());
			} else {
				$this->Session->setFlash(__('Usuario no pudo ser actualizado. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		if (($auth_user['funcionario_id'] != $this->request->data['User']['funcionario_id']) && $auth_user['group_id'] != 1) {
			$this->Session->setFlash(__('No tiene permisos para realizar esta acción'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect($this->referer());
		}
		$groups = $this->User->Group->find('list');
		$funcionarios = $this->User->Funcionario->find('list');
		$this->set(compact('auth_user', 'groups', 'funcionarios'));
	}

/**
 * password method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function password($id = null) {
		$auth_user = $this->getAuthUser();
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Contraseña actualizada'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('Contraseña no pudo ser actualizada. Intente de nuevo.'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->request->data = $this->User->find('first', $options);
		if (($auth_user['funcionario_id'] != $this->request->data['User']['funcionario_id']) && $auth_user['group_id'] != 1) {
			$this->Session->setFlash(__('No tiene permisos para realizar esta acción'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect($this->referer());
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$user = $this->User->read(null, $id);
		$this->request->onlyAllow('post', 'delete');
		if ($user['User']['bloqueado']) {
			$this->Session->setFlash(__('Registro bloqueado, no puede eliminarse'), 'flash_custom', array('class' => 'alert-error'));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Usuario eliminado'), 'flash_custom', array('class' => 'alert-success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Usuario no pudo ser eliminado'), 'flash_custom', array('class' => 'alert-error'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function login() {
		if ($this->Auth->isAuthorized()) {
			$this->redirect(array('controller' => 'pages', 'action' => 'display', 'home'));
		}
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->Session->setFlash(__('Bienvenido al Sistema de Actuaciones Fiscales'), 'flash_custom', array('class' => 'alert-success'));
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash(__('Usuario o contrasena incorrectos'), 'flash_custom', array('class' => 'alert-error'));
			}
		}
	}
	
	public function logout() {
		$this->Session->setFlash(__('Ha dejado el sistema, hasta pronto'), 'flash_custom', array('class' => 'alert-info'));
		$this->redirect($this->Auth->logout());
	}
}
