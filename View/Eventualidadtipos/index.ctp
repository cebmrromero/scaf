<?php $this->Html->addCrumb('Tipos de Eventualidad', '/eventualidadtipos'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3>Tipos de Eventualidades</h3>
<div class="row-fluid eventualidadtipos index">
	<div class="span12">
		<p>A continuación el listado de tipos de eventualidades:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('denominacion', 'Denominación'); ?></th>
					<th><?php echo $this->Paginator->sort('evento_id', 'Acción'); ?></th>
					<th><?php echo $this->Paginator->sort('afecta_dh', 'Afecta Días Hábiles'); ?></th>
					<th><?php echo $this->Paginator->sort('suma_resta', 'Afectación'); ?></th>
					<th><?php echo $this->Paginator->sort('numero_dh', 'Cantidad de Días'); ?></th>
					<th><?php echo $this->Paginator->sort('es_operacion', 'Es Actividad'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($eventualidadtipos as $eventualidadtipo): ?>
					<tr>
						<td><?php echo h($eventualidadtipo['Eventualidadtipo']['denominacion']); ?>&nbsp;</td>
						<td><?php echo ($eventualidadtipo['Evento']['denominacion']) ? h($eventualidadtipo['Evento']['denominacion']): '-'; ?>&nbsp;</td>
						<td><?php echo ($eventualidadtipo['Eventualidadtipo']['afecta_dh']) ? 'Sí' : 'No'; ?>&nbsp;</td>
						<td><?php echo ($eventualidadtipo['Eventualidadtipo']['numero_dh']) ? ($eventualidadtipo['Eventualidadtipo']['suma_resta']) ? 'Suma' : 'Resta' : '-'; ?>&nbsp;</td>
						<td><?php echo ($eventualidadtipo['Eventualidadtipo']['numero_dh']) ? ($eventualidadtipo['Eventualidadtipo']['numero_dh']) . ' Días': '-'; ?>&nbsp;</td>
						<td><?php echo ($eventualidadtipo['Eventualidadtipo']['es_operacion']) ? 'Sí' : 'No'; ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $eventualidadtipo['Eventualidadtipo']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $eventualidadtipo['Eventualidadtipo']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php if ($eventualidadtipo['Eventualidadtipo']['bloqueado']): ?>
									<button class="btn btn-danger btn-mini disabled">Eliminar</button>
								<?php else: ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $eventualidadtipo['Eventualidadtipo']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $eventualidadtipo['Eventualidadtipo']['id'])); ?>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid fases index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
