<?php $this->Html->addCrumb('Tipos de Eventualidad', '/eventualidadtipos'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $eventualidadtipo['Eventualidadtipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $eventualidadtipo['Eventualidadtipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid eventos view">
	<div class="span8">
		<h3><?php echo __('Eventualidades'); ?></h3>
		<?php if (!empty($eventualidadtipo['Eventualidade'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Detecta'); ?></th>
						<th><?php echo __('Fecha Detección'); ?></th>
						<th><?php echo __('Corrige/Revisa'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<th><?php echo __('Estado'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($eventualidadtipo['Eventualidade'] as $eventualidade): ?>
						<tr>
							<td>#<?php echo $eventualidade['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_deteccion']; ?></td>
							<td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_verificacion']; ?></td>
							<td><?php echo $eventualidade['numero']; ?></td>
							<td><?php echo $eventualidade['asunto']; ?></td>
							<td><?php echo $eventualidade['observaciones']; ?></td>
							<td><?php echo $eventualidadestados[$eventualidade['estado']]; ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay eventualidades asociadas al tipo de eventualidad</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4>Datos del Tipo de Eventualidad</h4>
		<dl class="dl-horizontal">
			<dt>Denominación</dt>
			<dd><?php echo $eventualidadtipo['Eventualidadtipo']['denominacion']; ?>&nbsp;</dd>
			<dt>Acción</dt>
			<dd><?php echo ($eventualidadtipo['Evento']['denominacion']) ? $eventualidadtipo['Evento']['denominacion'] : '-'; ?>&nbsp;</dd>
			<dt>¿Afecta los Días Hábiles?</dt>
			<dd><?php echo ($eventualidadtipo['Eventualidadtipo']['afecta_dh']) ? 'Sí' : 'No'; ?>&nbsp;</dd>
			<dt>¿Cuántos Días?</dt>
			<dd><?php echo ($eventualidadtipo['Eventualidadtipo']['numero_dh']) ? ($eventualidadtipo['Eventualidadtipo']['numero_dh']) . ' Días': '-'; ?></dd>
			<dt>¿Suma o Resta?</dt>
			<dd><?php echo ($eventualidadtipo['Eventualidadtipo']['numero_dh']) ? ($eventualidadtipo['Eventualidadtipo']['suma_resta']) ? 'Suma' : 'Resta' : '-'; ?>&nbsp;</dd>
			<dt>¿Es una Actividad?</dt>
			<dd><?php echo ($eventualidadtipo['Eventualidadtipo']['es_operacion']) ? 'Sí' : 'No'; ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actividades'); ?></h3>
		<?php if (!empty($eventualidadtipo['Operacione'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Remite'); ?></th>
						<th><?php echo __('Fecha Remite'); ?></th>
						<th><?php echo __('Recibe'); ?></th>
						<th><?php echo __('Fecha Recibe'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($eventualidadtipo['Operacione'] as $operacione): ?>
						<tr>
							<td>#<?php echo $operacione['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $operacione['Fremite']['nombre']; ?></td>
							<td><?php echo $operacione['Frecibe']['nombre']; ?></td>
							<td><?php echo $operacione['fecha_remite']; ?></td>
							<td><?php echo $operacione['fecha_recibe']; ?></td>
							<td><?php echo $operacione['observaciones']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'operaciones', 'action' => 'view', $operacione['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'operaciones', 'action' => 'edit', $operacione['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'operaciones', 'action' => 'delete', $operacione['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $operacione['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay actividades asociadas al tipo de eventualidad</p>
		<?php endif; ?>
	</div>
</div>
