<?php $this->Html->addCrumb('Tipos de Eventualidad', '/eventualidadtipos'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Eventualidadtipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid eventualidadtipos form">
	<?php echo $this->Form->create('Eventualidadtipo'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Tipo de Eventualidad'); ?></legend>
			
			<div class="row-fluid">
				<div class="span5 control-group">
					<?php echo $this->Form->input('denominacion', array('type' => 'text', 'class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
				<div class="span1 control-group">
					<?php echo $this->Form->input('posicion', array('class' => 'span12', 'label' => 'Posición')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('evento_id', array('class' => 'span12', 'label' => 'Acción', 'empty' => '-Seleccionar-', 'required' => false)); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span3 control-group">
					<div class="radio">
						<label for="">¿Afecta los Días Hábiles?</label>
						<?php echo $this->Form->input('afecta_dh', array('label' => 'Sí, afecta los días hábiles', 'required' => false)); ?>
					</div>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('numero_dh', array('class' => 'span12', 'label' => '¿Cuántos Días?', 'min' => 0, 'required' => false)); ?>
				</div>
				<div class="span3 control-group">
					<div class="radio">
						<label for="">¿Suma o Resta?</label>
						<?php echo $this->Form->input('suma_resta', array('type' => 'radio', 'required' => false, 'options' => array('1' => 'Suma', '0' => 'Resta'), 'legend' => false)); ?>
					</div>
				</div>
				<div class="span3 control-group">
					<label for="">¿Es una Actividad?</label>
					<?php echo $this->Form->input('es_operacion', array('label' => 'Sí, es una actividad', 'required' => false)); ?>
				</div>
			</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
