<?php $this->Html->addCrumb('Credenciales', '/credenciales'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $credenciale['Credenciale']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $credenciale['Credenciale']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid credenciales view">
	<div class="span8">
		<h3><?php echo __('Aqui el 1era related'); ?></h3>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del / de la Credenciale'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Id'); ?></dt>
			<dd><?php echo h($credenciale['Credenciale']['id']); ?>&nbsp;</dd>
			<dt><?php echo __('Numero'); ?></dt>
			<dd><?php echo h($credenciale['Credenciale']['numero']); ?>&nbsp;</dd>
			<dt><?php echo __('Fecha'); ?></dt>
			<dd><?php echo h($credenciale['Credenciale']['fecha']); ?>&nbsp;</dd>
			<dt><?php echo __('Actuacionesfiscale Id'); ?></dt>
			<dd><?php echo h($credenciale['Credenciale']['actuacionesfiscale_id']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Equipotrabajos'); ?></h3>
		<?php if (!empty($credenciale['Equipotrabajo'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Actuacionesfiscale Id'); ?></th>
						<th><?php echo __('Funcionario Id'); ?></th>
						<th><?php echo __('Funcionariorole Id'); ?></th>
						<th><?php echo __('Credenciale Id'); ?></th>
						<th><?php echo __('Nota Designacion'); ?></th>
						<th><?php echo __('Fecha'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($credenciale['Equipotrabajo'] as $equipotrabajo): ?>
						<tr>
							<td><?php echo $equipotrabajo['id']; ?></td>
							<td><?php echo $equipotrabajo['actuacionesfiscale_id']; ?></td>
							<td><?php echo $equipotrabajo['funcionario_id']; ?></td>
							<td><?php echo $equipotrabajo['funcionariorole_id']; ?></td>
							<td><?php echo $equipotrabajo['credenciale_id']; ?></td>
							<td><?php echo $equipotrabajo['nota_designacion']; ?></td>
							<td><?php echo $equipotrabajo['fecha']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'equipotrabajos', 'action' => 'view', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'equipotrabajos', 'action' => 'edit', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'equipotrabajos', 'action' => 'delete', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $equipotrabajo['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay equipotrabajos asociados al/el Credenciale</p>
		<?php endif; ?>
	</div>
</div>
