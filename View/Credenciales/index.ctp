<?php $this->Html->addCrumb('Credenciales', '/credenciales'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3><?php echo __('Credenciales'); ?></h3><div class="row-fluid credenciales index">
	<div class="span12">
		<p>A continuación el listado de credenciales:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('numero'); ?></th>
					<th><?php echo $this->Paginator->sort('fecha'); ?></th>
					<th><?php echo $this->Paginator->sort('actuacionesfiscale_id'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($credenciales as $credenciale): ?>
					<tr>
						<td><?php echo h($credenciale['Credenciale']['id']); ?>&nbsp;</td>
						<td><?php echo h($credenciale['Credenciale']['numero']); ?>&nbsp;</td>
						<td><?php echo h($credenciale['Credenciale']['fecha']); ?>&nbsp;</td>
						<td><?php echo h($credenciale['Credenciale']['actuacionesfiscale_id']); ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $credenciale['Credenciale']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $credenciale['Credenciale']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $credenciale['Credenciale']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $credenciale['Credenciale']['id'])); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid credenciales index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
