<?php $this->Html->addCrumb(__('Credenciales'), '/credenciales'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid credenciales form">
	<?php echo $this->Form->create('Credenciale'); ?>
		<fieldset>
			<legend><?php echo __('Oficio Credencial'); ?></legend>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php echo $this->Form->input('numero', array('class' => 'span12 mask-oc', 'label' => 'Número')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('fecha', array('type' => 'text', 'class' => 'span12 date', 'label' => 'Fecha')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'text', 'class' => 'span12', 'value' => $actuacionesfiscale_id)); ?>
				</div>
			</div>
		</fieldset>
		
		<fieldset>
			<legend><?php echo __('Nuevo Credenciale'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('numero', array('class' => 'span12', 'label' => 'Numero')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('fecha', array('class' => 'span12', 'label' => 'Fecha')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('actuacionesfiscale_id', array('class' => 'span12', 'label' => 'Actuacionesfiscale Id')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>
			<button type="button" class="btn cancel"><?php echo __("Cancelar"); ?></button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
