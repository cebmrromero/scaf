<?php $this->Html->addCrumb('Reportes'); ?>
<?php $this->Html->addCrumb('Distribución de Tiempo', '/actuacionesfiscales/byTime'); ?>
<?php $this->Html->addCrumb('Detalles'); ?>
<div class="row-fluid">
	<div class="span12">
		<h3>Detalle de los tiempos de actividad</h3>
		<p>A continuación se listan todas las actividades detalladas de la actuación fiscal:</p>
		<table class="table table-hover table-striped table-condensed">
			<caption><h4>Actuación Fiscal: #<?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']; ?></h4><h4><?php echo $evento['Evento']['denominacion']; ?></h4></caption>
			<thead>
				<tr>
					<th>Fecha Remisión</th>
					<th>Funcionario Remite</th>
					<th>Tiempo de Respuesta</th>
					<th>Funcionario Recibe</th>
					<th>Fecha Recepción</th>
					<th>Retraso en Recepción</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($operaciones): ?>
					<?php foreach($operaciones as $operacione): ?>
						<tr>
							<td><?php echo ($operacione['Operacione']['fecha_remite']) ? $this->Time->format('d-m-Y h:i A', $operacione['Operacione']['fecha_remite']) : '-'; ?></td>
							<td><?php echo $operacione['Fremite']['nombre']; ?></td>
							<td><?php echo ($operacione['Operacione']['diff_remision']) ? $operacione['Operacione']['diff_remision'] . ' Días': '-'; ?></td>
							<td><?php echo $operacione['Frecibe']['nombre']; ?></td>
							<td><?php echo ($operacione['Operacione']['fecha_recibe']) ? $this->Time->format('d-m-Y h:i A', $operacione['Operacione']['fecha_recibe']) : '-'; ?></td>
							<td><?php echo ($operacione['Operacione']['diff_recepcion']) ? $operacione['Operacione']['diff_recepcion'] . ' Días': '-'; ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5">No hay registro de actividades para esta actuación fiscal y acción indicados</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>