<?php $this->Html->addCrumb('Reportes'); ?>
<?php $this->Html->addCrumb('Distribución de Tiempo'); ?>
<div class="row-fluid">
	<div class="span12">
		<h3>Distribución de los Tiempos de las Actuaciones Fiscales</h3>
		<p>A continuación se listan todas las actuaciones fiscales acompañando su tiempo de ejecución:</p>
		<?php if ($actuacionesfiscales): ?>
			<?php //print_r($actuacionesfiscales) ?>
			<?php foreach($actuacionesfiscales as $actuacionesfiscale): ?>
				<table class="table table-hover table-striped table-condensed">
					<caption><h4>Actuación Fiscal: #<?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']; ?></h4></caption>
					<thead>
						<tr>
							<td colspan="6">
								<div class="actuacionesfiscaleswell">
									<dl class="pull-left">
										<dt>Objeto de Evaluación</dt>
										<dd><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></dd>
									</dl>
									<dl class="pull-left">
										<dt>Fecha de Inicio Estimada</dt>
										<dd><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio_estimada']); ?></dd>
									</dl>
									<dl class="pull-left">
										<dt>Fecha de Fin Estimada</dt>
										<dd><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['fecha_fin_estimada']); ?></dd>
									</dl>
									<dl class="pull-left">
										<dt>Planificación</dt>
										<dd><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_planificacion']; ?> Días</dd>
									</dl>
									<dl class="pull-left">
										<dt>Ejecución</dt>
										<dd><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_ejecucion']; ?> Días</dd>
									</dl>
									<dl class="pull-left">
										<dt>Resultados</dt>
										<dd><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_resultados']; ?> Días</dd>
									</dl>
									<dl class="pull-left">
										<dt>Total Estimado</dt>
										<dd><?php echo $actuacionesfiscale['Actuacionesfiscale']['total_estimado']; ?> Días</dd>
									</dl>
								</div>
							</td>
						</tr>
						<tr>
							<th>Acción</th>
							<th>Fecha Inicio</th>
							<th>Fecha Fin</th>
							<th>Días Ejecución</th>
							<th>Días Retrazo</th>
							<th>Detalles</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($actuacionesfiscale['Operacione'] as $operacione): ?>
							<tr>
								<td><?php echo $operacione['Evento']['denominacion']; ?></td>
								<td><?php echo ($operacione['fecha_inicio']) ? $this->Time->format('d-m-Y h:i A', $operacione['fecha_inicio']) : '-'; ?></td>
								<td><?php echo ($operacione['fecha_fin']) ? $this->Time->format('d-m-Y h:i A', $operacione['fecha_fin']) : '-'; ?></td>
								<td><?php echo ($operacione['diff']) ? $operacione['diff'] . ' Días': '-'; ?></td>
								<td><?php echo ($operacione['diff_ejecucion']) ? $operacione['diff_ejecucion'] . ' Días': '-'; ?></td>
								<td><?php echo ($operacione['fecha_fin']) ? $this->Html->link('Detalles', array('action' => 'byTimeDetail', $actuacionesfiscale['Actuacionesfiscale']['id'], $operacione['Evento']['id']), array('class' => 'btn btn-mini btn-info')) : '<span class="btn btn-mini btn-info disabled">Detalles</span>'; ?></td>
							</tr>
						<?php endforeach; ?>
						<tr>
							<td><strong>Total días de ejecución</strong></td>
							<td><strong><?php echo  $this->Time->format('d-m-Y h:i A', $actuacionesfiscale['Actuacionesfiscale']['fi']); ?></strong></td>
							<td><strong><?php echo ($actuacionesfiscale['Actuacionesfiscale']['ff']) ?  $this->Time->format('d-m-Y h:i A', $actuacionesfiscale['Actuacionesfiscale']['ff']) : '-'; ?></strong></td>
							<td colspan="2"><strong><?php echo ($actuacionesfiscale['Actuacionesfiscale']['diff_total']) ? $actuacionesfiscale['Actuacionesfiscale']['diff_total'] . ' Días': '-'; ?></strong></td>
						</tr>
					</tbody>
				</table>
			<?php endforeach; ?>
		<?php else: ?>
			<p>No hay actuaciones fiscales registradas</p>
		<?php endif; ?>
	</div>
</div>