<?php $this->Html->addCrumb('Reportes'); ?>
<?php $this->Html->addCrumb('Consolidado de Actuaciones Fiscales'); ?>
<div class="row-fluid">
	<div class="span12">
		<h3>Listado consolidado de actuaciones fiscales</h3>
		<?php echo $this->Form->create("Reporte", array("type" => 'get')); ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th rowspan="2">Dirección</th>
						<th rowspan="2">Actuación Fiscal</th>
						<th rowspan="2">Objeto o Ente</th>
						<th rowspan="2" class="span1">Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th rowspan="2" class="span1">Fin&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th rowspan="2">Tipo</th>
						<th colspan="4" class="text-center">Fases (Días Hábiles)</th>
						<th rowspan="2">Equipo de Trabajo</th>
						<th rowspan="2">Programa Entregado</th>
						<th colspan="3" class="text-center">Entrega de Informes</th>
					</tr>
					<tr>
						<th>Pla.</th>
						<th>Eje.</th>
						<th>Res.</th>
						<th>Total</th>
						<th class="span1">Original&nbsp;&nbsp;&nbsp;&nbsp;</th>
						<th class="span1">Preliminar</th>
						<th class="span1">Definitivo&nbsp;&nbsp;&nbsp;</th>
					</tr>
					<tr>
						<th>
							<?php echo $this->Form->input('direccioncontrole_id', array('value' => isset($this->request->query['direccioncontrole_id']) ? $this->request->query['direccioncontrole_id'] : '', 'empty' => 'Todas', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('actuacionesfiscale_id', array('value' => isset($this->request->query['actuacionesfiscale_id']) ? $this->request->query['actuacionesfiscale_id'] : '', 'options' => $actuacionesfiscales_list, 'empty' => 'Todas', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('objeto_id', array('value' => isset($this->request->query['objeto_id']) ? $this->request->query['objeto_id'] : '', 'options' => $objetos_list, 'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('fecha_desde', array('value' => isset($this->request->query['fecha_desde']) ? $this->request->query['fecha_desde'] : '', 'label' => false, 'placeholder' => 'Desde', 'class' => 'date-from span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('fecha_hasta', array('value' => isset($this->request->query['fecha_hasta']) ? $this->request->query['fecha_hasta'] : '', 'label' => false, 'placeholder' => 'Hasta', 'class' => 'date-to span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('actuaciontipo_id', array('value' => isset($this->request->query['actuaciontipo_id']) ? $this->request->query['actuaciontipo_id'] : '', 'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>
							<?php echo $this->Form->input('funcionario_id', array('value' => isset($this->request->query['funcionario_id']) ? $this->request->query['funcionario_id'] : '', 'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('programa_entregado', array('value' => isset($this->request->query['programa_entregado']) ? $this->request->query['programa_entregado'] : '', 'options' => array('1' => 'Entregado', '0' => 'No entregado'),'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('original', array('value' => isset($this->request->query['original']) ? $this->request->query['original'] : '', 'options' => array('1' => 'Entregado', '0' => 'No entregado'),'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('preliminar', array('value' => isset($this->request->query['preliminar']) ? $this->request->query['preliminar'] : '', 'options' => array('1' => 'Entregado', '0' => 'No entregado'),'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
						<th>
							<?php echo $this->Form->input('definitivo', array('value' => isset($this->request->query['definitivo']) ? $this->request->query['definitivo'] : '', 'options' => array('1' => 'Entregado', '0' => 'No entregado'),'empty' => 'Todos', 'label' => false, 'class' => 'span12')); ?>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($actuacionesfiscales): ?>
						<?php foreach($actuacionesfiscales as $actuacionesfiscale): ?>
							<tr>
								<td><?php echo $actuacionesfiscale['Direccioncontrole']['denominacion']; ?></td>
								<td>#<?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']; ?></td>
								<td><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['fecha_inicio']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['fecha_inicio']) : '-'; ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['fecha_fin']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['fecha_fin']) : '-'; ?></td>
								<td><?php echo $actuacionesfiscale['Actuaciontipo']['denominacion']; ?></td>
								<td><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_fase1']; ?>/<?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_planificacion']; ?></td>
								<td><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_fase2']; ?>/<?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_ejecucion']; ?></td>
								<td><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_fase3']; ?>/<?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_resultados']; ?></td>
								<td><?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_fase1'] + $actuacionesfiscale['Actuacionesfiscale']['dh_fase2'] + $actuacionesfiscale['Actuacionesfiscale']['dh_fase3']; ?>/<?php echo $actuacionesfiscale['Actuacionesfiscale']['dh_planificacion'] + $actuacionesfiscale['Actuacionesfiscale']['dh_ejecucion'] + $actuacionesfiscale['Actuacionesfiscale']['dh_resultados']; ?></td>
								<td><?php echo join(", ", $actuacionesfiscale['Funcionarios']); ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['programa_entregado']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['programa_entregado']) : '-'; ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['informe_original']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['informe_original']) : '-'; ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['informe_preliminar']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['informe_preliminar']) : '-'; ?></td>
								<td><?php echo ($actuacionesfiscale['Actuacionesfiscale']['informe_definitivo']) ? $this->Time->format('d-m-Y', $actuacionesfiscale['Actuacionesfiscale']['informe_definitivo']) : '-'; ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else: ?>
						<tr><td colspan="15">No hay actuaciones fiscales registradas, o no coinciden con los parametros de búsqueda</td></tr>
					<?php endif; ?>
				</tbody>
			</table>
		<?php echo $this->Form->end(); ?>
	</div>
</div>