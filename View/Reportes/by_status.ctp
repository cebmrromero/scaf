<?php $this->Html->addCrumb('Reportes'); ?>
<?php $this->Html->addCrumb('Estado de Actuaciones'); ?>
<div class="row-fluid">
	<div class="span12">
		<h3>Listado de actuaciones fiscales y su estado</h3>
		<p>A continuación se listan todas las actuaciones fiscales detallando el estado de las mismas</p>
		<?php $collapse_index = 0; ?>
        <?php foreach($actuacionesfiscales as $actuacionesfiscale): ?>
            <div class="well">
                <h4>Actuación Fiscal #<?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']; ?></h4>
                <p><?php echo $actuacionesfiscale['Actuacionesfiscale']['obj_general']; ?></p>
                <div class="row-fluid">
                    <div class="progress progress-striped">
                        <?php
                        $completados = 0;
                        $porcompletar = 0;
                        foreach($actuacionesfiscale['ActuacionesfiscalesEvento'] as $evento) {
                                $porcompletar++;
                                if ($evento['es_completado']) {
                                    $completados++;
                                }
                        }
                        $porcentajeindividual = 100 / $porcompletar;
                        $porcentajecompletado = ($completados / $porcompletar) * 100;
                        ?>
                        
                        <?php foreach($actuacionesfiscale['ActuacionesfiscalesEvento'] as $evento): ?>
                            <?php
                                $popovercontent = '';
                                if (!$evento['fecha_inicio'] && !$evento['fecha_fin']) {
                                    $popovercontent = "<p class='text-center'>Acción no iniciada</p>";
                                } elseif ($evento['fecha_inicio'] && !$evento['fecha_fin']) {
                                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y h:i A', $evento['fecha_inicio']) . ' al (en proceso)</p>';
                                } else if ($evento['fecha_inicio'] && $evento['fecha_fin']) {
                                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y h:i A', $evento['fecha_inicio']) . " al " . $this->Time->format('d-m-Y h:i A', $evento['fecha_fin']) . '</p>';
                                }
                                if ($evento['es_activo'] && !$evento['es_completado']) {
                                    $class = ' bar-warning';
                                } elseif ($evento['es_completado']) {
                                    $class = ' bar-success';
                                } else {
                                    $class = ' bar-danger';
                                }
                            ?>
                            <div class="bar<?php echo $class; ?>" style="width: <?php echo $porcentajeindividual; ?>%;" title="<?php echo $evento['Evento']['denominacion']; ?>" data-content="<?php echo $popovercontent; ?>"></div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="row-fluid">
					<div class="accordion" id="accordion<?php echo $collapse_index; ?>">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?php echo $collapse_index; ?>" href="#collapse<?php echo $collapse_index; ?>">
									Detalles de la actuación fiscal
								</a>
							</div>
							<div id="collapse<?php echo $collapse_index; ?>" class="accordion-body collapse out">
								<div class="accordion-inner">
									<table class="table table-hover table-striped table-condensed">
										<thead>
											<tr>
												<th>Fase 1</th>
												<th>Fase 2</th>
												<th>Fase 3</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<?php echo $this->element('actividades-all', array('fase' => 1, 'actuacionesfiscale' => $actuacionesfiscale, 'collapse_index' => $collapse_index)); ?>
												</td>
												<td>
													<?php echo $this->element('actividades-all', array('fase' => 2, 'actuacionesfiscale' => $actuacionesfiscale, 'collapse_index' => $collapse_index)); ?>
												</td>
												<td>
													<?php echo $this->element('actividades-all', array('fase' => 3, 'actuacionesfiscale' => $actuacionesfiscale, 'collapse_index' => $collapse_index)); ?>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
			<?php $collapse_index++; ?>
        <?php endforeach; ?>
	</div>
</div>