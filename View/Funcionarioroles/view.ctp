<?php $this->Html->addCrumb('Roles de los Funcionarios', '/funcionarioroles'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $funcionariorole['Funcionariorole']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $funcionariorole['Funcionariorole']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid funcionarioroles view">
	<div class="span8">
		<h3><?php echo __('Funcionarios que ejecutan el Rol'); ?></h3>
		<?php if (!empty($funcionariorole['Equipotrabajo'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Funcionario'); ?></th>
						<th><?php echo __('Nota de Designación'); ?></th>
						<th><?php echo __('Fecha de Asignación'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionariorole['Equipotrabajo'] as $equipotrabajo): ?>
						<tr>
							<td>#<?php echo $equipotrabajo['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $equipotrabajo['Funcionario']['nombre']; ?></td>
							<td><?php echo $equipotrabajo['nota_designacion']; ?></td>
							<td><?php echo $equipotrabajo['fecha']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'equipotrabajos', 'action' => 'view', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'equipotrabajos', 'action' => 'edit', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'equipotrabajos', 'action' => 'delete', $equipotrabajo['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $equipotrabajo['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay funcionarios asociados al presente rol</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del Rol'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Denominacion'); ?></dt>
			<dd><?php echo h($funcionariorole['Funcionariorole']['denominacion']); ?>&nbsp;</dd>
			<dt><?php echo __('¿Es requerido en las actuaciones fiscales?'); ?></dt>
			<dd><?php echo ($funcionariorole['Funcionariorole']['requerido']) ? 'Sí' : 'No'; ?>&nbsp;</dd>
		</dl>
	</div>
</div>
