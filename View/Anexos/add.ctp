<?php $this->Html->addCrumb('Anexos', '/anexos'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid anexos form">
	<?php echo $this->Form->create('Anexo'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Anexo'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => 'Denominación')); ?>
					<?php echo $this->Form->input('fase_id', array('class' => 'span12', 'label' => 'Fase a la que pertenece')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('Evento', array('class' => 'span12', 'label' => 'Acción asociada al Anexo', 'size' => '5')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
