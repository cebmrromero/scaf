<?php $this->Html->addCrumb('Anexos', '/anexos'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $anexo['Anexo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $anexo['Anexo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid anexos view">
	<div class="span8">
		<h3><?php echo __('Acciones'); ?></h3>
		<?php if (!empty($anexo['Evento'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominación'); ?></th>
						<th><?php echo __('Es Opcional'); ?></th>
						<th><?php echo __('Es Inspección'); ?></th>
						<th><?php echo __('Depende de'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($anexo['Evento'] as $evento): ?>
						<tr>
							<td><?php echo $evento['denominacion']; ?></td>
							<td><?php echo ($evento['es_opcional']) ? 'Sí' : 'No'; ?></td>
							<td><?php echo ($evento['es_inspeccion']) ? 'Sí' : 'No'; ?></td>
							<td><?php echo (isset($evento['Parent']['denominacion'])) ? $evento['Parent']['denominacion']: '-'; ?></td>
							<td><?php echo $evento['Fase']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventos', 'action' => 'view', $evento['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventos', 'action' => 'edit', $evento['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventos', 'action' => 'delete', $evento['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $evento['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay acciones asociadas al Anexo</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del Anexo'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Denominacion'); ?></dt>
			<dd><?php echo h($anexo['Anexo']['denominacion']); ?>&nbsp;</dd>
			<dt><?php echo __('Fase'); ?></dt>
			<dd><?php echo h($anexo['Fase']['denominacion']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>