<?php if ($actuacionesfiscale_id): ?>
	<?php $this->Html->addCrumb('Actuacion Fiscal', '/actuacionesfiscales/details/'.$actuacionesfiscale_id); ?>
	<?php $this->Html->addCrumb('Nueva Eventualidad'); ?>
<?php else: ?>
	<?php $this->Html->addCrumb('Eventualidades', '/eventualidades'); ?>
	<?php $this->Html->addCrumb('Nueva'); ?>
<?php endif; ?>
<div class="eventualidades form">
	<h3>Nueva Eventualidad</h3>
	<?php echo $this->Form->create('Eventualidade'); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Eventualidad'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php if ($actuacionesfiscale_id): ?>
						<label>Actuación Fiscal</label>
						<span><?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']?></span>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'value' => $actuacionesfiscale_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('class' => 'span12', 'label' => 'Actuación Fiscal')); ?>
					<?php endif; ?>
				</div>
				<div class="span3 control-group">
					<?php if ($evento_id): ?>
						<label>Acción</label>
						<span><?php echo $evento['Evento']['denominacion']?></span>
						<?php echo $this->Form->input('evento_id', array('type' => 'hidden', 'value' => $evento_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('evento_id', array('class' => 'span12', 'label' => 'Acción')); ?>
					<?php endif; ?>
				</div>
				<div class="span3 control-group">
					<?php if ($es_correccion || $eventualidadtipo_id): ?>
						<label>Tipo de eventualidad</label>
						<?php echo $this->Form->input('eventualidadtipo_id', array('type' => 'hidden', 'value' => $eventualidadtipo['Eventualidadtipo']['id'])); ?>
						<span><?php echo $eventualidadtipo['Eventualidadtipo']['denominacion']; ?></span>
					<?php else: ?>
						<?php echo $this->Form->input('eventualidadtipo_id', array('label' => 'Tipo de eventualidad', 'class' => 'span12')); ?>
					<?php endif; ?>
				</div>
				<?php if ($es_correccion || $eventualidadtipo_id): ?>
					<div class="span3 control-group">
						<label>Número</label>
						<?php echo $this->Form->input('numero', array('type' => 'hidden', 'value' => $numero_corecciones)); ?>
						<span><?php echo $numero_corecciones; ?></span>
					</div>
				<?php endif; ?>
			</div>
			<div class="row-fluid">
				<div class="span3 control-group">
					<label>De</label>
					<?php echo $this->Form->input('detecta_id', array('type' => 'hidden', 'value' => $auth_user['funcionario_id'])); ?>
					<span><?php echo $auth_user['Funcionario']['nombre']; ?></span>
				</div>
				<div class="span3 control-group">
					<?php if ($corrige_id): ?>
						<label>Para</label>
						<?php echo $this->Form->input('corrige_id', array('type' => 'hidden', 'value' => $corrige['Corrige']['id'])); ?>
						<span><?php echo $corrige['Corrige']['nombre']; ?></span>
					<?php else: ?>
						<?php echo $this->Form->input('corrige_id', array('class' => 'span12', 'label' => 'Para', 'required' => 'required', 'empty' => 'Seleccionar')); ?>
					<?php endif; ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('mediocomunicacione_id', array('class' => 'span12', 'label' => 'Medio de comunicación utilizado')); ?>
				</div>
				<div class="span3 control-group">
					<div class="input text required ">
						<?php echo $this->Form->label('fecha_deteccion', 'Fecha'); ?>
						<div class="input-prepend span10">
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo $this->Form->input('fecha_deteccion', array('type' => 'text', 'readonly' => 'readonly', 'class' => 'span12 date text-center', 'label' => false, 'div' => false, 'value' => date('d-m-Y h:i A'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('asunto', array('class' => 'span12', 'type' => 'text', 'label' => 'Asunto')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('municipio', array('class' => 'span12', 'type' => 'text', 'label' => 'Municipio')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('lugar', array('class' => 'span12', 'type' => 'text', 'label' => 'Lugar')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('observaciones', array('class' => 'span12 editor', 'label' => 'Observaciones')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>