<?php $this->Html->addCrumb('Días no Laborables', '/nolaborables'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3>Días no Laborables</h3>
<div class="row-fluid nolaborables index">
	<div class="span12">
		<p>A continuación el listado de días no laborables:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('dia', 'Fecha'); ?></th>
					<th><?php echo $this->Paginator->sort('descripcion', 'Descripción'); ?></th>
					<th><?php echo $this->Paginator->sort('repetitivo', 'Se repite en el tiempo'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($nolaborables as $nolaborable): ?>
					<tr>
						<td><?php echo $this->Time->format('d-m-Y', (strlen($nolaborable['Nolaborable']['dia']) == 5) ? date('Y') . '-' .  $nolaborable['Nolaborable']['dia'] : $nolaborable['Nolaborable']['dia']); ?>&nbsp;</td>
						<td><?php echo h($nolaborable['Nolaborable']['descripcion']); ?>&nbsp;</td>
						<td><?php echo ($nolaborable['Nolaborable']['repetitivo']) ? 'Sí' : 'No'; ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $nolaborable['Nolaborable']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $nolaborable['Nolaborable']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $nolaborable['Nolaborable']['id'])); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid nolaborables index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
