<?php $this->Html->addCrumb('Días no Laborables', '/nolaborables'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Nolaborable']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid nolaborables form">
	<?php echo $this->Form->create('Nolaborable'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Día no Laborable'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Form->input('dia', array('type' => 'text', 'class' => 'span12 date', 'label' => 'Fecha', 'value' => $this->Time->format('d-m-Y', (strlen($this->request->data['Nolaborable']['dia']) == 5) ? date('Y') . '-' . $this->request->data['Nolaborable']['dia'] : $this->request->data['Nolaborable']['dia']))); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('descripcion', array('class' => 'span12', 'label' => 'Descripción')); ?>
				</div>
				<div class="span3 control-group">
					<label for="">¿Es repetitivo?</label>
					<?php echo $this->Form->input('repetitivo', array('label' => 'Sí, se repite en el tiempo')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
