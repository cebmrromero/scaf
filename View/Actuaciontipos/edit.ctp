<?php $this->Html->addCrumb('Tipos de Actuaciones', '/actuaciontipos'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Actuaciontipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid actuaciontipos form">
	<?php echo $this->Form->create('Actuaciontipo'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Tipo de Actuación'); ?></legend>
			
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>