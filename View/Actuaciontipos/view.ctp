<?php $this->Html->addCrumb('Tipos de Actuaciones', '/actuaciontipos'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $actuaciontipo['Actuaciontipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $actuaciontipo['Actuaciontipo']['id'])); ?></li>
	</ul>
</div>
<div class="row-fluid actuaciontipos view">
	<div class="span8">
		<h3><?php echo __('Actuaciones Fiscales del Tipo de Actuación'); ?></h3>
		<?php if (!empty($actuaciontipo['Actuacionesfiscale'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Objetivo'); ?></th>
						<th class="span2"><?php echo __('Inicio Estimado'); ?></th>
						<th class="span2"><?php echo __('Fin Estimado'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th><?php echo __('Objeto de Evaluación'); ?></th>
						<th class="span1 actions"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($actuaciontipo['Actuacionesfiscale'] as $actuacionesfiscale): ?>
						<tr>
							<td>#<?php echo $actuacionesfiscale['numero']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_general']; ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_inicio_estimada']); ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_fin_estimada']); ?></td>
							<td><?php echo $actuacionesfiscale['Fase']['denominacion']; ?></td>
							<td><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Detalles'), array('controller' => 'actuacionesfiscales', 'action' => 'details', $actuacionesfiscale['id']), array('class' => 'btn btn-info btn-mini')); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="text-warning">No hay actuaciones fiscales asociadas al presente tipo</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4>Datos del Tipo de Actuación</h4>
		<dl class="dl-horizontal">
			<dt>Denominación</dt>
			<dd><?php echo $actuaciontipo['Actuaciontipo']['denominacion']; ?>&nbsp;</dd>
		</dl>
	</div>
</div>
