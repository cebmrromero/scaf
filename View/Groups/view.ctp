<?php $this->Html->addCrumb('Grupos', '/groups'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $group['Group']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $group['Group']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid groups view">
	<div class="span8">
		<h3><?php echo __('Usuarios del Grupo'); ?></h3>
		<?php if (!empty($group['User'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Nombre de Usuario'); ?></th>
						<th><?php echo __('Funcionario'); ?></th>
						<th><?php echo __('Fecha de Creación'); ?></th>
						<th><?php echo __('Fecha de Modificación'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($group['User'] as $user): ?>
						<tr>
							<td><?php echo $user['username']; ?></td>
							<td><?php echo $user['Funcionario']['nombre']; ?></td>
							<td><?php echo $user['created']; ?></td>
							<td><?php echo $user['modified']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'users', 'action' => 'edit', $user['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'users', 'action' => 'delete', $user['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $user['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay Usuarios asociados al Grupo</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del Grupo'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Nombre'); ?></dt>
			<dd><?php echo h($group['Group']['name']); ?>&nbsp;</dd>
			<dt><?php echo __('Fecha de Creación'); ?></dt>
			<dd><?php echo h($group['Group']['created']); ?>&nbsp;</dd>
			<dt><?php echo __('Fecha de Modificación'); ?></dt>
			<dd><?php echo h($group['Group']['modified']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>
