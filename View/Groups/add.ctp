<?php $this->Html->addCrumb('Grupos', '/groups'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid groups form">
	<?php echo $this->Form->create('Group'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Grupo'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('name', array('class' => 'span12', 'label' => 'Nombre')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
