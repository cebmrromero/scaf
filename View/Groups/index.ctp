<?php $this->Html->addCrumb('Grupos', '/groups'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3>Grupos</h3>
<div class="row-fluid groups index">
	<div class="span12">
		<p>A continuación el listado de grupos:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('name', 'Nombre'); ?></th>
					<th><?php echo $this->Paginator->sort('created', 'Fecha de Creación'); ?></th>
					<th><?php echo $this->Paginator->sort('modified', 'Fecha de Modificación'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($groups as $group): ?>
					<tr>
						<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
						<td><?php echo h($group['Group']['created']); ?>&nbsp;</td>
						<td><?php echo h($group['Group']['modified']); ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $group['Group']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $group['Group']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<button class="btn btn-danger btn-mini disabled">Eliminar</button>
								<?php //echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $group['Group']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $group['Group']['id'])); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid groups index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
