<?php $this->Html->addCrumb('Funcionarios', '/funcionarios'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $funcionario['Funcionario']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $funcionario['Funcionario']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid funcionarios view">
	<div class="span8">
		<h3><?php echo __('Equipos de Trabajo donde participa'); ?></h3>
		<?php if (!empty($funcionario['Equipotrabajo'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Nota de Designación'); ?></th>
						<th><?php echo __('Fecha de Asignación'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Equipotrabajo'] as $equipotrabajo): ?>
						<tr>
							<td>#<?php echo $equipotrabajo['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $equipotrabajo['nota_designacion']; ?></td>
							<td><?php echo $equipotrabajo['fecha']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Detalles'), array('controller' => 'actuacionesfiscales', 'action' => 'details', $equipotrabajo['Actuacionesfiscale']['id']), array('class' => 'btn btn-mini btn-info')); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No asociados a ningún equipo de trabajo</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del Funcionario o Funcionaria'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Nombre'); ?></dt>
			<dd><?php echo h($funcionario['Funcionario']['nombre']); ?>&nbsp;</dd>
			<dt><?php echo __('Dirección de Control'); ?></dt>
			<dd><?php echo ($funcionario['Direccioncontrole']['denominacion']) ? h($funcionario['Direccioncontrole']['denominacion']) : 'n/a'; ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Prorrogas Solicitadas'); ?></h3>
		<?php if (!empty($funcionario['Solicita'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Aprueba'); ?></th>
						<th><?php echo __('Fecha Solicitud'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Está Aprobada'); ?></th>
						<th><?php echo __('Número de Días'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Solicita'] as $solicita): ?>
						<tr>
							<td>#<?php echo $solicita['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $solicita['Aprueba']['nombre']; ?></td>
							<td><?php echo $solicita['fecha_solicitud']; ?></td>
							<td><?php echo $solicita['fecha_verificacion']; ?></td>
							<td><?php echo ($solicita['es_aprobada']) ? 'Sí': 'No'; ?></td>
							<td><?php echo $solicita['ndias']; ?> Días</td>
							<td><?php echo $solicita['asunto']; ?></td>
							<td><?php echo $solicita['observaciones']; ?></td>
							<!--<td class="actions">-->
							<!--	<div class="btn-group">-->
							<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'prorrogas', 'action' => 'view', $solicita['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'prorrogas', 'action' => 'edit', $solicita['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'prorrogas', 'action' => 'delete', $solicita['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $solicita['id'])); ?>-->
							<!--	</div>-->
							<!--</td>-->
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No ha solicitado ninguna prorroga</p>
		<?php endif; ?>
	</div>
</div>
<?php if ($funcionario['User'][0]['group_id'] == 1): ?>
	<div class="row-fluid related">
		<div class="span12">
			<h3><?php echo __('Prorrogas Asignadas al Contralor'); ?></h3>
			<?php if (!empty($funcionario['Aprueba'])): ?>
				<table class="table table-hover table-striped table-condensed">
					<thead>
						<tr>
							<th><?php echo __('Actuación Fiscal'); ?></th>
							<th><?php echo __('Solicita'); ?></th>
							<th><?php echo __('Fecha Solicitud'); ?></th>
							<th><?php echo __('Fecha Verificación'); ?></th>
							<th><?php echo __('Está Aprobada'); ?></th>
							<th><?php echo __('Número de Días'); ?></th>
							<th><?php echo __('Asunto'); ?></th>
							<th><?php echo __('Observaciones'); ?></th>
							<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						foreach ($funcionario['Aprueba'] as $aprueba): ?>
							<tr>
								<td>#<?php echo $aprueba['Actuacionesfiscale']['numero']; ?></td>
								<td><?php echo $aprueba['Solicita']['nombre']; ?></td>
								<td><?php echo $aprueba['fecha_solicitud']; ?></td>
								<td><?php echo $aprueba['fecha_verificacion']; ?></td>
								<td><?php echo ($aprueba['es_aprobada']) ? 'Sí': 'No'; ?></td>
								<td><?php echo $aprueba['ndias']; ?> Días</td>
								<td><?php echo $aprueba['asunto']; ?></td>
								<td><?php echo $aprueba['observaciones']; ?></td>
								<!--<td class="actions">-->
								<!--	<div class="btn-group">-->
								<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'prorrogas', 'action' => 'view', $aprueba['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
								<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'prorrogas', 'action' => 'edit', $aprueba['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
								<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'prorrogas', 'action' => 'delete', $aprueba['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $aprueba['id'])); ?>-->
								<!--	</div>-->
								<!--</td>-->
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else: ?>
				<p>No hay prorrogas designadas al contralor</p>
			<?php endif; ?>
		</div>
	</div>
<?php endif; ?>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actividades Remitidas'); ?></h3>
		<?php if (!empty($funcionario['Fremite'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Remite'); ?></th>
						<th><?php echo __('Fecha Remite'); ?></th>
						<th><?php echo __('Recibe'); ?></th>
						<th><?php echo __('Fecha Recibe'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Fremite'] as $fremite): ?>
						<tr>
							<td><?php echo isset($fremite['Eventualidadtipo']['denominacion']) ? $fremite['Eventualidadtipo']['denominacion'] : 'Regular'; ?></td>
							<td>#<?php echo $fremite['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $funcionario['Funcionario']['nombre']; ?></td>
							<td><?php echo $fremite['fecha_remite']; ?></td>
							<td><?php echo $fremite['Frecibe']['nombre']; ?></td>
							<td><?php echo $fremite['fecha_recibe']; ?></td>
							<td><?php echo $fremite['observaciones']; ?></td>
							<!--<td class="actions">-->
							<!--	<div class="btn-group">-->
							<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'operaciones', 'action' => 'view', $fremite['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'operaciones', 'action' => 'edit', $fremite['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'operaciones', 'action' => 'delete', $fremite['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $fremite['id'])); ?>-->
							<!--	</div>-->
							<!--</td>-->
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No ha remitido actividades</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actividades Recibidas'); ?></h3>
		<?php if (!empty($funcionario['Frecibe'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Remite'); ?></th>
						<th><?php echo __('Fecha Remite'); ?></th>
						<th><?php echo __('Recibe'); ?></th>
						<th><?php echo __('Fecha Recibe'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Frecibe'] as $frecibe): ?>
						<tr>
							<td><?php echo isset($frecibe['Eventualidadtipo']['denominacion']) ? $frecibe['Eventualidadtipo']['denominacion'] : 'Regular'; ?></td>
							<td>#<?php echo $frecibe['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $frecibe['Fremite']['nombre']; ?></td>
							<td><?php echo $frecibe['fecha_remite']; ?></td>
							<td><?php echo $funcionario['Funcionario']['nombre']; ?></td>
							<td><?php echo $frecibe['fecha_recibe']; ?></td>
							<td><?php echo $frecibe['observaciones']; ?></td>
							<!--<td class="actions">-->
							<!--	<div class="btn-group">-->
							<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'operaciones', 'action' => 'view', $frecibe['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'operaciones', 'action' => 'edit', $frecibe['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'operaciones', 'action' => 'delete', $frecibe['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $frecibe['id'])); ?>-->
							<!--	</div>-->
							<!--</td>-->
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No ha recibido actividades</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Eventualidades Detectadas'); ?></h3>
		<?php if (!empty($funcionario['Detecta'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Detecta'); ?></th>
						<th><?php echo __('Fecha Detección'); ?></th>
						<th><?php echo __('Corrige/Revisa'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Estado'); ?></th>
						<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Detecta'] as $detecta): ?>
						<tr>
							<td><?php echo $detecta['Eventualidadtipo']['denominacion']; ?></td>
							<td>#<?php echo $detecta['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $funcionario['Funcionario']['nombre']; ?></td>
							<td><?php echo $detecta['fecha_deteccion']; ?></td>
							<td><?php echo $detecta['Corrige']['nombre']; ?></td>
							<td><?php echo $detecta['fecha_verificacion']; ?></td>
							<td><?php echo $detecta['numero']; ?></td>
							<td><?php echo $detecta['asunto']; ?></td>
							<td><?php echo $eventualidadestados[$detecta['estado']]; ?></td>
							<!--<td class="actions">-->
							<!--	<div class="btn-group">-->
							<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventualidades', 'action' => 'view', $detecta['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventualidades', 'action' => 'edit', $detecta['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $detecta['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $detecta['id'])); ?>-->
							<!--	</div>-->
							<!--</td>-->
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No tiene eventualidades detectadas</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Eventualidades Corregidas/Revisadas'); ?></h3>
		<?php if (!empty($funcionario['Corrige'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Detecta'); ?></th>
						<th><?php echo __('Fecha Detección'); ?></th>
						<th><?php echo __('Corrige/Revisa'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Estado'); ?></th>
						<!--<th class="actions span1"><?php echo __('Acciones'); ?></th>-->
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($funcionario['Corrige'] as $corrige): ?>
						<tr>
							<td><?php echo $corrige['Eventualidadtipo']['denominacion']; ?></td>
							<td>#<?php echo $corrige['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $corrige['Detecta']['nombre']; ?></td>
							<td><?php echo $corrige['fecha_deteccion']; ?></td>
							<td><?php echo $funcionario['Funcionario']['nombre']; ?></td>
							<td><?php echo $corrige['fecha_verificacion']; ?></td>
							<td><?php echo $corrige['numero']; ?></td>
							<td><?php echo $corrige['asunto']; ?></td>
							<td><?php echo $eventualidadestados[$corrige['estado']]; ?></td>
							<!--<td class="actions">-->
							<!--	<div class="btn-group">-->
							<!--		<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventualidades', 'action' => 'view', $corrige['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventualidades', 'action' => 'edit', $corrige['id']), array('class' => 'btn btn-mini btn-info')); ?>-->
							<!--		<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $corrige['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $corrige['id'])); ?>-->
							<!--	</div>-->
							<!--</td>-->
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No tiene eventualidades corregidas o revisadas</p>
		<?php endif; ?>
	</div>
</div>