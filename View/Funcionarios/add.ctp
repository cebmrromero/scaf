<?php $this->Html->addCrumb('Funcionarios', '/funcionarios'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid funcionarios form">
	<?php echo $this->Form->create('Funcionario'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Funcionario'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('nombre', array('class' => 'span12', 'label' => 'Nombre Completo')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('direccioncontrole_id', array('class' => 'span12', 'empty' => true, 'label' => 'Dirección de Control')); ?>
				</div>
			</div>
			<legend><?php echo __('Datos del Usuario'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Form->input('User.0.id'); ?>
					<?php echo $this->Form->input('User.0.username', array('class' => 'span12', 'label' => 'Nombre de Usuario')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('User.0.password', array('class' => 'span12', 'required' => false, 'value' => '', 'label' => 'Contraseña')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('User.0.password2', array('type' => 'password', 'required' => false, 'value' => '', 'class' => 'span12', 'label' => 'Confirmar Contraseña')); ?>
				</div>
				<?php if ($auth_user['group_id'] == 1): ?>
					<div class="span3 control-group">
						<?php echo $this->Form->input('User.0.group_id', array('class' => 'span12', 'label' => 'Grupo')); ?>
					</div>
				<?php endif; ?>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
