<?php $this->Html->addCrumb('Funcionarios', '/funcionarios'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="search-div well">
	<?php echo $this->Search->create(); ?>
		<fieldset>
			<legend><?php echo __('Buscar Funcionario'); ?></legend>
			<div class="row-fluid">
				<div class="span5 control-group">
					<?php echo $this->Search->input('nombre', array('class' => 'span12', 'required' => false, 'label' => 'Nombre del Funcionario')); ?>
				</div>
				<div class="span5 control-group">
					<?php echo $this->Search->input('direccioncontrole_id', array('class' => 'span12', 'required' => false, 'label' => 'Dirección de Control', 'empty' => 'Cualquiera')); ?>
				</div>
				<div class="span2 control-group">
					<label>&nbsp;</label>
					<?php echo $this->Form->input('Buscar', array('type' => 'submit', 'class' => 'btn btn-primary', 'label' => false, 'div' => false)); ?>
					<?php echo $this->Html->link('Limpiar', array('action' => 'index')); ?>
				</div>
			</div>
		</fieldset>
	<?php echo $this->Search->end(); ?>
</div>

<h3>Funcionarios</h3>
<div class="row-fluid funcionarios index">
	<div class="span12">
		<p>A continuación el listado de funcionarios:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('nombre', 'Nombre Completo'); ?></th>
					<th><?php echo $this->Paginator->sort('direccioncontrole_id', 'Dirección de Control'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($funcionarios as $funcionario): ?>
					<tr>
						<td><?php echo h($funcionario['Funcionario']['nombre']); ?>&nbsp;</td>
						<td><?php echo ($funcionario['Direccioncontrole']['denominacion']) ? h($funcionario['Direccioncontrole']['denominacion']) : '-'; ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $funcionario['Funcionario']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $funcionario['Funcionario']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php if ($funcionario['Funcionario']['bloqueado']): ?>
									<button class="btn btn-danger btn-mini disabled">Eliminar</button>
								<?php else: ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $funcionario['Funcionario']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $funcionario['Funcionario']['id'])); ?>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid funcionarios index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
