<?php $this->Html->addCrumb('Funcionarios', '/funcionarios'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Funcionario']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid funcionarios form">
	<?php echo $this->Form->create('Funcionario'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Funcionario'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('nombre', array('class' => 'span12', 'label' => 'Nombre Completo')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('direccioncontrole_id', array('class' => 'span12', 'empty' => true, 'label' => 'Dirección de Control')); ?>
				</div>
			</div>
			<legend><?php echo __('Datos del Usuario'); ?></legend>
			<?php for ($i = 0; $i < sizeof($this->request->data['User']); $i++): ?>
				<div class="row-fluid">
					<div class="span3 control-group">
						<?php echo $this->Form->input('User.0.id'); ?>
						<?php echo $this->Form->input('User.0.username', array('class' => 'span12', 'label' => 'Nombre de Usuario')); ?>
					</div>
					<?php if ($auth_user['group_id'] == 1): ?>
						<div class="span3 control-group">
							<?php echo $this->Form->input('User.0.group_id', array('class' => 'span12', 'label' => 'Grupo')); ?>
						</div>
					<?php endif; ?>
				</div>
			<?php endfor; ?>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
