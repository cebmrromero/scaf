<?php if ($actuacionesfiscale_id): ?>
	<?php $this->Html->addCrumb('Actuacion Fiscal', '/actuacionesfiscales/details/'.$actuacionesfiscale_id); ?>
	<?php $this->Html->addCrumb('Solicitar Prórroga'); ?>
<?php else: ?>
	<?php $this->Html->addCrumb('Prórrogas', '/prorrogas'); ?>
	<?php $this->Html->addCrumb('Solicitar'); ?>
<?php endif; ?>
<div class="prorrogas form">
	<h3>Solicitar Prórroga</h3>
	<?php echo $this->Form->create('Prorroga'); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Prórroga'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php if ($actuacionesfiscale_id): ?>
						<label>Actuación Fiscal</label>
						<span><?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']?></span>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'value' => $actuacionesfiscale_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('class' => 'span12', 'label' => 'Actuación Fiscal')); ?>
					<?php endif; ?>
				</div>
				<div class="span4 control-group">
					<?php if ($fase_id): ?>
						<label>Fase</label>
						<span><?php echo $fase_id. '. ' . $fase['Fase']['denominacion']?></span>
						<?php echo $this->Form->input('fase_id', array('type' => 'hidden', 'value' => $fase_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('fase_id', array('class' => 'span12', 'label' => 'Fase')); ?>
					<?php endif; ?>
				</div>
				<div class="span3 control-group">
					<label>Solicitante</label>
					<?php echo $this->Form->input('solicita_id', array('type' => 'hidden', 'value' => $auth_user['funcionario_id'])); ?>
					<span><?php echo $auth_user['Funcionario']['nombre']; ?></span>
				</div>
				<div class="span2 control-group">
					<label>Fecha de Solicitud</label>
					<span><?php echo date('d-m-Y h:i A'); ?></span>
					<?php echo $this->Form->input('fecha_solicitud', array('type' => 'hidden', 'value' => date("d-m-Y h:i A"))); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span10 control-group">
					<?php echo $this->Form->input('asunto', array('type' => 'text', 'class' => 'span12', 'label' => 'Asunto')); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('ndias', array('class' => 'span12', 'label' => 'Días de Prórroga', 'value' => 1, 'min' => 1)); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('observaciones', array('class' => 'span12 editor', 'label' => 'Detalle los motivos')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>