<?php $this->Html->addCrumb('Prórrogas', '/prorrogas'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="prorrogas form">
	<h3>Editar Prórroga</h3>
	<?php echo $this->Form->create('Prorroga'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Prórroga'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<label>Actuación Fiscal</label>
					<span><?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']?></span>
					<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'value' => $this->request->data['Prorroga']['actuacionesfiscale_id'])); ?>
				</div>
				<div class="span4 control-group">
					<label>Fase</label>
					<span><?php echo $this->request->data['Prorroga']['fase_id']. '. ' . $fase['Fase']['denominacion']?></span>
					<?php echo $this->Form->input('fase_id', array('type' => 'hidden')); ?>
				</div>
				<div class="span3 control-group">
					<label>Solicitante</label>
					<?php echo $this->Form->input('solicita_id', array('type' => 'hidden', 'value' => $auth_user['funcionario_id'])); ?>
					<span><?php echo $auth_user['Funcionario']['nombre']; ?></span>
				</div>
				<div class="span2 control-group">
					<label>Fecha de Solicitud</label>
					<span><?php echo $this->Time->format('d-m-Y h:i A', $this->request->data['Prorroga']['fecha_solicitud']); ?></span>
					<?php echo $this->Form->input('fecha_solicitud', array('type' => 'hidden')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span10 control-group">
					<?php echo $this->Form->input('asunto', array('type' => 'text', 'class' => 'span12', 'label' => 'Asunto')); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('ndias', array('class' => 'span12', 'label' => 'Días de Prórroga')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('observaciones', array('class' => 'span12 editor', 'label' => 'Detalle los motivos')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('nota', array('class' => 'span12 editor', 'label' => 'Indicar una nota si es necesaria', 'row' => '2')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>