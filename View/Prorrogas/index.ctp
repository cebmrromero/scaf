<?php $this->Html->addCrumb('Prórrogas', '/prorrogas'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3><?php echo __('Prorrogas'); ?></h3><div class="row-fluid prorrogas index">
	<div class="span12">
		<p>A continuación el listado de prórrogas:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('solicita_id', 'Solicitante'); ?></th>
					<th><?php echo $this->Paginator->sort('fecha_solicitud', 'Fecha de Solicitud'); ?></th>
					<th><?php echo $this->Paginator->sort('asunto', 'Asunto'); ?></th>
					<th><?php echo $this->Paginator->sort('ndias', 'Días de Prórroga'); ?></th>
					<th><?php echo $this->Paginator->sort('actuacionesfiscale_id', 'Actuación'); ?></th>
					<th><?php echo $this->Paginator->sort('fase_id', 'Fase'); ?></th>
					<th><?php echo $this->Paginator->sort('fecha_verificacion', 'Fecha de Verificación'); ?></th>
					<th><?php echo $this->Paginator->sort('es_aprobada', 'Estado'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($prorrogas as $prorroga): ?>
					<tr>
						<td><?php echo h($prorroga['Solicita']['nombre']); ?>&nbsp;</td>
						<td><?php echo h($prorroga['Prorroga']['fecha_solicitud']); ?>&nbsp;</td>
						<td><?php echo h($prorroga['Prorroga']['asunto']); ?>&nbsp;</td>
						<td><?php echo h($prorroga['Prorroga']['ndias']); ?>&nbsp;</td>
						<td><?php echo $this->Html->link("#" . $prorroga['Actuacionesfiscale']['numero'], array('controller' => 'actuacionesfiscales', 'action' => 'details', $prorroga['Actuacionesfiscale']['id'])); ?>&nbsp;</td>
						<td><?php echo h($prorroga['Prorroga']['fase_id']); ?>&nbsp;</td>
						<td><?php echo h($prorroga['Prorroga']['fecha_verificacion']); ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group" data-toggle="buttons-radio">
								<?php echo $prorroga['Prorroga']['es_aprobada']; ?>
								<?php if ($prorroga['Prorroga']['es_aprobada'] == '2'): ?>
									<?php echo $this->Html->link('Editar', array('action' => 'edit', $prorroga['Prorroga']['id']), array('class' => 'btn btn-info btn-mini')); ?>
									<?php echo $this->Html->link('Aprobar', array('action' => 'marcar_aprobada', $prorroga['Prorroga']['id']), array('class' => 'btn btn-success btn-mini')); ?>
									<?php echo $this->Html->link('Negar', array('action' => 'marcar_negada', $prorroga['Prorroga']['id']), array('class' => 'btn btn-danger btn-mini')); ?>
								<?php elseif ($prorroga['Prorroga']['es_aprobada'] == '1'): ?>
									<span class="btn btn-success btn-mini disabled">Aprobada</span>
								<?php else: ?>
									<span class="btn btn-danger btn-mini disabled">Negada</span>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid prorrogas index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
