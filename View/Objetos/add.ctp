<?php $this->Html->addCrumb('Objetos de Evaluación', '/objetos'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid eventos form">
	<?php echo $this->Form->create('Objeto'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Objeto de Evaluación'); ?></legend>
			
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('denominacion', array('type' => 'text', 'class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<div class="row-fluid">
						<?php echo $this->Form->input('siglas', array('type' => 'text', 'class' => 'span12', 'label' => 'Siglas')); ?>
					</div>
					<div class="row-fluid">
						<?php echo $this->Form->input('objetotipo_id', array('class' => 'span12', 'label' => 'Tipo de Objeto')); ?>
					</div>
				</div>
				<div class="span6 control-group">
					<div class="span12 control-group">
						<?php echo $this->Form->input('direccion', array('class' => 'span12', 'label' => 'Dirección', 'rows' => '4')); ?>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="span12 control-group">
						<?php echo $this->Form->input('telefonos', array('class' => 'span12', 'label' => 'Teléfonos', 'rows' => '4')); ?>
					</div>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
