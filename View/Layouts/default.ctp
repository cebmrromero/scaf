<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'SCAF: Sistema de Control de Actuaciones Fiscales');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->meta('viewport', "width=device-width, initial-scale=1.0");

		echo $this->Html->css(array(
			'bootstrap-wysihtml5',
			'bootstrap.min',
			'bootstrap-responsive.min',
			'redmond/jquery-ui-1.10.2.custom.min',
			'bootstrap-multiselect',
			'main'
		));
		echo $this->Html->script(array(
			'wysihtml5-0.3.0.min',
			'jquery-1.9.1.min',
			'jquery-ui-1.10.2.custom.min',
			'bootstrap.min',
			'bootstrap-wysihtml5',
			'bootstrap-wysihtml5.es-ES',
			'jquery.ui.datepicker-es',
			'jquery.mask.min',
			'bootstrap-multiselect',
			'modal',
			'main'
		));
		
		echo $this->Html->scriptBlock("
			var BASE_URL = '{$this->base}';
			var MASK_AF = '" . Configure::read('App.MASK_AF') . "';
			var MASK_ND = '" . Configure::read('App.MASK_ND') . "';
			var MASK_OC = '" . Configure::read('App.MASK_OC') . "';
		");

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container" class="container">
		<div id="header">
			<h1 class="muted"><?php echo $cakeDescription; ?></h1>
			<div class="navbar">
				<div class="navbar-inner">
					<div class="container">
						<ul class="nav">
							<li><?php echo $this->Html->link(__d('cake_dev', 'Inicio'), '/'); ?></li>
							<?php if ($user = $this->Session->read('Auth.User')): ?>
								<li class="dropdown">
									<?php echo $this->Html->link(__d('cake_dev', 'Actuaciones Fiscales <b class="caret"></b>'), '#', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
									<ul class="dropdown-menu">
										<li><?php echo $this->Html->link(__d('cake_dev', 'Mis Actuaciones'), array('controller' => 'actuacionesfiscales', 'action' => 'index')); ?></li>	
										<li><?php echo $this->Html->link(__d('cake_dev', 'Nueva'), array('controller' => 'actuacionesfiscales', 'action' => 'add')); ?></li>
										<li class="divider"></li>
										<li><?php echo $this->Html->link(__d('cake_dev', 'Prórrogas'), array('controller' => 'prorrogas', 'action' => 'index')); ?></li>
										<li class="divider"></li>
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Reportes'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Estado de Actuaciones'), array('controller' => 'reportes', 'action' => 'byStatus')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Distribución de Tiempo de las Actuaciones'), array('controller' => 'reportes', 'action' => 'byTime')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Consolidado de Actuaciones'), array('controller' => 'reportes', 'action' => 'consolidado')); ?></li>
											</ul>
										</li>
									</ul>
								</li>
							<?php endif; ?>
							<?php if ($user = $this->Session->read('Auth.User') && ($user['group_id'] == 1 || $user['group_id'] == 2)): ?>
								<li class="dropdown">
									<?php $admin_text = ($user['group_id'] == 1) ? 'Administración' : 'Consultas'; ?>
									<?php echo $this->Html->link(__d('cake_dev', $admin_text . ' <b class="caret"></b>'), '#', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
									<ul class="dropdown-menu">
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Definiciones Básicas'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Direcciones de Control'), array('controller' => 'direccioncontroles', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Acciones'), array('controller' => 'eventos', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Fases'), array('controller' => 'fases', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Objetos de Evaluación'), array('controller' => 'objetos', 'action' => 'index')); ?></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Complementos'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Tipos Actuaciones'), array('controller' => 'actuaciontipos', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Tipos de Eventualidad'), array('controller' => 'eventualidadtipos', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Tipos de Objetos a Evaluar'), array('controller' => 'objetotipos', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Anexos'), array('controller' => 'anexos', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Orígenes de las Actuaciones'), array('controller' => 'actuacionesorigenes', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Medios de Comunicación'), array('controller' => 'mediocomunicaciones', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Días no Laborables'), array('controller' => 'nolaborables', 'action' => 'index')); ?></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Personal'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Roles de los Funcionarios'), array('controller' => 'funcionarioroles', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Funcionarios'), array('controller' => 'funcionarios', 'action' => 'index')); ?></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Seguridad'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Grupos'), array('controller' => 'groups', 'action' => 'index')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Usuarios'), array('controller' => 'users', 'action' => 'index')); ?></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<?php echo $this->Html->link(__d('cake_dev', 'Respaldo y Recuperación'), '#', array('tabindex' => '-1')); ?>
											<ul class="dropdown-menu">
												<li><?php echo $this->Html->link(__d('cake_dev', 'Generar Respaldo'), array('controller' => 'backup', 'action' => 'respaldo')); ?></li>
												<li><?php echo $this->Html->link(__d('cake_dev', 'Realizar Recuperación'), array('controller' => 'backup', 'action' => 'recuperacion')); ?></li>
											</ul>
										</li>
									</ul>
								</li>
							<?php endif; ?>
							<li<?php echo ($this->request->url == 'pages/faq') ? ' class="active"' : ''; ?>><?php echo $this->Html->link(__d('cake_dev', 'Preguntas Frecuentes'), array('controller' => 'pages', 'action' => 'display', 'faq'), array('escape' => false)); ?></li>
							<li<?php echo ($this->request->url == 'pages/about') ? ' class="active"' : ''; ?>><?php echo $this->Html->link(__d('cake_dev', 'Acerca de SCAF'), array('controller' => 'pages', 'action' => 'display', 'about'), array('escape' => false)); ?></li>
							<li<?php echo ($this->request->url == 'pages/help') ? ' class="active"' : ''; ?>><?php echo $this->Html->link(__d('cake_dev', 'Ayuda (?)'), array('controller' => 'pages', 'action' => 'display', 'help'), array('escape' => false)); ?></li>
						</ul>
						<ul class="nav pull-right user-login">
							<?php if ($user = $this->Session->read('Auth.User')): ?>
								<li class="pull-right"><?php echo $this->Html->link("<em>" . $user['Funcionario']['nombre'] . "</em>", array("controller" => "users", "action" => "edit", $user['id']), array("escape" => false)); ?></li>
								<li class="pull-right"><a id="pending-container" alt="<?php echo $this->Html->url(array('controller' => 'json', 'action' => 'getPending')); ?>">&nbsp;</a></li>
								<li class="pull-right"><?php echo $this->Html->link(__d('cake_dev', '<strong>Salir</strong>'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?></li>
							<?php else: ?>
								<li class="pull-right"><?php echo $this->Html->link(__d('cake_dev', '<strong>Ingresar</strong>'), array('controller' => 'users', 'action' => 'login'), array('escape' => false)); ?></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<?php echo $this->Html->getCrumbList(array('separator' => '<span class="divider">/</span>', 'class' => 'breadcrumb', 'lastClass' => array('class' => 'active')), array('text' => '<i class="icon-home"></i> Inicio', 'escape' => false)); ?>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->Session->flash('auth'); ?>
		<div id="content">
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
