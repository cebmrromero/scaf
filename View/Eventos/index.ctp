<?php $this->Html->addCrumb('Acciones', '/eventos'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3>Acciones</h3>
<div class="row-fluid eventos index">
	<div class="span12">
		<p>A continuación el listado de acciones que se llevarán a cabo en cada actuación fiscal:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('denominacion', 'Denominación'); ?></th>
					<th><?php echo $this->Paginator->sort('parent_id', 'Depende de'); ?></th>
					<th><?php echo $this->Paginator->sort('fase_id', 'Pertenece a la Fase'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($eventos as $evento): ?>
					<tr>
						<td><?php echo h($evento['Evento']['denominacion']); ?>&nbsp;</td>
						<td><?php echo $evento['Parent']['denominacion']; ?></td>
						<td><?php echo $evento['Fase']['denominacion']; ?></td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $evento['Evento']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $evento['Evento']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php if ($evento['Evento']['bloqueado']): ?>
									<button class="btn btn-danger btn-mini disabled">Eliminar</button>
								<?php else: ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $evento['Evento']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $evento['Evento']['id'])); ?>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid direccioncontroles index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
