<?php $this->Html->addCrumb('Acciones', '/eventos'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Evento']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid eventos form">
	<?php echo $this->Form->create('Evento'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Acción'); ?></legend>
			
			<div class="row-fluid">
				<div class="span8 control-group">
					<?php echo $this->Form->input('denominacion', array('type' => 'text', 'class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('parent_id', array('class' => 'span12', 'label' => 'Depende de la Acción', 'empty' => 'Ninguna')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('descripcion', array('class' => 'span12 editor', 'label' => 'Descripción')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php echo $this->Form->input('fase_id', array('class' => 'span12', 'label' => 'Fase a la que pertenece')); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('posicion', array('class' => 'span12', 'label' => 'Posición')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->label('es_opcional', 'Activar si es opcional'); ?>
					<?php echo $this->Form->input('es_opcional', array('class' => '', 'label' => array('text' => 'Es opcional', 'class' => 'normal'))); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->label('es_inspeccion', 'Activar si es una inspección'); ?>
					<?php echo $this->Form->input('es_inspeccion', array('class' => '', 'label' => array('text' => 'Es inspección', 'class' => 'normal'))); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('Anexo', array('class' => 'span12', 'label' => 'Anexos Asociados', 'size' => '10')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('Actuacionesfiscale', array('class' => 'span12', 'empty' => 'Ninguna', 'label' => 'Actuaciones Fiscales donde esta acción se ejecuta', 'size' => '10')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
