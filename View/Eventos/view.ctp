<?php $this->Html->addCrumb('Acciones', '/eventos'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $evento['Evento']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $evento['Evento']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid eventos view">
	<div class="span8">
		<h3><?php echo __('Anexos'); ?></h3>
		<?php if (!empty($evento['Anexo'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominación'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($evento['Anexo'] as $anexo): ?>
						<tr>
							<td><?php echo $anexo['denominacion']; ?></td>
							<td><?php echo $anexo['Fase']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'anexos', 'action' => 'view', $anexo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'anexos', 'action' => 'edit', $anexo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'anexos', 'action' => 'delete', $anexo['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $anexo['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay anexos asociados a la acción</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4>Datos de la Acción</h4>
		<dl class="dl-horizontal">
			<dt>Denominación</dt>
			<dd><?php echo $evento['Evento']['denominacion']; ?>&nbsp;</dd>
			<dt>Depende de la Acción</dt>
			<dd><?php echo ($evento['Parent']['denominacion']) ? $evento['Parent']['denominacion'] : 'Ninguna'; ?>&nbsp;</dd>
			<dt>Pertenece a la Fase</dt>
			<dd><?php echo $evento['Fase']['denominacion']; ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actuaciones Fiscales Asociadas a la Acción'); ?></h3>
		<?php if (!empty($evento['Actuacionesfiscale'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Objetivo'); ?></th>
						<th class="span2"><?php echo __('Inicio Estimado'); ?></th>
						<th class="span2"><?php echo __('Fin Estimado'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th><?php echo __('Objeto de Evaluación'); ?></th>
						<th class="span1 actions"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($evento['Actuacionesfiscale'] as $actuacionesfiscale): ?>
						<tr>
							<td><?php echo $actuacionesfiscale['numero']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_general']; ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_inicio_estimada']); ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_fin_estimada']); ?></td>
							<td><?php echo $actuacionesfiscale['Fase']['denominacion']; ?></td>
							<td><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Detalles'), array('controller' => 'actuacionesfiscales', 'action' => 'details', $actuacionesfiscale['id']), array('class' => 'btn btn-info btn-mini')); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="text-warning">No hay actuaciones fiscales asociadas a la presente acción</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Eventualidades'); ?></h3>
		<?php if (!empty($evento['Eventualidade'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Detecta'); ?></th>
						<th><?php echo __('Fecha Detección'); ?></th>
						<th><?php echo __('Corrige/Revisa'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Estado'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($evento['Eventualidade'] as $eventualidade): ?>
						<tr>
							<td><?php echo $eventualidade['Eventualidadtipo']['denominacion']; ?></td>
							<td>#<?php echo $eventualidade['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_deteccion']; ?></td>
							<td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_verificacion']; ?></td>
							<td><?php echo $eventualidade['numero']; ?></td>
							<td><?php echo $eventualidade['asunto']; ?></td>
							<td><?php echo $eventualidadestados[$eventualidade['estado']]; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventualidades', 'action' => 'view', $eventualidade['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventualidades', 'action' => 'edit', $eventualidade['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $eventualidade['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $eventualidade['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay eventualidades asociadas a la acción</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actividades'); ?></h3>
		<?php if (!empty($evento['Operacione'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Remite'); ?></th>
						<th><?php echo __('Fecha Remite'); ?></th>
						<th><?php echo __('Recibe'); ?></th>
						<th><?php echo __('Fecha Recibe'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($evento['Operacione'] as $operacione): ?>
						<tr>
							<td><?php echo isset($operacione['Eventualidadtipo']['denominacion']) ? $operacione['Eventualidadtipo']['denominacion'] : 'Regular'; ?></td>
							<td>#<?php echo $operacione['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $operacione['Fremite']['nombre']; ?></td>
							<td><?php echo $operacione['Frecibe']['nombre']; ?></td>
							<td><?php echo $operacione['fecha_remite']; ?></td>
							<td><?php echo $operacione['fecha_recibe']; ?></td>
							<td><?php echo $operacione['observaciones']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'operaciones', 'action' => 'view', $operacione['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'operaciones', 'action' => 'edit', $operacione['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'operaciones', 'action' => 'delete', $operacione['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $operacione['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay actividades asociadas a la acción</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Acciones Dependientes'); ?></h3>
		<?php if (!empty($evento['Children'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominación'); ?></th>
						<th><?php echo __('Descripción'); ?></th>
						<th><?php echo __('Es Opcional'); ?></th>
						<th><?php echo __('Es Inspección'); ?></th>
						<th><?php echo __('Anexo'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($evento['Children'] as $children): ?>
						<tr>
							<td><?php echo $children['denominacion']; ?></td>
							<td><?php echo $children['descripcion']; ?></td>
							<td><?php echo ($children['es_opcional']) ? 'Sí' : 'No'; ?></td>
							<td><?php echo ($children['es_inspeccion']) ? 'Sí' : 'No'; ?></td>
							<td>
								<ul>
									<?php foreach ($children['Anexo'] as $anexo): ?>
										<li><?php echo $anexo['denominacion']; ?></li>
									<?php endforeach; ?>
								</ul>
							</td>
							<td><?php echo $children['Fase']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventos', 'action' => 'view', $children['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventos', 'action' => 'edit', $children['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventos', 'action' => 'delete', $children['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $children['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No existen otras acciones que dependan de la presente acción</p>
		<?php endif; ?>
	</div>
</div>
