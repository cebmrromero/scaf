<?php $this->Html->addCrumb('Fases', '/fases'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['Fase']['id'])); ?></li>
	</ul>
</div>
<div class="row-fluid fases form">
	<?php echo $this->Form->create('Fase'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Fase'); ?></legend>
			
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('inicioauto', array('class' => 'span12', 'label' => 'Inicio', 'options' => $inicios)); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('has_formulario', array('class' => 'span12', 'label' => 'Requiere formulario', 'options' => array('0' => 'No', '1' => 'Sí'))); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Form->input('formulario', array('class' => 'span12', 'label' => 'Indicar el formulario')); ?>
				</div>
				<div class="span2 control-group">
					<?php echo $this->Form->input('evento_id', array('class' => 'span12', 'label' => 'Evento donde se mostrara el formulario')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>