<?php $this->Html->addCrumb('Fases', '/fases'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $fase['Fase']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $fase['Fase']['id'])); ?></li>
	</ul>
</div>
<div class="row-fluid eventos view">
	<div class="span8">
		<h3><?php echo __('Actuaciones Fiscales Asociadas a la Fase'); ?></h3>
		<?php if (!empty($fase['Actuacionesfiscale'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Objetivo'); ?></th>
						<th class="span2"><?php echo __('Inicio Estimado'); ?></th>
						<th class="span2"><?php echo __('Fin Estimado'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th><?php echo __('Objeto de Evaluación'); ?></th>
						<th class="span1 actions"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($fase['Actuacionesfiscale'] as $actuacionesfiscale): ?>
						<tr>
							<td>#<?php echo $actuacionesfiscale['numero']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_general']; ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_inicio_estimada']); ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_fin_estimada']); ?></td>
							<td><?php echo $actuacionesfiscale['Fase']['denominacion']; ?></td>
							<td><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Detalles'), array('controller' => 'actuacionesfiscales', 'action' => 'details', $actuacionesfiscale['id']), array('class' => 'btn btn-info btn-mini')); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="text-warning">No hay actuaciones fiscales asociadas a la presente fase</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4>Datos de la Fase</h4>
		<dl class="dl-horizontal">
			<dt>Denominación</dt>
			<dd><?php echo $fase['Fase']['denominacion']; ?>&nbsp;</dd>
			<dd><?php echo ($fase['Fase']['inicioauto']) ? 'Sí' : 'No'; ?>&nbsp;</dd>
			<?php if ($fase['Fase']['inicioauto']): ?>
				<dd><?php echo $fase['Fase']['formulario']; ?>&nbsp;</dd>
			<?php endif; ?>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Acciones'); ?></h3>
		<?php if (!empty($fase['Evento'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominación'); ?></th>
						<th><?php echo __('Es Opcional'); ?></th>
						<th><?php echo __('Es Inspeccion'); ?></th>
						<th><?php echo __('Anexo'); ?></th>
						<th><?php echo __('Depende de'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($fase['Evento'] as $evento): ?>
						<tr>
							<td><?php echo $evento['denominacion']; ?></td>
							<td><?php echo ($evento['es_opcional']) ? 'Sí' : 'No'; ?></td>
							<td><?php echo ($evento['es_inspeccion']) ? 'Sí' : 'No'; ?></td>
							<td><?php echo (isset($evento['Anexo']['denominacion'])) ? $evento['Anexo']['denominacion']: ''; ?></td>
							<td><?php echo (isset($evento['Parent']['denominacion'])) ? $evento['Parent']['denominacion']: '-'; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventos', 'action' => 'view', $evento['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventos', 'action' => 'edit', $evento['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventos', 'action' => 'delete', $evento['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $evento['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay acciones asociadas a la fase</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Prorrogas'); ?></h3>
		<?php if (!empty($fase['Prorroga'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Solicita'); ?></th>
						<th><?php echo __('Fecha Solicitud'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Está Aprobada'); ?></th>
						<th><?php echo __('Número de Días'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Observaciones'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($fase['Prorroga'] as $prorroga): ?>
						<tr>
							<td>#<?php echo $prorroga['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $prorroga['Solicita']['nombre']; ?></td>
							<td><?php echo $prorroga['fecha_solicitud']; ?></td>
							<td><?php echo $prorroga['fecha_verificacion']; ?></td>
							<td><?php echo ($prorroga['es_aprobada']) ? 'Sí': 'No'; ?></td>
							<td><?php echo $prorroga['ndias']; ?> Días</td>
							<td><?php echo $prorroga['asunto']; ?></td>
							<td><?php echo $prorroga['observaciones']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'prorrogas', 'action' => 'view', $prorroga['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'prorrogas', 'action' => 'edit', $prorroga['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'prorrogas', 'action' => 'delete', $prorroga['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $prorroga['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay prorrogas asociadas a la fase</p>
		<?php endif; ?>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Anexos'); ?></h3>
		<?php if (!empty($fase['Anexo'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominacion'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($fase['Anexo'] as $anexo): ?>
						<tr>
							<td><?php echo $anexo['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'anexos', 'action' => 'view', $anexo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'anexos', 'action' => 'edit', $anexo['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'anexos', 'action' => 'delete', $anexo['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $anexo['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay anexos asociadas a la fase</p>
		<?php endif; ?>
	</div>
</div>
