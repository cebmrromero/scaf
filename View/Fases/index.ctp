<?php $this->Html->addCrumb('Fases', '/fases'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
	</ul>
</div>
<h3>Fases</h3>
<div class="row-fluid fases index">
	<div class="span12">
		<p>A continuación el listado de fases ejecutadas por actuación fiscal:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('denominacion', 'Denominación'); ?></th>
					<th><?php echo $this->Paginator->sort('inicioauto', 'Inicio'); ?></th>
					<th><?php echo $this->Paginator->sort('has_formulario', 'Requiere formulario'); ?></th>
					<th><?php echo $this->Paginator->sort('formulario', 'Formulario'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($fases as $fase): ?>
					<tr>
						<td><?php echo h($fase['Fase']['denominacion']); ?>&nbsp;</td>
						<td><?php echo h($inicios[$fase['Fase']['inicioauto']]); ?>&nbsp;</td>
						<td><?php echo ($fase['Fase']['has_formulario']) ? 'Sí' : 'No'; ?>&nbsp;</td>
						<td><?php echo ($fase['Fase']['formulario']) ? $fase['Fase']['formulario'] : '-'; ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $fase['Fase']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $fase['Fase']['id']), array('class' => 'btn btn-info btn-mini')); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid fases index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
