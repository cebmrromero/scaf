<?php if ($actuacionesfiscale_id): ?>
	<?php $this->Html->addCrumb('Actuacion Fiscal', '/actuacionesfiscales/details/'.$actuacionesfiscale_id); ?>
	<?php $this->Html->addCrumb('Nueva Actividad'); ?>
<?php else: ?>
	<?php $this->Html->addCrumb('Actividades', '/operaciones'); ?>
	<?php $this->Html->addCrumb('Nueva'); ?>
<?php endif; ?>
<?php $formulario_completado = false; ?>
<?php $ocultar = ''; ?>
<div class="operaciones form">
	<h3>Nueva Actividad</h3>
	<?php foreach (($actuacionesfiscale['ActuacionesfiscalesEvento']) as $actuacionesfiscalesevento): ?>
		<?php if (($evento_id == $actuacionesfiscalesevento['evento_id']) && !empty($actuacionesfiscalesevento['fecha_inicio'])): ?>
			<?php $formulario_completado = true; ?>
		<?php endif; ?>
	<?php endforeach; ?>
	<?php if ($evento_id && isset($evento['Fase'][0]['evento_id']) && ($evento['Fase'][0]['evento_id'] == $evento_id) && !$formulario_completado): ?>
		<?php $ocultar = ' hidden'; ?>
		<?php echo $this->element($evento['Fase'][0]['formulario']); ?>
	<?php endif; ?>
	<?php echo $this->Form->create('Operacione', array("class" => (($tiene_anexos)) ? "check-anexos$ocultar" : "$ocultar")); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Actividad'); ?></legend>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php if ($actuacionesfiscale_id): ?>
						<label>Actuación Fiscal</label>
						<span><?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']?></span>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'value' => $actuacionesfiscale_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('class' => 'span12', 'label' => 'Actuación Fiscal')); ?>
					<?php endif; ?>
				</div>
				<div class="span4 control-group">
					<?php if ($evento_id): ?>
						<label>Acción</label>
						<span><?php echo $evento['Evento']['denominacion']?></span>
						<?php echo $this->Form->input('evento_id', array('type' => 'hidden', 'value' => $evento_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('evento_id', array('class' => 'span12', 'label' => 'Evento')); ?>
					<?php endif; ?>
				</div>
				<div class="span4 control-group">
					<div class="input text required ">
						<?php echo $this->Form->label('fecha_remite', 'Fecha'); ?>
						<div class="input-prepend span10">
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo $this->Form->input('fecha_remite', array('type' => 'text', 'readonly' => 'readonly', 'class' => 'span12 text-center', 'label' => false, 'div' => false, 'value' => date('d-m-Y h:i A'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<label>De</label>
					<?php echo $this->Form->input('fremite_id', array('type' => 'hidden', 'value' => $auth_user['funcionario_id'])); ?>
					<span><?php echo $auth_user['Funcionario']['nombre']; ?></span>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('frecibe_id', array('class' => 'span12', 'label' => 'Para', 'required' => 'required', 'empty' => 'Seleccionar')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('eventualidadtipo_id', array('class' => 'span12', 'label' => 'Tipo de actividad', 'required' => false)); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('observaciones', array('class' => 'span12', 'label' => 'Breve descripción de la actividad', 'rows' => '2')); ?>
					<?php echo $this->Form->input('es_recibido', array('type' => 'hidden', 'value' => 0)); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
		<?php if ($tiene_anexos): ?>
			<div class="modal hide fade" id="anexos-adjuntos">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Remitir Anexos</h3>
				</div>
				<div class="modal-body">
					<fieldset>
						<p>A continuación debe indicar ¿Cuáles anexos esta remitiendo?:</p>
						<?php $i = 0; ?>
						<?php foreach ($evento['Anexo'] as $anexo): ?>
							<div class="row-fluid">
								<div class="span12 control-group">
									<?php echo $this->Form->input("AnexosOperacione.$i.anexo_id", array('type' => 'hidden', 'value' => $anexo['id'])); ?>
									<?php echo $this->Form->input("AnexosOperacione.$i.remitido", array('value' => '1', 'label' => $anexo['denominacion'])); ?>
								</div>
							</div>
							<?php $i++; ?>
						<?php endforeach; ?>
					</fieldset>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn close-anexos-modal">Cerrar</a>
					<a href="#" class="btn btn-primary continue-anexos-modal">Continuar</a>
				</div>
			</div>
		<?php endif; ?>
	<?php echo $this->Form->end(); ?>
</div>