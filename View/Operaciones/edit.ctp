<?php if ($actuacionesfiscale_id): ?>
	<?php $this->Html->addCrumb('Actuacion Fiscal', '/actuacionesfiscales/details/'.$actuacionesfiscale_id); ?>
	<?php $this->Html->addCrumb('Editar Actividad'); ?>
<?php else: ?>
	<?php $this->Html->addCrumb('Actividades', '/operaciones'); ?>
	<?php $this->Html->addCrumb('Editar'); ?>
<?php endif; ?>
<div class="operaciones form">
	<h3>Editar Actividad</h3>
	<?php echo $this->Form->create('Operacione'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Actividad'); ?></legend>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php if ($actuacionesfiscale_id): ?>
						<label>Actuación Fiscal</label>
						<span><?php echo $actuacionesfiscale['Actuacionesfiscale']['numero']?></span>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'value' => $actuacionesfiscale_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('actuacionesfiscale_id', array('class' => 'span12', 'label' => 'Actuación Fiscal')); ?>
					<?php endif; ?>
				</div>
				<div class="span4 control-group">
					<?php if ($evento_id): ?>
						<label>Acción</label>
						<span><?php echo $evento['Evento']['denominacion']?></span>
						<?php echo $this->Form->input('evento_id', array('type' => 'hidden', 'value' => $evento_id)); ?>
					<?php else: ?>
						<?php echo $this->Form->input('evento_id', array('class' => 'span12', 'label' => 'Evento')); ?>
					<?php endif; ?>
				</div>
				<div class="span4 control-group">
					<div class="input text required ">
						<?php echo $this->Form->label('fecha_remite', 'Fecha'); ?>
						<div class="input-prepend span10">
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo $this->Form->input('fecha_remite', array('type' => 'text', 'readonly' => 'readonly', 'class' => 'span12 text-center', 'label' => false, 'div' => false)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<label>De</label>
					<?php echo $this->Form->input('fremite_id', array('type' => 'hidden', 'value' => $this->request->data['Operacione']['fremite_id'])); ?>
					<span><?php echo $this->request->data['Fremite']['nombre']; ?></span>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('frecibe_id', array('class' => 'span12', 'label' => 'Para', 'required' => 'required', 'empty' => 'Seleccionar')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('eventualidadtipo_id', array('class' => 'span12', 'label' => 'Tipo de actividad', 'required' => false)); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('observaciones', array('class' => 'span12', 'label' => 'Breve descripcion de la operacion', 'rows' => '2')); ?>
					<?php echo $this->Form->input('es_recibido', array('type' => 'hidden', 'value' => 0)); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>