<!-- TODO: Esto ira en otro element -->
<h3><?php echo $evento['denominacion']; ?></h3>
<h4>Descripción de la Acción</h4>
<p><?php echo $evento['descripcion']; ?></p>
<div class="row-fluid detalle-evento">
    <div class="span12 control-group">
        <div class="btn-group" data-toggle="buttons-radio">
            <?php foreach($data['Evento'] as $mevento): ?>
                <?php if ($mevento['id'] == $evento['parent_id']): ?>
                    <?php if ($mevento['ActuacionesfiscalesEvento']['es_completado']): ?>
                        <?php $parent_completado = true; ?>
                    <?php else: ?>
                        <?php $parent_completado = false; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ((!empty($evento['parent_id']) && $parent_completado) || empty($evento['parent_id'])): ?>
                <?php if ($evento['ActuacionesfiscalesEvento']['es_completado']): ?>
                    <span class="btn btn-success disabled">Completado</span>
                <?php elseif (isset($evento['Operacione']) && !empty($evento['Operacione'])): ?>
                    <?php if ($evento['Operacione'][sizeof($evento['Operacione']) - 1]['fremite_id'] == $auth_user['funcionario_id']): ?>
                        <?php if (!$evento['Operacione'][sizeof($evento['Operacione']) - 1]['es_recibido']): ?>
                            <span class="btn btn-warning disabled">En espera</span>
                        <?php else: ?>
                            <span class="btn btn-warning disabled">Remitida</span>
                        <?php endif; ?>
                    <?php elseif (!$evento['Operacione'][sizeof($evento['Operacione']) - 1]['es_recibido']): ?>
                        <span class="btn btn-warning disabled">No recibida</span>
                    <?php elseif ($evento['Operacione'][sizeof($evento['Operacione']) - 1]['frecibe_id'] == $auth_user['funcionario_id']): ?>
                        <?php echo $this->Html->link('Remitir', array('controller' => 'operaciones', 'action' => 'add', $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn btn-info')); ?>
                    <?php endif; ?>
                    <?php echo $this->Html->link('Eventualidad', array('controller' => 'eventualidades', 'action' => 'add', $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn')); ?>
                    <!--Inicio / Esto es para evitar que finalicen la accion antes de tiempo-->
                    <?php $can_finalizar = true; ?>
                    <?php foreach (array_reverse($evento['Operacione']) as $operacione): ?>
                        <?php if (!$operacione['es_recibido']): ?>
                            <?php $can_finalizar = false; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if ($can_finalizar): ?>
                        <?php echo $this->Html->link('Finalizar la acción', array('controller' => 'actuacionesfiscales_eventos', 'action' => 'finalizar', $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn btn-danger'), __('¿Está seguro(a) que desea finalizar la acción?')); ?>
                    <?php else: ?>
                        <span class="btn btn-danger disabled" title="Debe recibir todas las acciones">Finalizar la acción</span>
                    <?php endif; ?>
                    <!--Fin-->
                <?php else: ?>
                    <?php //if ($this->request->data['Fase']['has_formulario']): ?>
                        <?php //list($controller, $action) = explode('/', $this->request->data['Fase']['formulario']); ?>
                        <?php //echo $this->Html->link('Iniciar la acción', array('controller' => $controller, 'action' => $action, $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn btn-success')); ?>
                    <?php //else: ?>
                        <?php echo $this->Html->link('Iniciar la acción', array('controller' => 'operaciones', 'action' => 'add', $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn btn-success')); ?>
                        <?php echo $this->Html->link('Eventualidad', array('controller' => 'eventualidades', 'action' => 'add', $data['Actuacionesfiscale']['id'], $evento['id']), array('class' => 'btn')); ?>
                    <?php //endif; ?>
                <?php endif; ?>
            <?php else: ?>
                <span class="btn btn-danger disabled">Inicia al completar la anterior</span>
            <?php endif; ?>
        </div>
        <?php if ($prorroga && ($prorroga['Prorroga']['fase_id'] == $fase)): ?>
            <span class="btn btn-inverse disabled">Prórroga Solicitada</span>
        <?php elseif (!$prorroga && $data['Actuacionesfiscale']['fase_id'] <= $fase): ?>
            <?php echo $this->Html->link('Solicitar Prórroga', array('controller' => 'prorrogas', 'action' => 'add', $data['Actuacionesfiscale']['id'],  $fase), array('class' => 'btn btn-inverse')); ?>
        <?php endif; ?>
    </div>
</div>