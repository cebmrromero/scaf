<div class="alert<?php echo (isset($class) && !empty($class)) ? ' ' . $class : ''; ?>">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php if (isset($class) && !empty($class)): ?>
        <?php if ($class == 'alert-error'): ?>
            <strong>Error!</strong>
        <?php elseif ($class == 'alert-success'): ?>
            <strong>Éxito!</strong>
        <?php elseif ($class == 'alert-info'): ?>
            <strong>Información!</strong>
        <?php endif; ?>
    <?php else: ?>
        <strong>Alerta!</strong>
    <?php endif; ?>
    
    <?php echo $message; ?>
</div>