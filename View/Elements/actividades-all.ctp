<?php
$completados = 0;
$porcompletar = 0;
foreach($actuacionesfiscale['ActuacionesfiscalesEvento'] as $evento) {
    if ($evento['Evento']['fase_id'] == $fase) {
        $porcompletar++;
        if ($evento['es_completado']) {
            $completados++;
        }
    }
}
$porcentajeindividual = 100 / $porcompletar;
$porcentajecompletado = ($completados / $porcompletar) * 100;
?>
<div class="progress progress-striped">
    <?php foreach($actuacionesfiscale['ActuacionesfiscalesEvento'] as $evento): ?>
        <?php if ($evento['Evento']['fase_id'] == $fase): ?>
            <?php
                $popovercontent = '';
                if (!$evento['fecha_inicio'] && !$evento['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Acción no iniciada</p>";
                } elseif ($evento['fecha_inicio'] && !$evento['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y h:i A', $evento['fecha_inicio']) . ' al (en proceso)</p>';
                } else if ($evento['fecha_inicio'] && $evento['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y h:i A', $evento['fecha_inicio']) . " al " . $this->Time->format('d-m-Y h:i A', $evento['fecha_fin']) . '</p>';
                }
                if ($evento['es_activo'] && !$evento['es_completado']) {
                    $class = ' bar-warning';
                } elseif ($evento['es_completado']) {
                    $class = ' bar-success';
                } else {
                    $class = ' bar-danger';
                }
            ?>
            <div class="bar<?php echo $class; ?>" style="width: <?php echo $porcentajeindividual; ?>%;" title="<?php echo $evento['Evento']['denominacion']; ?>" data-content="<?php echo $popovercontent; ?>"></div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
<div class="accordion-detail" id="accordion<?php echo $fase . $collapse_index; ?>">
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?php echo $fase . $collapse_index; ?>" href="#collapse-op-<?php echo $fase . $collapse_index; ?>">
                Histórico de Actividades
            </a>
        </div>
        <div id="collapse-op-<?php echo $fase . $collapse_index; ?>" class="accordion-body collapse out">
            <div class="accordion-inner">
                <div class="h-operaciones">
                    <table class="table table-hover table-striped table-condensed mini">
                        <thead>
                            <tr>
                                <th>De</th>
                                <th>Para</th>
                                <th>Fecha</th>
                                <th>Observaciones</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($actuacionesfiscale['Operacione']): ?>
                                <?php foreach($actuacionesfiscale['Operacione'] as $operacione): ?>
                                    <?php if ($operacione['Evento']['fase_id'] == $fase): ?>
                                        <tr>
                                            <td><?php echo $operacione['Fremite']['nombre']; ?></td>
                                            <td><?php echo $operacione['Frecibe']['nombre']; ?></td>
                                            <td><?php echo $this->Time->format('d-m-Y h:i A', $operacione['fecha_remite']); ?></td>
                                            <td><?php echo $operacione['observaciones']; ?></td>
                                            <td><?php echo (isset($operacione['Eventualidadtipo']['denominacion'])) ? $operacione['Eventualidadtipo']['denominacion'] : 'Regular'; ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="5">No hay registro de actividades</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?php echo $fase . $collapse_index; ?>" href="#collapse-ev-<?php echo $fase . $collapse_index; ?>">
                Histórico de Eventualidades
            </a>
        </div>
        <div id="collapse-ev-<?php echo $fase . $collapse_index; ?>" class="accordion-body collapse out">
            <div class="accordion-inner">
                <div class="h-eventualidades">
                    <table class="table table-hover table-striped table-condensed mini">
                        <thead>
                            <tr>
                                <th>De</th>
                                <th>Para</th>
                                <th>Fecha</th>
                                <th>Asunto</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($actuacionesfiscale['Eventualidade']): ?>
                                <?php foreach($actuacionesfiscale['Eventualidade'] as $eventualidade): ?>
                                    <?php if ($eventualidade['Evento']['fase_id'] == $fase): ?>
                                        <tr>
                                            <td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
                                            <td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
                                            <td><?php echo $this->Time->format('d-m-Y h:i A', $eventualidade['fecha_deteccion']); ?></td>
                                            <td><?php echo $eventualidade['asunto']; ?></td>
                                            <td><?php echo (isset($eventualidade['Eventualidadtipo']['denominacion'])) ? $eventualidade['Eventualidadtipo']['denominacion'] : 'Regular'; ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="5">No hay registro de eventualidades</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
