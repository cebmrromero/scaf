<?php
//$dh_ejecutados = ${"dh_ejecutados_f$fase"};
//$fechas_limites = ${"fechas_limites_f$fase"};
$dh_disponibles = 0;
if ($fase == 1) {
    $dh_disponibles = $actuacionesfiscale['Actuacionesfiscale']['dh_planificacion'];
}
if ($fase == 2) {
    $dh_disponibles = $actuacionesfiscale['Actuacionesfiscale']['dh_ejecucion'];
}
if ($fase == 3) {
    $dh_disponibles = $actuacionesfiscale['Actuacionesfiscale']['dh_resultados'];
}
//$porcentajecompletado = ($dh_ejecutados / $dh_disponibles) * 100;
//if ($evento['ActuacionesfiscalesEvento']['fecha_fin']) {
//    $porcentajecompletado = 100;
//}
//if ($porcentajecompletado <= 33) {
//    $class = ' bar-danger';
//} else if ($porcentajecompletado <= 66 && $porcentajecompletado > 33) {
//    $class = ' bar-warning';
//} else {
//    $class = ' bar-success';
//}


$completados = 0;
$porcompletar = 0;

foreach($actuacionesfiscale['Evento'] as $evento_aux) {
    //print_r($evento_aux);
    if ($evento_aux['fase_id'] == $fase) {
        $porcompletar++;
        if ($evento_aux['ActuacionesfiscalesEvento']['es_completado']) {
            $completados++;
        }
    }
}
if ($details[$fase]['prorrogas']) {
    foreach ($details[$fase]['prorrogas'] as $prorrogas) {
        $porcompletar++;
    }
}
$porcentajeindividual = 100 / $porcompletar;
$porcentajecompletado = ($completados / $porcompletar) * 100;
?>
<h5>Progreso General de la Fase <?php echo $title_tab; ?> - Ver Detalles</h5>
<div class="progress progress-striped top-fase">
    <?php foreach($actuacionesfiscale['Evento'] as $evento_aux): ?>
        <?php if ($evento_aux['fase_id'] == $fase): ?>
            <?php
                $popovercontent = '';
                if (!$evento_aux['ActuacionesfiscalesEvento']['fecha_inicio'] && !$evento_aux['ActuacionesfiscalesEvento']['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Acción no iniciada</p>";
                    $class = ' bar-danger';
                } elseif ($evento_aux['ActuacionesfiscalesEvento']['fecha_inicio'] && !$evento_aux['ActuacionesfiscalesEvento']['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y', $evento_aux['ActuacionesfiscalesEvento']['fecha_inicio']) . ' al (en proceso)</p>';
                    $class = ' bar-warning';
                } else if ($evento_aux['ActuacionesfiscalesEvento']['fecha_inicio'] && $evento_aux['ActuacionesfiscalesEvento']['fecha_fin']) {
                    $popovercontent = "<p class='text-center'>Del " . $this->Time->format('d-m-Y', $evento_aux['ActuacionesfiscalesEvento']['fecha_inicio']) . " al " . $this->Time->format('d-m-Y', $evento_aux['ActuacionesfiscalesEvento']['fecha_fin']) . '</p>';
                    $class = ' bar-success';
                }
                //if ($evento_aux['ActuacionesfiscalesEvento']['es_activo'] && !$evento_aux['ActuacionesfiscalesEvento']['es_completado']) {
                //    $class = ' bar-warning';
                //} elseif ($evento_aux['ActuacionesfiscalesEvento']['es_completado']) {
                //    $class = ' bar-success';
                //} else {
                //    $class = ' bar-danger';
                //}
            ?>
            <div class="bar<?php echo $class; ?>" style="width: <?php echo $porcentajeindividual; ?>%;" title="<?php echo $evento_aux['denominacion']; ?>" data-content="<?php echo $popovercontent; ?>"></div>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if ($details[$fase]['prorrogas']): ?>
        <?php foreach($details[$fase]['prorrogas'] as $prorroga): ?>
            <div class="bar" style="width: <?php echo $porcentajeindividual; ?>%;" title="Prorroga" data-content="<?php echo $prorroga['ndias']; echo ($prorroga['ndias'] > 1) ? ' Días hábiles' : ' Día hábil';?> "></div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<div class="row-fluid">
    <div class="span2">
        <dl>
            <dt>Fecha de inicio estimada de la fase</dt>
            <dd><?php echo $this->Time->format('d-m-Y h:i A', $details[$fase]['fechas_limites']['fecha_inicio']); ?></dd>
        </dl>
    </div>
    <div class="span2">
        <dl>
            <dt>Fecha de fin estimada de la Fase</dt>
            <dd><?php echo $this->Time->format('d-m-Y h:i A', $details[$fase]['fechas_limites']['fecha_fin']); ?></dd>
        </dl>
    </div>
    <?php if ($evento['ActuacionesfiscalesEvento']['fecha_inicio']): ?>
        <div class="span2">
            <dl>
                <dt>Fecha de inicio de la Acción</dt>
                <dd><?php echo $this->Time->format('d-m-Y h:i A', $evento['ActuacionesfiscalesEvento']['fecha_inicio']); ?></dd>
            </dl>
        </div>
    <?php endif; ?>
    <?php if ($evento['ActuacionesfiscalesEvento']['fecha_fin']): ?>
        <div class="span2">
            <dl>
                <dt>Fecha de fin de la Acción</dt>
                <dd><?php echo $this->Time->format('d-m-Y h:i A', $evento['ActuacionesfiscalesEvento']['fecha_fin']); ?></dd>
            </dl>
        </div>
    <?php endif; ?>
    <div class="span2">
        <dl>
            <dt>Días hábiles para la ejecución de la fase</dt>
            <dd><?php echo $dh_disponibles; ?> Días</dd>
        </dl>
    </div>
    <div class="span2">
        <dl>
            <dt>Días ejecutados de la Actuación Fiscal</dt>
			<dd><?php echo $actuacionesfiscale['Actuacionesfiscale']['total_ejecutados']; ?> Días</dd>
        </dl>
    </div>
    <?php if ($details[$fase]['fechas_limites']['atrazo'] && empty($evento['ActuacionesfiscalesEvento']['fecha_fin'])): ?>
        <div class="span2 text-error">
            <dl>
                <dt>La fase tiene un retrazo de</dt>
                <dd><?php echo $details[$fase]['fechas_limites']['atrazo']; ?> Días</dd>
            </dl>
        </div>
    <?php elseif (!$evento['ActuacionesfiscalesEvento']['fecha_fin'] && $details[$fase]['fechas_limites']['diff']): ?>
        <div class="span2 text-error">
            <dl>
                <dt>Días hábiles para culminar la fase</dt>
                <dd><?php echo $details[$fase]['fechas_limites']['diff']; ?> Días</dd>
            </dl>
        </div>
    <?php endif; ?>
</div>
