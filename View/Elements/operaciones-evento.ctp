<?php if (isset($evento['Operacione']) && !empty($evento['Operacione'])): ?>
    <h4 id="operaciones<?php echo $evento['id']; ?>">Actividades sobre la Acción</h4>
    <div class="row-fluid" id="operaciones-evento">
        <?php echo $this->Html->Link("Actualizar", array('controller' => 'json', 'action' => 'getOperacionesEvento', $actuacionesfiscale_id, $evento['id']), array("class" => 'update-operaciones hidden')); ?>
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
					<th>Fecha Remisión</th>
					<th>Funcionario Remite</th>
					<th>Tiempo de Respuesta</th>
					<th>Funcionario Recibe</th>
					<th>Fecha Recepción</th>
					<th>Tiempo de Recepción</th>
                    <th class="span1">¿Recibida?</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (array_reverse($evento['Operacione']) as $operacione): ?>
                    <tr<?php echo (!$operacione['es_recibido'] && ($operacione['frecibe_id'] == $auth_user['funcionario_id'])) ? ' class="success"' : '';?>  title="Descripción" data-content="<?php echo (!empty($operacione['observaciones'])) ? $operacione['observaciones'] : 'Sin descripción'; ?>">
                        <td><?php echo ($operacione['fecha_remite']) ? $this->Time->format('d-m-Y h:i A', $operacione['fecha_remite']) : '-'; ?></td>
                        <td><?php echo $operacione['Fremite']['nombre']; ?></td>
                        <td><?php echo ($operacione['diff_remision']) ? $operacione['diff_remision'] . ' Días': '-'; ?></td>
                        <td><?php echo $operacione['Frecibe']['nombre']; ?></td>
                        <td><?php echo ($operacione['fecha_recibe']) ? $this->Time->format('d-m-Y h:i A', $operacione['fecha_recibe']) : '-'; ?></td>
                        <td><?php echo ($operacione['diff_recepcion']) ? $operacione['diff_recepcion'] . ' Días': '-'; ?></td>
                        <td>
                            <?php if ($operacione['es_recibido']): ?>
                                <span class="btn btn-success btn-mini disabled">Recibida</span>
                            <?php else: ?>
                                <?php if ($operacione['frecibe_id'] == $auth_user['funcionario_id']): ?>
                                    <?php echo $this->Html->link('<span class="btn btn-success btn-mini">Recibir</span>', array('controller' => 'operaciones', 'action' => 'marcar_recibida', $operacione['id']), array('class' => 'recibir-anexos', 'escape' => false)); ?>
                                    <?php if ($operacione['Anexo']): ?>
                                        <div class="modal hide fade" id="anexos-adjuntos-recibir">
                                            <?php echo $this->Form->create('AnexosOperacione', array('action' => 'recibidos')); ?>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h3>Recibir Anexos</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <fieldset>
                                                        <p>A continuación debe indicar ¿Cuáles anexos esta recibiendo?:</p>
                                                        <?php $i = 0; ?>
                                                        <?php foreach ($operacione['Anexo'] as $anexo): ?>
                                                            <div class="row-fluid">
                                                                <div class="span12 control-group">
                                                                    <?php echo $this->Form->input("AnexosOperacione.$i.id", array('type' => 'hidden', 'value' => $anexo['AnexosOperacione']['id'])); ?>
                                                                    <?php echo $this->Form->input("AnexosOperacione.$i.operacione_id", array('type' => 'hidden', 'value' => $anexo['AnexosOperacione']['operacione_id'])); ?>
                                                                    <?php echo $this->Form->input("AnexosOperacione.$i.anexo_id", array('type' => 'hidden', 'value' => $anexo['AnexosOperacione']['anexo_id'])); ?>
                                                                    <?php echo $this->Form->input("AnexosOperacione.$i.remitido", array('type' => 'hidden', 'value' => '1')); ?>
                                                                    <?php echo $this->Form->input("AnexosOperacione.$i.recibido", array('value' => '1', 'label' => $anexo['denominacion'], 'checked' => ($anexo['AnexosOperacione']['remitido']) ? 'checked': '')); ?>
                                                                </div>
                                                                <?php $i++; ?>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </fieldset>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn close-recibe-anexos-modal">Cerrar</button>
                                                    <button type="submit" class="btn btn-primary continue-recibe-anexos-modal">Continuar</button>
                                                </div>
                                            <?php echo $this->Form->end(); ?>
                                        </div>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <div class="control-group">
                                        <div class="btn-group">
                                            <?php if ($operacione['fremite_id'] == $auth_user['funcionario_id']): ?>
                                                <?php echo $this->Html->link('Editar', array('controller' => 'operaciones', 'action' => 'edit', $operacione['id']), array('class' => 'btn btn-info btn-mini')); ?>
                                                <?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'operaciones', 'action' => 'delete', $operacione['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?')); ?>
                                            <?php endif; ?>
                                            <span class="btn btn-warning btn-mini disabled">Remitida</span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
<?php echo $this->Html->scriptBlock('$(function () { $( "tr" ).popover({html:true, trigger: "hover", placement: "top"}); })'); ?>