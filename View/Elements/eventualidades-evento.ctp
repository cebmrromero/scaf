<?php if (isset($evento['Eventualidade']) && !empty($evento['Eventualidade'])): ?>
    <h4 id="eventualidades<?php echo $evento['id']; ?>">Eventualidades de la Acción</h4>
    <div class="row-fluid" id="eventualidades-evento">
        <?php echo $this->Html->Link("Actualizar", array('controller' => 'json', 'action' => 'getEventualidadesEvento', $actuacionesfiscale_id, $evento['id']), array("class" => 'update-eventualidades hidden')); ?>
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>Fecha Detección</th>
                    <th>Funcionario Detecta</th>
                    <th>Fecha Verificación</th>
                    <th>Funcionario Verifica</th>
                    <th>Tiempo de Respuesta</th>
                    <th>Tipo</th>
                    <th class="span1">Estado</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (array_reverse($evento['Eventualidade']) as $eventualidade): ?>
                    <tr<?php echo ($eventualidade['estado'] == 'NR' && ($eventualidade['corrige_id'] == $auth_user['funcionario_id'])) ? ' class="success"' : '';?> title="<?php echo $eventualidade['asunto']; ?>" data-content="<?php echo $eventualidade['observaciones'] ?><?php echo ($eventualidade['municipio']) ? "<br /><strong>Municipio:</strong> {$eventualidade['municipio']}" : '' ?><?php echo ($eventualidade['lugar']) ? "<br /><strong>Lugar:</strong> {$eventualidade['lugar']}" : '';  ?>">
                        <td><?php echo $this->Time->format('d-m-Y h:i A', $eventualidade['fecha_deteccion']); ?></td>
                        <td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
                        <td><?php echo ($eventualidade['fecha_verificacion']) ? $this->Time->format('d-m-Y h:i A', $eventualidade['fecha_verificacion']) : '-'; ?></td>
                        <td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
                        <td><?php echo ($eventualidade['diff_verificacion']) ? $eventualidade['diff_verificacion'] . ' Días' : '-'; ?></td>
                        <td><?php echo $eventualidade['Eventualidadtipo']['denominacion']; ?></td>
                        <td>
                            <?php if ($eventualidade['estado'] == 'R'): ?>
                                <span class="btn btn-info btn-mini disabled">Revisada</span>
                            <?php elseif ($eventualidade['estado'] == 'A'): ?>
                                <span class="btn btn-success btn-mini disabled">Aprobada</span>
                            <?php elseif ($eventualidade['estado'] == 'N'): ?>
                                <span class="btn btn-danger btn-mini disabled">Negada</span>
                            <?php else: ?>
                                <?php if ($eventualidade['corrige_id'] == $auth_user['funcionario_id']): ?>
                                    <div class="btn-group" data-toggle="buttons-radio">
                                        <?php echo $this->Html->link('Revisar', array('controller' => 'eventualidades', 'action' => 'marcar_revisada', $eventualidade['id']), array('class' => 'btn btn-info btn-mini')); ?>
                                        <?php echo $this->Html->link('Aprobar', array('controller' => 'eventualidades', 'action' => 'marcar_aprobada', $eventualidade['id']), array('class' => 'btn btn-success btn-mini')); ?>
                                        <?php echo $this->Html->link('Negar', array('controller' => 'eventualidades', 'action' => 'marcar_negada', $eventualidade['id']), array('class' => 'btn btn-danger btn-mini')); ?>
                                    </div>
                                <?php else: ?>
                                    <div class="control-group">
                                        <div class="btn-group">
                                            <?php if ($eventualidade['detecta_id'] == $auth_user['funcionario_id']): ?>
                                                <?php echo $this->Html->link('Editar', array('controller' => 'eventualidades', 'action' => 'edit', $eventualidade['id']), array('class' => 'btn btn-info btn-mini')); ?>
                                                <?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $eventualidade['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?')); ?>
                                            <?php endif; ?>
                                            <span class="btn btn-warning btn-mini disabled">Pendiente</span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
<?php echo $this->Html->scriptBlock('$(function () { $( "tr" ).popover({html:true, trigger: "hover", placement: "top"}); })'); ?>