<div class="span12">
    <div class="tabbable tabs-left">
        <ul class="nav nav-tabs span2">
            <li class="nav-header">Acciones de la Fase de <?php echo $title_tab; ?></li>
            <?php $i = 0; ?>
            <?php $todos_completados = true; ?>
            <?php foreach($this->request->data['Evento'] as $evento): ?>
                <?php if ($evento['fase_id'] == $fase): ?>
                    <?php if (!$evento['ActuacionesfiscalesEvento']['es_completado']): ?>
                        <?php $todos_completados = false; ?>
                    <?php endif; ?>
                    <li class="<?php echo ($evento['ActuacionesfiscalesEvento']['es_activo']) ? 'active': ''; ?>"><?php echo $this->Html->link(++$i . '. ' . $evento['denominacion'], '#eventtab_' . $evento['id'], array('data-toggle' => 'tab')); ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($todos_completados && $this->request->data['Actuacionesfiscale']['fase_id'] == $fase): ?>
                <li class="active"><?php echo $this->Html->link(++$i . '. Finalizar Fase de ' . $title_tab, '#endtab_' . $fase, array('data-toggle' => 'tab')); ?></li>
            <?php endif; ?>
        </ul>
        <div class="tab-content">
            <?php foreach($this->request->data['Evento'] as $evento): ?>
                <?php if ($evento['fase_id'] == $fase): ?>
                    <div class="tab-pane<?php echo ($evento['ActuacionesfiscalesEvento']['es_activo']) ? ' active': ''; ?>" id="<?php echo 'eventtab_' . $evento['id']; ?>">
                        <div class="row-fluid evento">
                            <?php echo $this->element('top-fase', array('actuacionesfiscale' => $this->request->data, 'evento' => $evento, 'fase' => $fase, 'title_tab' => $title_tab)); ?>
                            <?php echo $this->element('botonera-evento', array('data' => $this->request->data, 'evento' => $evento, 'fase' => $fase)); ?>
                            <?php echo $this->element('operaciones-evento', array('evento' => $evento, 'actuacionesfiscale_id' => $this->request->data['Actuacionesfiscale']['id'])); ?>
                            <?php echo $this->element('eventualidades-evento', array('evento' => $evento, 'actuacionesfiscale_id' => $this->request->data['Actuacionesfiscale']['id'])); ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php if ($todos_completados && $this->request->data['Actuacionesfiscale']['fase_id'] == $fase): ?>
                <div class="tab-pane active" id="<?php echo 'endtab_' . $fase; ?>">
                    <div class="row-fluid evento">
                        <?php if ($fase != 3): ?>
                            <?php echo $this->Html->link('Pasar a la siguiente fase', array('action' => 'cambiar_fase', $this->request->data['Actuacionesfiscale']['id'], ($fase < 3) ? $fase : 2), array('class' => 'btn btn-danger')) ;?>
                        <?php else: ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <span><strong>Éxito!</strong> Ha terminado todas las fases de la Actuación Fiscal</span>
                        </div>
                        <?php echo $this->Html->link('Terminar la actuación fiscal', array('action' => 'terminar', $this->request->data['Actuacionesfiscale']['id']), array('class' => 'btn btn-success')) ;?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>