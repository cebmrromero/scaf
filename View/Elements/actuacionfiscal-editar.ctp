<div class="actuacionesfiscales form">
	<h3>Editar Actuación Fiscal</h3>
	<?php echo $this->Form->create('Actuacionesfiscale', array('action' => 'edit')); ?>
		<?php echo $this->Form->input('id'); ?>
		<?php echo $this->Form->input('fase_id', array('type' => 'hidden')); ?>
		<fieldset>
			<legend><?php echo __('Datos de la Actuación'); ?></legend>
			<div class="row-fluid">
				<div class="span7 control-group">
					<?php echo $this->Form->input('direccioncontrole_id', array('class' => 'span12', 'label' => 'Dirección de Control')); ?>
				</div>
				<div class="span2 control-group">
					<div class="input number required">
						<?php echo $this->Form->label('numero', 'Número'); ?>
						<div class="input-prepend span10">
							<span class="add-on">#</span>
							<?php echo $this->Form->input('numero', array('class' => 'span12 mask-af', 'label' => false, 'div' => false)); ?>
						</div>
					</div>
				</div>
				<div class="span3 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('Ejerciciosfiscale', 'Ejercicio Económico Financiero'); ?>
						<?php echo $this->Form->input('Ejerciciosfiscale', array('class' => 'span12 multiselect', 'multiple' => 'multiple', 'empty' => false, 'label' => false)); ?>
					</div>
				</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<div class="hidden">
						<?php echo $this->Form->input('objeto_id'); ?>
					</div>
					<?php echo $this->Form->input('objeto_id', array('id' => 'objeto_id', 'autocomplete' => 'off', 'name' => 'objeto_id', 'type' => 'text', 'class' => 'span12', 'label' => 'Objeto de la Actuación')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('obj_general', array('class' => 'span12', 'label' => 'Objetivo General', 'rows' => '4')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('alcance', array('class' => 'span12', 'label' => 'Alcance', 'rows' => '4')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php echo $this->Form->input('objetivo_content', array('class' => 'span12', 'name' => 'objetivo_content', 'label' => 'Objetivos Específicos', 'rows' => '2')); ?>
				</div>
				<div class="span1 control-group">
					<p class="text-center">
						<label>&nbsp;</label>
						<a href="#" id="add-objetivo" class="btn btn-success btn-mini">+</a>
					</p>
				</div>
				<div class="span7 control-group" id="obj-container">
					<label>&nbsp;</label>
					<p class="text-success">Para agregar objetivos específicos, debe ingresar su contenido y presionar el botón (+).</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Form->input('actuaciontipo_id', array('class' => 'span12', 'label' => 'Tipo de Actuación')); ?>
				</div>
				<div class="span9 control-group">
					<?php echo $this->Form->input('actuacionesorigene_id', array('class' => 'span12', 'label' => 'Origen de la Actuación')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2 control-group">
					<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
						<label>Inicia</label>
						<span><?php echo $this->Time->format('d-m-Y h:i A', $this->request->data['Actuacionesfiscale']['fecha_inicio_estimada']); ?></span>
						<?php echo $this->Form->input('fecha_inicio_estimada', array('type' => 'hidden', 'class' => 'date-start')); ?>
					<?php else: ?>
						<?php echo $this->Form->input('fecha_inicio_estimada', array('type' => 'text', 'class' => 'span12 date-start', 'label' => 'Iniciará el')); ?>
					<?php endif; ?>
				</div>
				<div class="span2 control-group">
					<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
						<label>Finaliza</label>
						<span><?php echo $this->Time->format('d-m-Y h:i A', $this->request->data['Actuacionesfiscale']['fecha_fin_estimada']); ?></span>
						<?php echo $this->Form->input('fecha_fin_estimada', array('type' => 'hidden', 'class' => 'date-end')); ?>
					<?php else: ?>
						<?php echo $this->Form->input('fecha_fin_estimada', array('type' => 'text', 'required' => 'required', 'class' => 'span12 date-end', 'label' => 'Finalizará el')); ?>
					<?php endif; ?>
				</div>
				<div class="span2 control-group">
					<label>Hábiles Disponibles</label>
					<p class="text-right">
						<span id="dh-span">0</span>
						<span class="add-on">Días</span>
					</p>
					<?php echo $this->Form->input('dh-input', array('type' => 'hidden', 'value' => 0, 'id' => 'dh-input', 'name' => 'dh-input')); ?>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_planificacion', 'Tiempo Planificacion'); ?>
						<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
							<p class="text-right"><?php echo $this->request->data['Actuacionesfiscale']['dh_planificacion']; ?> Días</p>
						<?php else: ?>
							<div class="input-append span9">
								<?php echo $this->Form->input('dh_planificacion', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
								<span class="add-on">Días</span>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_ejecucion', 'Tiempo Ejecución'); ?>
						<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
							<p class="text-right"><?php echo $this->request->data['Actuacionesfiscale']['dh_ejecucion']; ?> Días</p>
						<?php else: ?>
							<div class="input-append span9">
								<?php echo $this->Form->input('dh_ejecucion', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
								<span class="add-on">Días</span>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_resultados', 'Tiempo Resultados'); ?>
						<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
							<p class="text-right"><?php echo $this->request->data['Actuacionesfiscale']['dh_resultados']; ?> Días</p>
						<?php else: ?>
							<div class="input-append span9">
								<?php echo $this->Form->input('dh_resultados', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
								<span class="add-on">Días</span>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="control-group">
				<?php echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $auth_user['id'])); ?>
			</div>
		</fieldset>
		<fieldset id="equipo-autidor">
			<legend><?php echo __('Equipo Auditor'); ?></legend>
            <div class="row-fluid">
				<div class="span4 control-group required">
					<label><strong>Rol</strong></label>
				</div>
				<div class="span3 control-group required">
					<label><strong>Funcionario</strong></label>
				</div>
				<div class="span2 control-group required">
					<label><strong>Nota de Designación</strong></label>
				</div>
				<div class="span3 control-group required">
					<label><strong>Fecha de Asignación</strong></label>
				</div>
            </div>
            <?php $i = 0; ?>
            <?php foreach ($this->request->data['Equipotrabajo'] as $funcionario):  ?>
                <div class="row-fluid">
                    <div class="span4 control-group">
                        <?php echo $this->Form->input('Equipotrabajo.'.$i.'.funcionariorole_id', array('type' => 'select', 'class' => 'span12 funcionariorole-changer', 'data-changer'=> '#Equipotrabajo'.$i.'FuncionarioId', 'required' => 'required', 'options' => $funcionarioroles_list, 'label' => false, 'alt' => $this->Html->url(array('controller' => 'json', 'action' => 'getFuncionariosByRol')))); ?>
                    </div>
                    <div class="span3 control-group">
                        <?php echo $this->Form->input('Equipotrabajo.'.$i.'.funcionario_id', array('type' => 'select', 'class' => 'span12 funcionario', 'required' => 'required', 'options' => $funcionarios, 'empty' => '-Seleccionar-', 'label' => false)); ?>
                    </div>
					<div class="span2 control-group">
						<div class="input text required">
							<div class="input-prepend span10">
								<span class="add-on">#</span>
								<?php echo $this->Form->input('Equipotrabajo.'.$i.'.nota_designacion', array('type' => 'text', 'class' => 'span12 mask-nd', 'required' => 'required', 'label' => false, 'div' => false)); ?>
								<?php echo $this->Form->input('Equipotrabajo.'.$i.'.credenciale_id', array('type' => 'hidden', 'class' => 'empty credencial', 'required' => 'required')); ?>
							</div>
						</div>
					</div>
					<div class="span2 control-group">
						<div class="input text required ">
							<div class="input-prepend span10">
								<span class="add-on"><i class="icon-calendar"></i></span>
								<?php echo $this->Form->input('Equipotrabajo.'.$i.'.fecha', array('type' => 'text', 'class' => 'span12 date text-center', 'required' => 'required', 'label' => false, 'div' => false)); ?>
								<?php echo $this->Form->input('Equipotrabajo.'.$i.'.id', array('type' => 'hidden', 'value' => $funcionario['id'])); ?>
							</div>
						</div>
					</div>
					<div class="span1 control-group">
						<?php echo $this->Html->link("X", array('controller' => 'equipotrabajos', 'action' => 'delete', $funcionario['id']), array("class" => "del-funcionario btn btn-danger btn-mini")); ?>
					</div>
                    <?php $i++; ?>
                </div>
            <?php endforeach; ?>
		</fieldset>
		<?php echo $this->Html->link("Agregar Funcionario", "#", array("id" => "add-funcionario", "class" => "btn btn-primary btn-mini")); ?>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
	<?php if ($this->request->data['Fase']['id'] > 1): ?>
		<div class="modal hide fade" id="nueva-credencial">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Oficio Credencial</h3>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('Credenciale', array('action' => 'add')); ?>
					<p>A continuación debe indicar los datos del oficio credencial del funcionario:</p>
					<fieldset>
						<div class="row-fluid">
							<div class="span6 control-group">
								<?php echo $this->Form->input('numero', array('class' => 'span12 mask-oc', 'label' => 'Número')); ?>
								<?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'class' => 'span12', 'value' => $this->request->data['Actuacionesfiscale']['id'])); ?>
								<?php echo $this->Form->input('funcionario_id', array('type' => 'hidden', 'class' => 'span12')); ?>
							</div>
							<div class="span5 control-group">
								<?php echo $this->Form->input('fecha', array('type' => 'text', 'class' => 'span12 date', 'label' => 'Fecha del oficio')); ?>
							</div>
						</div>
					</fieldset>
				<?php echo $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn close-credencial-modal">Cerrar</a>
				<a href="#" class="btn btn-primary continue-credencial-modal">Continuar</a>
			</div>
		</div>
	<?php endif; ?>
</div>
<?php echo $this->Html->script("objs"); ?>
<script type="application/x-javascript">
	$(function () {
		$("body").ready(function () {
			<?php foreach ($this->request->data['Objetivoespecifico'] as $objetivoespecifico): ?>
				obj_especificos[<?php echo $objetivoespecifico['id'] ?>] = '<?php echo $objetivoespecifico['denominacion'] ?>';
			<?php endforeach; ?>
			write_obj_especificos(obj_especificos, true);
		})
		
    
    function write_obj_especificos(obj_especificos, editar) {
        if (obj_especificos.length) {
            ul = $("<ul>").addClass("unstyled")
            m = 1;
            for ( i in obj_especificos) {
                input = $("<textarea>")
                $(input).attr({id: 'Objetivoespecifico' + i + 'Denominacion', name: 'data[Objetivoespecifico][' + i + '][denominacion]', 'class': 'span11'}).val(obj_especificos[i])
                li = $("<li>")
                $("<span>").addClass("content").text(m + ". ").appendTo(li)
                $(input).appendTo(li)
				
				input = $("<input>")
				$(input).attr({type: 'hidden', id: 'Objetivoespecifico' + i + 'Id', name: 'data[Objetivoespecifico][' + i + '][id]'}).val(i)
				$(input).appendTo(li)
                $('<a href="#" class="obj del-obj btn btn-danger btn-mini" alt="' + i + '">X</a>').appendTo(li)
                
                if (editar === true) {
                    input = $("<input>")
                    $(input).attr({type: 'hidden', id: 'Objetivoespecifico' + i + 'Id', name: 'data[Objetivoespecifico][' + i + '][id]'}).val(i)
                    $(input).appendTo(li)
                }
                $(li).appendTo(ul)
                m++;
            }
            $("#obj-container").html(ul)
    
            $("a.del-obj").click(function (e) {
                e.preventDefault();
                if (confirm("¿Está seguro que desea eliminar el objetivo específico?")) {
                    obj_especificos.splice($(this).attr("alt"), 1);
                    write_obj_especificos(obj_especificos)
                }
            })
        } else {
            $("#obj-container").html('<label>&nbsp;</label><p class="text-success">Para agregar objetivos específicos, debe ingresar su contenido y presionar el botón (+).</p>');
        }
    }
	})
</script>