<?php echo $this->Form->create('Credenciale', array('action' => 'add')); ?>
    <p>Antes de dar inicio a la presente fase, debe ingresar los datos solicitados en el siguiente formulario:</p>
    <fieldset>
        <legend><?php echo __('Oficio Credencial'); ?></legend>
        <div class="row-fluid">
            <div class="span2 control-group">
                <?php echo $this->Form->input('numero', array('class' => 'span12 mask-oc', 'label' => 'Número')); ?>
                <?php echo $this->Form->input('actuacionesfiscale_id', array('type' => 'hidden', 'class' => 'span12', 'value' => $actuacionesfiscale_id)); ?>
            </div>
            <div class="span2 control-group">
                <?php echo $this->Form->input('fecha', array('type' => 'text', 'class' => 'span12 date', 'label' => 'Fecha del oficio')); ?>
            </div>
        </div>
    </fieldset>
    <div class="form-actions">
        <button type="submit" class="btn btn-primary"><?php echo __("Continuar"); ?></button>
    </div>
<?php echo $this->Form->end(); ?>