<?php $this->Html->addCrumb('Orígenes de la Actuación', '/actuacionesorigenes'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $actuacionesorigene['Actuacionesorigene']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $actuacionesorigene['Actuacionesorigene']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid actuacionesorigenes view">
	<div class="span8">
		<h3><?php echo __('Actuaciones Fiscales asociadas a este orígen '); ?></h3>
		<?php if (!empty($actuacionesorigene['Actuacionesfiscale'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Numero'); ?></th>
						<th><?php echo __('Objetivo'); ?></th>
						<th class="span2"><?php echo __('Inicio Estimado'); ?></th>
						<th class="span2"><?php echo __('Fin Estimado'); ?></th>
						<th><?php echo __('Fase'); ?></th>
						<th><?php echo __('Objeto de Evaluación'); ?></th>
						<th class="span1 actions"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach ($actuacionesorigene['Actuacionesfiscale'] as $actuacionesfiscale): ?>
						<tr>
							<td><?php echo $actuacionesfiscale['numero']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_general']; ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_inicio_estimada']); ?></td>
							<td><?php echo $this->Time->format('d-m-Y', $actuacionesfiscale['fecha_fin_estimada']); ?></td>
							<td><?php echo $actuacionesfiscale['Fase']['denominacion']; ?></td>
							<td><?php echo $actuacionesfiscale['Objeto']['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Detalles'), array('controller' => 'actuacionesfiscales', 'action' => 'details', $actuacionesfiscale['id']), array('class' => 'btn btn-info btn-mini')); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="text-warning">No hay actuaciones fiscales asociadas al presente orígen</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4>Datos del Orígen de la Actuación</h4>
		<dl class="dl-horizontal">
			<dt>Denominación</dt>
			<dd><?php echo $actuacionesorigene['Actuacionesorigene']['denominacion']; ?></dd>
		</dl>
	</div>
</div>