<?php $this->Html->addCrumb('Orígenes de la Actuación', '/actuacionesorigenes'); ?>
<?php $this->Html->addCrumb('Nueva'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid actuacionesorigenes form">
	<?php echo $this->Form->create('Actuacionesorigene'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Orígen de la Actuación'); ?></legend>
			
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>