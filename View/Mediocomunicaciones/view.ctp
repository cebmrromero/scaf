<?php $this->Html->addCrumb('Medios de comunicación', '/mediocomunicaciones'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $mediocomunicacione['Mediocomunicacione']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $mediocomunicacione['Mediocomunicacione']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid mediocomunicaciones view">
	<div class="span8">&nbsp;</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del Medio de Comunicación'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Denominación'); ?></dt>
			<dd><?php echo h($mediocomunicacione['Mediocomunicacione']['denominacion']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Eventualidades'); ?></h3>
		<?php if (!empty($mediocomunicacione['Eventualidade'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Acción'); ?></th>
						<th><?php echo __('Tipo de Eventualidad'); ?></th>
						<th><?php echo __('Actuación Fiscal'); ?></th>
						<th><?php echo __('Detecta'); ?></th>
						<th><?php echo __('Fecha Detección'); ?></th>
						<th><?php echo __('Corrige/Revisa'); ?></th>
						<th><?php echo __('Fecha Verificación'); ?></th>
						<th><?php echo __('Número'); ?></th>
						<th><?php echo __('Asunto'); ?></th>
						<th><?php echo __('Estado'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($mediocomunicacione['Eventualidade'] as $eventualidade): ?>
						<tr>
							<td><?php echo $eventualidade['Evento']['denominacion']; ?></td>
							<td><?php echo $eventualidade['Eventualidadtipo']['denominacion']; ?></td>
							<td>#<?php echo $eventualidade['Actuacionesfiscale']['numero']; ?></td>
							<td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_deteccion']; ?></td>
							<td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
							<td><?php echo $eventualidade['fecha_verificacion']; ?></td>
							<td><?php echo $eventualidade['numero']; ?></td>
							<td><?php echo $eventualidade['asunto']; ?></td>
							<td><?php echo $eventualidadestados[$eventualidade['estado']]; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'eventualidades', 'action' => 'view', $eventualidade['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'eventualidades', 'action' => 'edit', $eventualidade['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $eventualidade['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $eventualidade['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay eventualidades asociadas al Medio de Comunicación</p>
		<?php endif; ?>
	</div>
</div>
