<?php $this->Html->addCrumb('Usuarios', '/users'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Cambiar Contraseña'), array('action' => 'password', $this->request->data['User']['id'])); ?></li>
	</ul>
</div>
<div class="row-fluid users form">
	<?php echo $this->Form->create('User'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Editar Usuario'); ?></legend>
			<div class="row-fluid">
				<?php if ($auth_user['group_id'] == 1): ?>
					<div class="span4 control-group">
						<?php echo $this->Form->input('funcionario_id', array('class' => 'span12', 'label' => 'Funcionario', 'empty' => '-Seleccionar-')); ?>
					</div>
					<div class="span4 control-group">
						<?php echo $this->Form->input('group_id', array('class' => 'span12', 'label' => 'Grupo', 'empty' => '-Seleccionar-')); ?>
					</div>
				<?php else:?>
					<div class="span4 control-group">
						<label>Funcionario</label>
						<?php echo $this->request->data['Funcionario']['nombre']; ?>
					</div>
					<div class="span4 control-group">
						<label>Grupo</label>
						<?php echo $this->request->data['Group']['name']; ?>
					</div>
				<?php endif; ?>
				<div class="span4 control-group">
					<?php echo $this->Form->input('username', array('class' => 'span12', 'label' => 'Nombre de Usuario')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
