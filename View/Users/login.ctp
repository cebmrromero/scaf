<?php $this->Html->addCrumb('Ingreso al Sistema'); ?>
<?php echo $this->Form->create('User', array('action' => 'login', 'class' => 'form-signin')); ?>
    <h2 class="form-signin-heading">Ingreso al Sistema</h2>
    <?php echo $this->Form->input('username', array('placeholder' => 'Nombre de usuario', 'class' => 'input-block-level', 'label' => false, 'autofocus' => true)); ?>
    <?php echo $this->Form->input('password', array('placeholder' => 'Contraseña', 'class' => 'input-block-level', 'label' => false)); ?>
<?php echo $this->Form->end(array('label' => 'Ingresar', 'class' => 'btn btn-large btn-primary')); ?>
