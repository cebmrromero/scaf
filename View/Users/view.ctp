<?php $this->Html->addCrumb('Usuarios', '/users'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $user['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid users view">
	<div class="span8">
		<h3><?php echo __('Aqui el 1era related'); ?></h3>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del / de la User'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Id'); ?></dt>
			<dd><?php echo h($user['User']['id']); ?>&nbsp;</dd>
			<dt><?php echo __('Username'); ?></dt>
			<dd><?php echo h($user['User']['username']); ?>&nbsp;</dd>
			<dt><?php echo __('Password'); ?></dt>
			<dd><?php echo h($user['User']['password']); ?>&nbsp;</dd>
			<dt><?php echo __('Group Id'); ?></dt>
			<dd><?php echo h($user['User']['group_id']); ?>&nbsp;</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd><?php echo h($user['User']['created']); ?>&nbsp;</dd>
			<dt><?php echo __('Modified'); ?></dt>
			<dd><?php echo h($user['User']['modified']); ?>&nbsp;</dd>
			<dt><?php echo __('Funcionario Id'); ?></dt>
			<dd><?php echo h($user['User']['funcionario_id']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo __('Actuacionesfiscales'); ?></h3>
		<?php if (!empty($user['Actuacionesfiscale'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Numero'); ?></th>
						<th><?php echo __('Alcance'); ?></th>
						<th><?php echo __('Obj General'); ?></th>
						<th><?php echo __('Obj Especificos'); ?></th>
						<th><?php echo __('Fecha Inicio Estimada'); ?></th>
						<th><?php echo __('Fecha Fin Estimada'); ?></th>
						<th><?php echo __('Fecha Inicio'); ?></th>
						<th><?php echo __('Fecha Fin'); ?></th>
						<th><?php echo __('Dh Planificacion'); ?></th>
						<th><?php echo __('Dh Ejecucion'); ?></th>
						<th><?php echo __('Dh Resultados'); ?></th>
						<th><?php echo __('Ejercicio Economico'); ?></th>
						<th><?php echo __('Fase Id'); ?></th>
						<th><?php echo __('Actuacionesorigene Id'); ?></th>
						<th><?php echo __('Actuaciontipo Id'); ?></th>
						<th><?php echo __('Objeto Id'); ?></th>
						<th><?php echo __('User Id'); ?></th>
						<th><?php echo __('Direccioncontrole Id'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($user['Actuacionesfiscale'] as $actuacionesfiscale): ?>
						<tr>
							<td><?php echo $actuacionesfiscale['id']; ?></td>
							<td><?php echo $actuacionesfiscale['numero']; ?></td>
							<td><?php echo $actuacionesfiscale['alcance']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_general']; ?></td>
							<td><?php echo $actuacionesfiscale['obj_especificos']; ?></td>
							<td><?php echo $actuacionesfiscale['fecha_inicio_estimada']; ?></td>
							<td><?php echo $actuacionesfiscale['fecha_fin_estimada']; ?></td>
							<td><?php echo $actuacionesfiscale['fecha_inicio']; ?></td>
							<td><?php echo $actuacionesfiscale['fecha_fin']; ?></td>
							<td><?php echo $actuacionesfiscale['dh_planificacion']; ?></td>
							<td><?php echo $actuacionesfiscale['dh_ejecucion']; ?></td>
							<td><?php echo $actuacionesfiscale['dh_resultados']; ?></td>
							<td><?php echo $actuacionesfiscale['ejercicio_economico']; ?></td>
							<td><?php echo $actuacionesfiscale['fase_id']; ?></td>
							<td><?php echo $actuacionesfiscale['actuacionesorigene_id']; ?></td>
							<td><?php echo $actuacionesfiscale['actuaciontipo_id']; ?></td>
							<td><?php echo $actuacionesfiscale['objeto_id']; ?></td>
							<td><?php echo $actuacionesfiscale['user_id']; ?></td>
							<td><?php echo $actuacionesfiscale['direccioncontrole_id']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'actuacionesfiscales', 'action' => 'view', $actuacionesfiscale['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'actuacionesfiscales', 'action' => 'edit', $actuacionesfiscale['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'actuacionesfiscales', 'action' => 'delete', $actuacionesfiscale['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $actuacionesfiscale['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay actuacionesfiscales asociados al/el User</p>
		<?php endif; ?>
	</div>
</div>
