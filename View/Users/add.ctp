<?php $this->Html->addCrumb('Usuarios', '/users'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid users form">
	<?php echo $this->Form->create('User'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Usuario'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('funcionario_id', array('class' => 'span12', 'label' => 'Funcionario', 'empty' => '-Seleccionar-')); ?>
				</div>
				<div class="span6 control-group">
					<?php echo $this->Form->input('group_id', array('class' => 'span12', 'label' => 'Grupo', 'empty' => '-Seleccionar-')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php echo $this->Form->input('username', array('class' => 'span12', 'label' => 'Nombre de Usuario')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('password', array('class' => 'span12', 'label' => 'Contraseña')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Form->input('password2', array('type' => 'password', 'class' => 'span12', 'label' => 'Confirmar Contraseña')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
