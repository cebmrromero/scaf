<?php $this->Html->addCrumb('Usuarios', '/users'); ?>
<?php $this->Html->addCrumb('Editar'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $this->request->data['User']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Cambiar Contraseña'), array('action' => 'password', $this->request->data['User']['id'])); ?></li>
	</ul>
</div>
<div class="row-fluid users form">
	<?php echo $this->Form->create('User'); ?>
		<?php echo $this->Form->input('id'); ?>
		<fieldset>
			<legend><?php echo __('Cambiar Contraseña'); ?></legend>
			<div class="row-fluid">
				<div class="span2 control-group">
                    <label>Funcionario</label>
					<?php echo $this->request->data['Funcionario']['nombre']; ?>
				</div>
				<div class="span2 control-group">
                    <label>Grupo</label>
					<?php echo $this->request->data['Group']['name']; ?>
				</div>
				<div class="span2 control-group">
                    <label>Usuario</label>
					<?php echo $this->request->data['User']['username']; ?>
				</div>
                <div class="span6 control-group">
                    <div class="row-fluid">
                        <div class="span6 control-group">
                            <?php echo $this->Form->input('password', array('class' => 'span12', 'value' => '', 'label' => 'Nueva Contraseña')); ?>
                        </div>
                        <div class="span6 control-group">
                            <?php echo $this->Form->input('password2', array('type' => 'password', 'class' => 'span12', 'label' => 'Confirmar Contraseña')); ?>
                        </div>
                    </div>
                </div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
