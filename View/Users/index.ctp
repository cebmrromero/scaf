<?php $this->Html->addCrumb('Usuarios', '/users'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="search-div well">
	<?php echo $this->Search->create(); ?>
		<fieldset>
			<legend><?php echo __('Buscar Usuarios'); ?></legend>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Search->input('username', array('class' => 'span12', 'required' => false, 'label' => 'Nombre de Usuario')); ?>
				</div>
				<div class="span4 control-group">
					<?php echo $this->Search->input('funcionario', array('class' => 'span12', 'required' => false, 'label' => 'Nombre del Funcionario')); ?>
				</div>
				<div class="span3 control-group">
					<?php echo $this->Search->input('group_id', array('class' => 'span12', 'required' => false, 'label' => 'Grupo', 'empty' => 'Cualquiera')); ?>
				</div>
				<div class="span2 control-group">
					<label>&nbsp;</label>
					<?php echo $this->Form->input('Buscar', array('type' => 'submit', 'class' => 'btn btn-primary', 'label' => false, 'div' => false)); ?>
					<?php echo $this->Html->link('Limpiar', array('action' => 'index')); ?>
				</div>
			</div>
		</fieldset>
	<?php echo $this->Search->end(); ?>
</div>

<h3>Usuarios del Sistema</h3>
<div class="row-fluid users index">
	<div class="span12">
		<p>A continuación el listado de usuarios del sistema:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('username', 'Nombre de Usuario'); ?></th>
					<th><?php echo $this->Paginator->sort('funcionario_id'); ?></th>
					<th><?php echo $this->Paginator->sort('group_id', 'Grupo'); ?></th>
					<th><?php echo $this->Paginator->sort('created', 'Fecha de Creación'); ?></th>
					<th><?php echo $this->Paginator->sort('modified', 'Fecha de Modificación'); ?></th>
					<th class="actions"><?php echo __('Acciones'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user): ?>
					<tr>
						<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
						<td><?php echo h($user['Funcionario']['nombre']); ?>&nbsp;</td>
						<td><?php echo h($user['Group']['name']); ?>&nbsp;</td>
						<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
						<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
						<td class="actions span1">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php if ($user['User']['bloqueado']): ?>
									<button class="btn btn-danger btn-mini disabled">Eliminar</button>
								<?php else: ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $user['User']['id'])); ?>
								<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid users index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
