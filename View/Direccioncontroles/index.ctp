<?php $this->Html->addCrumb('Direcciones de Control', '/direccioncontroles'); ?>
<?php $this->Html->addCrumb('Listado'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('Nueva'), array('action' => 'add')); ?></li>
	</ul>
</div>
<h3>Direcciones de Control</h3>
<div class="row-fluid direccioncontroles index">
	<div class="span12">
		<p>A continuación el listado de direcciones de control registradas:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('denominacion', 'Denominación'); ?></th>
					<th class="actions span1"><?php echo __('Acciones'); ?></th>
				</tr>
			<tbody>
				<?php foreach ($direccioncontroles as $direccioncontrole): ?>
					<tr>
						<td><?php echo h($direccioncontrole['Direccioncontrole']['denominacion']); ?>&nbsp;</td>
						<td class="actions">
							<div class="btn-group">
								<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $direccioncontrole['Direccioncontrole']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $direccioncontrole['Direccioncontrole']['id']), array('class' => 'btn btn-info btn-mini')); ?>
								<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $direccioncontrole['Direccioncontrole']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', $direccioncontrole['Direccioncontrole']['id'])); ?>
							</div>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid direccioncontroles index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php
				echo $this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
				echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
				echo $this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
			?>
		</ul>
	</div>
</div>
