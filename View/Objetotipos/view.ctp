<?php $this->Html->addCrumb('Tipos de Objeto a Evaluar', '/objetotipos'); ?>
<?php $this->Html->addCrumb('Ver'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Ver'), array('action' => 'view', $objetotipo['Objetotipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $objetotipo['Objetotipo']['id'])); ?></li>
		<li><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid objetotipos view">
	<div class="span8">
		<h3><?php echo __('Objetos a Evaluar según el Tipo'); ?></h3>
		<?php if (!empty($objetotipo['Objeto'])): ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
						<th><?php echo __('Denominación'); ?></th>
						<th class="actions span1"><?php echo __('Acciones'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$i = 0;
					foreach ($objetotipo['Objeto'] as $objeto): ?>
						<tr>
							<td><?php echo $objeto['denominacion']; ?></td>
							<td class="actions">
								<div class="btn-group">
									<?php echo $this->Html->link(__('Ver'), array('controller' => 'objetos', 'action' => 'view', $objeto['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Html->link(__('Editar'), array('controller' => 'objetos', 'action' => 'edit', $objeto['id']), array('class' => 'btn btn-mini btn-info')); ?>
									<?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'objetos', 'action' => 'delete', $objeto['id']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', $objeto['id'])); ?>
								</div>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p>No hay objetos a evaluar asociados al Tipo de Objeto</p>
		<?php endif; ?>
	</div>
	<div class="span4 well">
		<h4><?php  echo __('Datos del / de la Objetotipo'); ?></h4>
		<dl class="dl-horizontal">
			<dt><?php echo __('Denominación'); ?></dt>
			<dd><?php echo h($objetotipo['Objetotipo']['denominacion']); ?>&nbsp;</dd>
		</dl>
	</div>
</div>