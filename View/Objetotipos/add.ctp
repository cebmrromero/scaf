<?php $this->Html->addCrumb('Tipos de Objeto a Evaluar', '/objetotipos'); ?>
<?php $this->Html->addCrumb('Nuevo'); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link(__('Listado'), array('action' => 'index')); ?></li>
		<li class="active"><?php echo $this->Html->link(__('Nuevo'), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="row-fluid objetotipos form">
	<?php echo $this->Form->create('Objetotipo'); ?>
		<fieldset>
			<legend><?php echo __('Nuevo Tipo de Objeto a Evaluar'); ?></legend>
			<div class="row-fluid">
				<div class="span6 control-group">
					<?php echo $this->Form->input('denominacion', array('class' => 'span12', 'label' => 'Denominación')); ?>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
