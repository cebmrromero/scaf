<?php $this->Html->addCrumb('Listado', '/actuacionesfiscales'); ?>
<?php $this->Html->addCrumb('Nueva'); ?>
<div class="actuacionesfiscales form">
	<h3>Nueva Actuación Fiscal</h3>
	<?php echo $this->Form->create('Actuacionesfiscale'); ?>
	<?php $this->Form->inputDefaults(array('error' => array('attributes' => array('wrap' => true, 'class' => 'text-error span12 text-center'))));?>
		<fieldset>
			<legend><?php echo __('Datos de la Actuación'); ?></legend>
			<div class="row-fluid">
				<div class="span7 control-group">
					<?php echo $this->Form->input('direccioncontrole_id', array('class' => 'span12', 'label' => 'Dirección de Control', 'empty' => '-Seleccione-')); ?>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('numero', 'Número'); ?>
						<div class="input-prepend span10">
							<span class="add-on">#</span>
							<?php echo $this->Form->input('numero', array('class' => 'span12 mask-af', 'label' => false, 'div' => false, 'value' => $numero_af)); ?>
						</div>
					</div>
				</div>
				<div class="span3 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('Ejerciciosfiscale', 'Ejercicio Económico Financiero'); ?>
						<?php echo $this->Form->input('Ejerciciosfiscale', array('class' => 'span12 multiselect', 'multiple' => 'multiple', 'empty' => false, 'label' => false, 'default' => date('Y') - 1)); ?>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<div class="hidden">
						<?php echo $this->Form->input('objeto_id'); ?>
					</div>
					<?php echo $this->Form->input('objeto_id', array('id' => 'objeto_id', 'autocomplete' => 'off', 'name' => 'objeto_id', 'type' => 'text', 'class' => 'span12', 'label' => 'Objeto de la Actuación', 'placeholder' => 'Escriba un texto referente al objeto, ejemplo: Metropolitano')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('obj_general', array('class' => 'span12', 'label' => 'Objetivo General', 'rows' => '4')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12 control-group">
					<?php echo $this->Form->input('alcance', array('class' => 'span12', 'label' => 'Alcance', 'rows' => '4')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span4 control-group">
					<?php echo $this->Form->input('objetivo_content', array('class' => 'span12', 'name' => 'objetivo_content', 'label' => 'Objetivos Específicos', 'rows' => '2', 'placeholder' => 'Indicar el Objetivo Especifico y presionar el boton verde a continuacion para agregarlo')); ?>
				</div>
				<div class="span1 control-group">
					<p class="text-center">
						<label>&nbsp;</label>
						<a href="#" id="add-objetivo" class="btn btn-success btn-mini">+</a>
					</p>
				</div>
				<div class="span7 control-group" id="obj-container">
					<label>&nbsp;</label>
					<p class="text-success">Para agregar objetivos específicos, debe ingresar su contenido y presionar el botón (+).</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span3 control-group">
					<?php echo $this->Form->input('actuaciontipo_id', array('class' => 'span12', 'label' => 'Tipo de Actuación')); ?>
				</div>
				<div class="span9 control-group">
					<?php echo $this->Form->input('actuacionesorigene_id', array('class' => 'span12', 'label' => 'Origen de la Actuación')); ?>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2 control-group">
					<div class="input text required ">
						<?php echo $this->Form->label('fecha_inicio_estimada', 'Iniciará el'); ?>
						<div class="input-prepend span10">
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo $this->Form->input('fecha_inicio_estimada', array('type' => 'text', 'readonly' => 'readonly', 'class' => 'span12 date-start text-center', 'label' => false, 'div' => false, 'value' => date('d-m-Y'))); ?>
						</div>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="input text required ">
						<?php echo $this->Form->label('fecha_fin_estimada', 'Finalizará el'); ?>
						<div class="input-prepend span10">
							<span class="add-on"><i class="icon-calendar"></i></span>
							<?php echo $this->Form->input('fecha_fin_estimada', array('type' => 'text', 'required' => 'required', 'class' => 'span12 date-end text-center', 'label' => false, 'div' => false)); ?>
						</div>
					</div>
				</div>
				<div class="span2 control-group">
					<label>Hábiles Disponibles</label>
					<p class="text-right">
						<span id="dh-span">0</span>
						<span class="add-on">Días</span>
					</p>
					<?php echo $this->Form->input('dh-input', array('type' => 'hidden', 'value' => 0, 'id' => 'dh-input', 'name' => 'dh-input')); ?>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_planificacion', 'Tiempo Planificacion'); ?>
						<div class="input-append span9">
							<?php echo $this->Form->input('dh_planificacion', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
							<span class="add-on">Días</span>
						</div>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_ejecucion', 'Tiempo Ejecución'); ?>
						<div class="input-append span9">
							<?php echo $this->Form->input('dh_ejecucion', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
							<span class="add-on">Días</span>
						</div>
					</div>
				</div>
				<div class="span2 control-group">
					<div class="input number required ">
						<?php echo $this->Form->label('dh_resultados', 'Tiempo Resultados'); ?>
						<div class="input-append span9">
							<?php echo $this->Form->input('dh_resultados', array('disabled' => 'disabled', 'required' => 'required', 'class' => 'span12 dhabil text-right', 'label' => false, 'div' => false, 'min' => 1)); ?>
							<span class="add-on">Días</span>
						</div>
					</div>
				</div>
			</div>
			<div class="control-group">
				<?php echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $auth_user['id'])); ?>
			</div>
		</fieldset>
		<fieldset id="equipo-autidor">
			<legend><?php echo __('Equipo Auditor'); ?></legend>
				<div class="row-fluid">
					<div class="span4 control-group required">
						<label><strong>Rol</strong></label>
					</div>
					<div class="span3 control-group required">
						<label><strong>Funcionario</strong></label>
					</div>
					<div class="span2 control-group required">
						<label><strong>Nota de Designación</strong></label>
					</div>
					<div class="span3 control-group required">
						<label><strong>Fecha de Asignación</strong></label>
					</div>
				</div>
				<?php $i = 0; ?>
				<?php foreach ($funcionarioroles as $funcionariorole): ?>
					<?php if (!$funcionariorole['Funcionariorole']['requerido']): ?>
						<?php continue; ?>
					<?php endif; ?>
					<div class="row-fluid">
						<div class="span4 control-group">
							<?php echo $this->Form->input('Equipotrabajo.'.$i.'.funcionariorole_id', array('type' => 'select', 'class' => 'span12 funcionariorole-changer ', 'data-changer'=> '#Equipotrabajo'.$i.'FuncionarioId', 'required' => 'required', 'options' => $funcionarioroles_list, 'value' => $funcionariorole['Funcionariorole']['id'], 'label' => false, 'alt' => $this->Html->url(array('controller' => 'json', 'action' => 'getFuncionariosByRol')))); ?>
						</div>
						<div class="span3 control-group">
							<?php echo $this->Form->input('Equipotrabajo.'.$i.'.funcionario_id', array('type' => 'select', 'class' => 'span12 funcionario', 'required' => 'required', 'options' => array(), 'empty' => '-Seleccionar-', 'label' => false, 'disabled' => 'disabled')); ?>
						</div>
						<div class="span2 control-group">
							<div class="input text required">
								<div class="input-prepend span10">
									<span class="add-on">#</span>
									<?php echo $this->Form->input('Equipotrabajo.'.$i.'.nota_designacion', array('type' => 'text', 'class' => 'span12 mask-nd', 'required' => 'required', 'label' => false, 'div' => false)); ?>
								</div>
							</div>
						</div>
						<div class="span2 control-group">
							<div class="input text required ">
								<div class="input-prepend span10">
									<span class="add-on"><i class="icon-calendar"></i></span>
									<?php echo $this->Form->input('Equipotrabajo.'.$i.'.fecha', array('type' => 'text', 'readonly' => 'readonly', 'class' => 'span12 date text-center', 'required' => 'required', 'label' => false, 'div' => false, 'value' => date('d-m-Y'))); ?>
								</div>
							</div>
						</div>
						<div class="span1 control-group">
							<?php echo $this->Html->link("X", "#", array("class" => "del-funcionario btn btn-danger btn-mini")); ?>
						</div>
						<?php $i++; ?>
					</div>
				<?php endforeach; ?>
				<?php $i = 0; ?>
				<?php foreach ($eventos as $k => $v): ?>
						<?php echo $this->Form->input("ActuacionesfiscalesEvento.$i.evento_id", array('type' => 'hidden', 'value' => $k)); ?>
						<?php echo $this->Form->input("ActuacionesfiscalesEvento.$i.es_activo", array('type' => 'hidden', 'value' => ($i == 0) ? 1 : 0)); ?>
						<?php $i++; ?>
				<?php endforeach; ?>
		</fieldset>
		<?php echo $this->Html->link("Agregar Funcionario", "#", array("id" => "add-funcionario", "class" => "btn btn-primary btn-mini")); ?>
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Guardar</button>
			<button type="button" class="btn cancel">Cancelar</button>
		</div>
	<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->Html->script("objs"); ?>