<?php $this->Html->addCrumb('Listado'); ?>
<h3>Resumen de Actuaciones Fiscales</h3>
<div class="row-fluid">
	<div class="span5">
		<h4>Actuaciones fiscales en las cuales usted participa</h4>
		<p>A continuación se listas todas las actuaciones fiscales en las cuales participa</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th>Número</th>
					<th>Objeto de Evaluación</th>
					<th>Dirección de Control</th>
					<th>Fase</th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($actuacionesfiscales)): ?>
					<?php foreach($actuacionesfiscales as $actuacionesfiscale): ?>
						<tr>
							<td><?php echo h($actuacionesfiscale['Actuacionesfiscale']['numero']); ?></td>
							<td><?php echo h($actuacionesfiscale['Objeto']['denominacion']); ?></td>
							<td><?php echo h($actuacionesfiscale['Direccioncontrole']['denominacion']); ?></td>
							<td><?php echo h($actuacionesfiscale['Fase']['denominacion']); ?></td>
							<td><?php echo $this->Html->link('<i class="icon-search"></i>', array('action' => 'details', $actuacionesfiscale['Actuacionesfiscale']['id']), array('class' => 'btn btn-mini', 'escape' => false)); ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5" class="text-success">Actualmente no participa en ninguna Actuación Fiscal</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
	<div class="span7">
		<h4>Actividades recientes que requieren su atención</h4>
		<p>
			Los siguientes registros, corresponden a las actividades llevadas a cabo sobre la actuación fiscal
			y que requieren de su atención
		</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th class="span1">Actuación</th>
					<th class="span2">Acción</th>
					<th class="span2">Fecha</th>
					<th class="span2">Remitente</th>
					<th class="span3">Descripción</th>
					<th class="span1">¿Correcciones?</th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($operaciones)): ?>
					<?php foreach($operaciones as $operacione): ?>
						<tr class="warning">
							<td><?php echo h($operacione['Actuacionesfiscale']['numero']); ?></td>
							<td><?php echo h($operacione['Evento']['denominacion']); ?></td>
							<td><?php echo h($this->Time->format('d-m-Y h:i A', $operacione['Operacione']['fecha_remite'])); ?></td>
							<td><?php echo h($operacione['Fremite']['nombre']); ?></td>
							<td><?php echo h($operacione['Operacione']['observaciones']); ?></td>
							<td><?php echo ($operacione['Operacione']['eventualidadtipo_id'] == Configure::read('App.CORECCION_ID')) ? h('Sí') : h('No'); ?></td>
							<td><?php echo $this->Html->link('<i class="icon-search"></i>', array('action' => 'details', $operacione['Operacione']['actuacionesfiscale_id']), array('class' => 'btn btn-mini', 'escape' => false)); ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="7" class="text-success">No hay actividades pendientes</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		
		<h4>Eventualidades recientes que requieren su atención</h4>
		<p>
			Los siguientes registros, corresponden a las eventualidades que requieren de su revisión/aprobación
		</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
					<th class="span1">Actuación</th>
					<th class="span2">Acción</th>
					<th class="span2">Fecha</th>
					<th class="span2">Remitente</th>
					<th class="span4">Asunto</th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php if (!empty($eventualidades)): ?>
					<?php foreach($eventualidades as $eventualidade): ?>
						<tr class="warning" title="Observaciones" data-content="<?php echo $eventualidade['Eventualidade']['observaciones'] ?><?php echo ($eventualidade['Eventualidade']['municipio']) ? "<br /><strong>Municipio:</strong> {$eventualidade['Eventualidade']['municipio']}" : '' ?><?php echo ($eventualidade['Eventualidade']['lugar']) ? "<br /><strong>Lugar:</strong> {$eventualidade['Eventualidade']['lugar']}" : '';  ?>">
							<td><?php echo h($eventualidade['Actuacionesfiscale']['numero']); ?></td>
							<td><?php echo h($eventualidade['Evento']['denominacion']); ?></td>
							<td><?php echo h($this->Time->format('d-m-Y h:i A', $eventualidade['Eventualidade']['fecha_deteccion'])); ?></td>
							<td><?php echo h($eventualidade['Detecta']['nombre']); ?></td>
							<td><?php echo h($eventualidade['Eventualidade']['asunto']); ?></td>
							<td><?php echo $this->Html->link('<i class="icon-search"></i>', array('action' => 'details', $eventualidade['Eventualidade']['actuacionesfiscale_id']), array('class' => 'btn btn-mini', 'escape' => false)); ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="6" class="text-success">No hay eventualidades pendientes</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>
