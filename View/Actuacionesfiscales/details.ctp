<?php $this->Html->addCrumb('Listado', '/actuacionesfiscales'); ?>
<?php $this->Html->addCrumb('Detalles'); ?>
<div class="row-fluid">
	<div class="span12 breadcrumb actuacionesfiscaleswell">
		<dl class="pull-left">
			<dt>Actuación Fscal</dt>
			<dd>#<?php echo $this->request->data['Actuacionesfiscale']['numero']; ?></dd>
		</dl>
		<dl class="pull-left">
			<dt>Tipo de Actuación</dt>
			<dd><?php echo $this->request->data['Actuaciontipo']['denominacion']; ?></dd>
		</dl>
		<?php if ($this->request->data['Actuacionesfiscale']['fecha_inicio']): ?>
			<dl class="pull-left">
				<dt>Fecha de Inicio Real</dt>
				<dd><?php echo $this->Time->format('d-m-Y', $this->request->data['Actuacionesfiscale']['fecha_inicio']); ?></dd>
			</dl>
		<?php else: ?>
			<dl class="pull-left">
				<dt>Fecha de Inicio Estimada</dt>
				<dd><?php echo $this->Time->format('d-m-Y', $this->request->data['Actuacionesfiscale']['fecha_inicio_estimada']); ?></dd>
			</dl>
		<?php endif; ?>
		<?php if ($this->request->data['Actuacionesfiscale']['fecha_fin']): ?>
			<dl class="pull-left">
				<dt>Fecha de Fin Real</dt>
				<dd><?php echo $this->Time->format('d-m-Y', $this->request->data['Actuacionesfiscale']['fecha_fin']); ?></dd>
			</dl>
		<?php else: ?>
			<dl class="pull-left">
				<dt>Fecha de Fin Estimada</dt>
				<dd><?php echo $this->Time->format('d-m-Y', $this->request->data['Actuacionesfiscale']['fecha_fin_estimada']); ?></dd>
			</dl>
		<?php endif; ?>
		<dl class="pull-left">
			<dt>Planificación</dt>
			<dd><?php echo $this->request->data['Actuacionesfiscale']['dh_planificacion']; ?> Días</dd>
		</dl>
		<dl class="pull-left">
			<dt>Ejecución</dt>
			<dd><?php echo $this->request->data['Actuacionesfiscale']['dh_ejecucion']; ?> Días</dd>
		</dl>
		<dl class="pull-left">
			<dt>Resultados</dt>
			<dd><?php echo $this->request->data['Actuacionesfiscale']['dh_resultados']; ?> Días</dd>
		</dl>
		<dl class="pull-left">
			<dt>Objeto de Evaluación</dt>
			<dd><?php echo $this->request->data['Objeto']['denominacion']; ?></dd>
		</dl>
	</div>
</div>
<div class="tabbable">
	<ul class="nav nav-tabs">
		<?php foreach($fases_all as $f): ?>
			<li<?php echo ($this->request->data['Actuacionesfiscale']['fase_id'] == $f['Fase']['id']) ? ' class="active"' : ''; ?>>
				<a href="#tab<?php echo $f['Fase']['id']; ?>" data-toggle="tab">Fase <?php echo $f['Fase']['id']; ?>: <?php echo $f['Fase']['denominacion']?></a>
			</li>
		<?php endforeach; ?>
		<li class="pull-right">
			<a href="#tab<?php echo $f['Fase']['id'] + 1; ?>" data-toggle="tab">Editar Actuación Fiscal</a>
		</li>
	</ul>
	<div class="tab-content">
		<?php foreach($fases_all as $f): ?>
			<div class="tab-pane row-fluid<?php echo ($this->request->data['Actuacionesfiscale']['fase_id'] == $f['Fase']['id']) ? ' active' : ''; ?>" id="tab<?php echo $f['Fase']['id']; ?>">
				<?php echo $this->element('eventos-tab', array('title_tab' => $f['Fase']['denominacion'], 'fase' => $f['Fase']['id'])); ?>
			</div>
		<?php endforeach; ?>
		<div class="tab-pane row-fluid" id="tab<?php echo $f['Fase']['id'] + 1; ?>">
			<?php echo $this->element('actuacionfiscal-editar'); ?>
		</div>
	</div>
</div>