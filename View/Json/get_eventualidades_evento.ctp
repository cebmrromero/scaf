<?php echo $this->Html->Link("Actualizar", array('controller' => 'json', 'action' => 'getEventualidadesEvento', $actuacionesfiscale_id, $evento_id), array("class" => 'update-eventualidades hidden')); ?>
<table class="table table-hover table-striped table-condensed">
    <thead>
        <tr>
            <th>Fecha Detección</th>
            <th>Funcionario Detecta</th>
            <th>Fecha Verificación</th>
            <th>Funcionario Verifica</th>
			<th>Tiempo de Respuesta</th>
            <th>Tipo</th>
            <th class="span1">Estado</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (array_reverse($eventualidades) as $eventualidade): ?>
            <tr<?php echo ($eventualidade['Eventualidade']['estado'] == 'NR' && ($eventualidade['Eventualidade']['corrige_id'] == $auth_user['funcionario_id'])) ? ' class="success"' : '';?> title="<?php echo $eventualidade['Eventualidade']['asunto']; ?>" data-content="<?php echo $eventualidade['Eventualidade']['observaciones'] ?><?php echo ($eventualidade['Eventualidade']['municipio']) ? "<br /><strong>Municipio:</strong> {$eventualidade['Eventualidade']['municipio']}" : '' ?><?php echo ($eventualidade['Eventualidade']['lugar']) ? "<br /><strong>Lugar:</strong> {$eventualidade['Eventualidade']['lugar']}" : '';  ?>">
                <td><?php echo $this->Time->format('d-m-Y h:i A', $eventualidade['Eventualidade']['fecha_deteccion']); ?></td>
                <td><?php echo $eventualidade['Detecta']['nombre']; ?></td>
                <td><?php echo ($eventualidade['Eventualidade']['fecha_verificacion']) ? $this->Time->format('d-m-Y h:i A', $eventualidade['Eventualidade']['fecha_verificacion']) : '-'; ?></td>
                <td><?php echo $eventualidade['Corrige']['nombre']; ?></td>
                <td><?php echo ($eventualidade['Eventualidade']['diff_verificacion']) ? $eventualidade['Eventualidade']['diff_verificacion'] . ' Días' : '-'; ?></td>
                <td><?php echo $eventualidade['Eventualidadtipo']['denominacion']; ?></td>
                <td>
                    <?php if ($eventualidade['Eventualidade']['estado'] == 'R'): ?>
                        <span class="btn btn-info btn-mini disabled">Revisada</span>
                    <?php elseif ($eventualidade['Eventualidade']['estado'] == 'A'): ?>
                        <span class="btn btn-success btn-mini disabled">Aprobada</span>
                    <?php elseif ($eventualidade['Eventualidade']['estado'] == 'N'): ?>
                        <span class="btn btn-danger btn-mini disabled">Negada</span>
                    <?php else: ?>
                        <?php if ($eventualidade['Eventualidade']['corrige_id'] == $auth_user['funcionario_id']): ?>
                            <div class="btn-group" data-toggle="buttons-radio">
                                <?php echo $this->Html->link('Revisar', array('controller' => 'eventualidades', 'action' => 'marcar_revisada', $eventualidade['Eventualidade']['id']), array('class' => 'btn btn-info btn-mini')); ?>
                                <?php echo $this->Html->link('Aprobar', array('controller' => 'eventualidades', 'action' => 'marcar_aprobada', $eventualidade['Eventualidade']['id']), array('class' => 'btn btn-success btn-mini')); ?>
                                <?php echo $this->Html->link('Negar', array('controller' => 'eventualidades', 'action' => 'marcar_negada', $eventualidade['Eventualidade']['id']), array('class' => 'btn btn-danger btn-mini')); ?>
                            </div>
                        <?php else: ?>
                            <div class="control-group">
                                <div class="btn-group">
                                    <?php if ($eventualidade['Eventualidade']['detecta_id'] == $auth_user['funcionario_id']): ?>
                                        <?php echo $this->Html->link('Editar', array('controller' => 'eventualidades', 'action' => 'edit', $eventualidade['Eventualidade']['id']), array('class' => 'btn btn-info btn-mini')); ?>
                                        <?php echo $this->Form->postLink(__('Eliminar'), array('controller' => 'eventualidades', 'action' => 'delete', $eventualidade['Eventualidade']['id']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?')); ?>
                                    <?php endif; ?>
                                    <span class="btn btn-warning btn-mini disabled">Pendiente</span>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php echo $this->Html->scriptBlock('
$(function () {
    $( "tr" ).popover({html:true, trigger: "hover", placement: "top"});
});
');
?>