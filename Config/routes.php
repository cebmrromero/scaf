<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'actuacionesfiscales', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Conectado una ruta para permitir agregar operaciones asociadas a un evento especifico
 */
    Router::connect('/operaciones/add/:actuacionesfiscale_id/:evento_id/', array('controller' => 'operaciones', 'action' => 'add', 'actuacionesfiscale_id' => '[0-9]+', 'evento_id' => '[0-9]+'));

/**
 * Conectado una ruta para permitir finalizar eventos de una actuacion fiscal
 */
    Router::connect('/actuacionesfiscales_eventos/finalizar/:actuacionesfiscale_id/:evento_id/', array('controller' => 'actuacionesfiscales_eventos', 'action' => 'finalizar', 'actuacionesfiscale_id' => '[0-9]+', 'evento_id' => '[0-9]+'));

/**
 * Conectado una ruta para permitir agregar eventualidades a una actuacion
 */
    Router::connect('/eventualidades/add/:actuacionesfiscale_id/:evento_id/:corrige_id/:es_correccion/', array('controller' => 'eventualidades', 'action' => 'add', 'actuacionesfiscale_id' => '[0-9]+', 'evento_id' => '[0-9]+', 'corrige_id' => '[0-9]+', 'es_correccion' => '[0-9]+'));
/**
 * Conectado una ruta para cambiar fase de la actuacion fiscal
 */
    Router::connect('/actuacionesfiscales/cambiar_fase/:id/:fase_id/', array('controller' => 'actuacionesfiscales', 'action' => 'cambiar_fase', 'id' => '[0-9]+', 'fase_id' => '[0-9]+'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
