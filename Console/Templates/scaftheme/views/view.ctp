<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php echo "<?php \$this->Html->addCrumb('{$pluralHumanName}', '/{$pluralVar}'); ?>\n"; ?>
<?php echo "<?php \$this->Html->addCrumb('Ver'); ?>\n"; ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo "<?php echo \$this->Html->link(__('Listado'), array('action' => 'index')); ?>"; ?></li>
		<li class="active"><?php echo "<?php echo \$this->Html->link(__('Ver'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>"; ?></li>
		<li><?php echo "<?php echo \$this->Html->link(__('Editar'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>";?></li>
		<li><?php echo "<?php echo \$this->Html->link(__('Nuevo'), array('action' => 'add')); ?>"; ?></li>
	</ul>
</div>
<div class="row-fluid <?php echo $pluralVar; ?> view">
	<div class="span8">
		<h3><?php echo "<?php echo __('Aqui el 1era related'); ?>"; ?></h3>
	</div>
	<div class="span4 well">
		<h4><?php echo "<?php  echo __('Datos del / de la {$singularHumanName}'); ?>"; ?></h4>
		<dl class="dl-horizontal">
<?php
foreach ($fields as $field) {
	echo "\t\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
	echo "\t\t\t<dd><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</dd>\n";
}
?>
		</dl>
	</div>
</div>
<?php
if (!empty($associations['hasOne'])) :
	foreach ($associations['hasOne'] as $alias => $details): ?>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo "<?php echo __('" . Inflector::humanize($details['controller']) . "'); ?>"; ?></h3>
	<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
		<dl>
	<?php
			foreach ($details['fields'] as $field) {
				echo "\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
				echo "\t\t<dd>\n\t<?php echo \${$singularVar}['{$alias}']['{$field}']; ?>\n&nbsp;</dd>\n";
			}
	?>
		</dl>
	<?php echo "<?php endif; ?>\n"; ?>
		</div>
	</div>
	<?php
	endforeach;
endif;
if (empty($associations['hasMany'])) {
	$associations['hasMany'] = array();
}
if (empty($associations['hasAndBelongsToMany'])) {
	$associations['hasAndBelongsToMany'] = array();
}
$relations = array_merge($associations['hasMany'], $associations['hasAndBelongsToMany']);
$i = 0;
foreach ($relations as $alias => $details):
	$otherSingularVar = Inflector::variable($alias);
	$otherPluralHumanName = Inflector::humanize($details['controller']);
	?>
<div class="row-fluid related">
	<div class="span12">
		<h3><?php echo "<?php echo __('" . $otherPluralHumanName . "'); ?>"; ?></h3>
		<?php echo "<?php if (!empty(\${$singularVar}['{$alias}'])): ?>\n"; ?>
			<table class="table table-hover table-striped table-condensed">
				<thead>
					<tr>
<?php
			foreach ($details['fields'] as $field) {
				echo "\t\t\t\t\t\t<th><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
			}
?>
						<th class="actions span1"><?php echo "<?php echo __('Acciones'); ?>"; ?></th>
					</tr>
				</thead>
				<tbody>
<?php
echo "\t\t\t\t\t<?php
		\t\t\t\$i = 0;
		\t\t\tforeach (\${$singularVar}['{$alias}'] as \${$otherSingularVar}): ?>\n";
		echo "\t\t\t\t\t\t<tr>\n";
			foreach ($details['fields'] as $field) {
				echo "\t\t\t\t\t\t\t<td><?php echo \${$otherSingularVar}['{$field}']; ?></td>\n";
			}

			echo "\t\t\t\t\t\t\t<td class=\"actions\">\n";
			echo "\t\t\t\t\t\t\t\t<div class=\"btn-group\">\n";
			echo "\t\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('Ver'), array('controller' => '{$details['controller']}', 'action' => 'view', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-mini btn-info')); ?>\n";
			echo "\t\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('Editar'), array('controller' => '{$details['controller']}', 'action' => 'edit', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-mini btn-info')); ?>\n";
			echo "\t\t\t\t\t\t\t\t\t<?php echo \$this->Form->postLink(__('Eliminar'), array('controller' => '{$details['controller']}', 'action' => 'delete', \${$otherSingularVar}['{$details['primaryKey']}']), array('class' => 'btn btn-mini btn-danger'), __('¿Está seguro(a) que desea eliminar éste registro?', \${$otherSingularVar}['{$details['primaryKey']}'])); ?>\n";
			echo "\t\t\t\t\t\t\t\t</div>\n";
			echo "\t\t\t\t\t\t\t</td>\n";
		echo "\t\t\t\t\t\t</tr>\n";

echo "\t\t\t\t\t<?php endforeach; ?>\n";
?>
				</tbody>
			</table>
		<?php echo "<?php else: ?>\n"; ?>
			<?php echo "<p>No hay {$details['controller']} asociados al/el {$singularHumanName}</p>\n"; ?>
		<?php echo "<?php endif; ?>\n"; ?>
	</div>
</div>
<?php endforeach; ?>
