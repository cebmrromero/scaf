<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php echo "<?php \$this->Html->addCrumb('{$pluralHumanName}', '/{$pluralVar}'); ?>\n"; ?>
<?php echo "<?php \$this->Html->addCrumb('Listado'); ?>\n"; ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li class="active"><?php echo "<?php echo \$this->Html->link(__('Listado'), array('action' => 'index')); ?>"; ?></li>
		<li><?php echo "<?php echo \$this->Html->link(__('Nuevo'), array('action' => 'add')); ?>"; ?></li>
	</ul>
</div>
<?php echo "<h3><?php echo __('{$pluralHumanName}'); ?></h3>" ;?>
<div class="row-fluid <?php echo $pluralVar; ?> index">
	<div class="span12">
		<p>A continuación el listado de <?php echo $pluralVar; ?>:</p>
		<table class="table table-hover table-striped table-condensed">
			<thead>
				<tr>
				<?php foreach ($fields as $field): ?>
	<th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
				<?php endforeach; ?>
	<th class="actions"><?php echo "<?php echo __('Acciones'); ?>"; ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
			echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
			echo "\t\t\t\t\t<tr>\n";
				foreach ($fields as $field) {
					echo "\t\t\t\t\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
				}

		echo "\t\t\t\t\t\t<td class=\"actions span1\">\n";
		echo "\t\t\t\t\t\t\t<div class=\"btn-group\">\n";
		echo "\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('Ver'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-info btn-mini')); ?>\n";
		echo "\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('Editar'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-info btn-mini')); ?>\n";
		echo "\t\t\t\t\t\t\t\t<?php echo \$this->Form->postLink(__('Eliminar'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn btn-danger btn-mini'), __('¿Está seguro(a) que desea eliminar éste registro?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
		echo "\t\t\t\t\t\t\t</div>\n";
		echo "\t\t\t\t\t\t</td>\n";
	echo "\t\t\t\t\t</tr>\n";

	echo "\t\t\t\t<?php endforeach; ?>\n";
	?>
			</tbody>
		</table>
	</div>
</div>
<div class="row-fluid <?php echo $pluralVar; ?> index">
	<div class="span12 text-center">
	<div class="pagination">
		<ul>
			<?php echo "<?php\n"; ?>
				<?php echo "echo \$this->Paginator->prev('< ' . __('Anterior'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));\n"; ?>
				<?php echo "echo \$this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));\n"; ?>
				<?php echo "echo \$this->Paginator->next(__('Siguiente') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));\n"; ?>
			<?php echo "?>\n"; ?>
		</ul>
	</div>
</div>
