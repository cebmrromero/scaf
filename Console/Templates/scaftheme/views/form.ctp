<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php echo "<?php \$this->Html->addCrumb(__('{$pluralHumanName}'), '/{$pluralVar}'); ?>\n"; ?>
<?php printf("<?php \$this->Html->addCrumb('%s'); ?>\n", ($action == 'add') ? __('Nuevo') : __('Editar')); ?>
<div class="actions">
	<ul class="nav nav-pills">
		<li><?php echo "<?php echo \$this->Html->link(__('Listado'), array('action' => 'index')); ?></li>\n"; ?>
		<?php
		if ($action == 'add') {
			printf("<li class=\"active\"><?php echo \$this->Html->link(__('%s'), array('action' => 'add')); ?></li>\n", ($action == 'add') ? 'Nuevo': 'Editar');
		} else {
			echo "<li class=\"active\">";
				printf("<?php echo \$this->Html->link(__('%s'), array('action' => 'edit', \$this->request->data['{$modelClass}']['id'])); ?>", Inflector::humanize('editar'));
			echo "</li>\n";
			printf("\t\t<li><?php echo \$this->Html->link(__('%s'), array('action' => 'add')); ?></li>\n", Inflector::humanize('nuevo'));
		}
		?>
	</ul>
</div>
<div class="row-fluid <?php echo $pluralVar; ?> form">
	<?php echo "<?php echo \$this->Form->create('{$modelClass}'); ?>\n"; ?>
<?php if ($action == 'edit'): ?>
		<?php echo "<?php echo \$this->Form->input('id'); ?>\n"; ?>
<?php endif; ?>
		<fieldset>
			<legend><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize(($action == 'add') ? 'Nuevo': 'Editar'), $singularHumanName); ?></legend>
			<div class="row-fluid">
<?php
				foreach ($fields as $field) {
					if (strpos($action, 'add') !== false && $field == $primaryKey) {
						continue;
					} elseif (!in_array($field, array('id', 'created', 'modified', 'updated'))) {
						?>
<?php echo "\t\t\t\t<div class=\"span6 control-group\">\n"; ?>
					<?php printf("<?php echo \$this->Form->input('{$field}', array('class' => 'span12', 'label' => '%s')); ?>\n", Inflector::humanize($field)); ?>
				</div>
<?php
					}
				}
				if (!empty($associations['hasAndBelongsToMany'])) {
					foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
						?>
<?php echo "\t\t\t\t<div class=\"span6 control-group\">\n"; ?>
					<?php printf("<?php echo \$this->Form->input('{$assocName}', array('class' => 'span12', 'label' => '%s')); ?>\n", Inflector::humanize($assocName)); ?>
				</div>
<?php
					}
				}
?>
			</div>
		</fieldset>
		<div class="form-actions">
			<?php echo '<button type="submit" class="btn btn-primary"><?php echo __("Guardar"); ?></button>'; ?>
			<?php echo '<button type="button" class="btn cancel"><?php echo __("Cancelar"); ?></button>'; ?>
		</div>
	<?php echo "<?php echo \$this->Form->end(); ?>\n"; ?>
</div>
